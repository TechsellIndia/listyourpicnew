const router = require("express").Router();
const { Adminschema } = require("../models/admin");
const { videoschema } = require("../models/videos");
const { User } = require("../models/user");
const fetchuser = require("../middleware/fetchuser");
const commonFunction = require('../helper/commonFunction');
const { commonResponse: response } = require('../helper/commonResponseHandler');
const { ErrorMessage } = require('../helper/message');
const { SuccessMessage } = require('../helper/message');
const { ErrorCode } = require('../helper/statusCode');
const { SuccessCode } = require('../helper/statusCode');
const jwt = require('jsonwebtoken');
const { upload } = require("../helper/filehelper");
const messageschema = require("../models/messageSchema");
const bannerCategory = require("../models/bannerCategory");
const { afterBeforeImage } = require("../models/afterBeforeImage");
const generateAuthToken = (user) => {
    const token = jwt.sign({ _id: user._id.toString() }, "thisisnewcourse");
    return token;
};
module.exports = {
    /**
     * Function Name :addAfterbeforeImage
     * Description   : addAfterbeforeImage
     *
     * @return response
    */
    addAfterbeforeImage: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let afterImage = req.files['afterImage'];
                let beforeImage = req.files['beforeImage'];
                let data1 = await commonFunction.uploadFile(afterImage[0])
                let data2 = await commonFunction.uploadFile(beforeImage[0])
                req.body.afterImage = data1.Location
                req.body.beforeImage = data2.Location
                const afterBeforeImageSave = new afterBeforeImage({
                    name: req.body.name,
                    afterImage: req.body.afterImage,
                    beforeImage: req.body.beforeImage,
                });
                const saveafterBeforeImage = await afterBeforeImageSave.save();
                response(res, SuccessCode.SUCCESS, saveafterBeforeImage, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :bannerList
     * Description   : bannerList
     *
     * @return response
    */
    afterbeforeImagerList: async (req, res) => {
        try {
            const data = await afterBeforeImage.find();
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :editAfterbeforeImage
     * Description   : editAfterbeforeImage
     *
     * @return response
    */
    editAfterbeforeImage: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let imageEdit = await afterBeforeImage.findOne({ _id: req.body._id });
                if (imageEdit) {
                    if (req.files['afterImage'] && !req.files['beforeImage']) {
                        let afterImage = req.files['afterImage'];
                        let data1 = await commonFunction.uploadFile(afterImage[0])
                        req.body.afterImage = data1.Location
                    }
                    if (!req.files['afterImage'] && req.files['beforeImage']) {
                        let beforeImage = req.files['beforeImage'];
                        let data2 = await commonFunction.uploadFile(beforeImage[0])
                        req.body.beforeImage = data2.Location
                    }
                    if (req.files['afterImage'] && req.files['beforeImage']) {
                        let afterImage = req.files['afterImage'];
                        let beforeImage = req.files['beforeImage'];

                        let data1 = await commonFunction.uploadFile(afterImage[0])
                        let data2 = await commonFunction.uploadFile(beforeImage[0])
                        req.body.afterImage = data1.Location
                        req.body.beforeImage = data2.Location
                    }
                    let update = await afterBeforeImage.findByIdAndUpdate({ _id: imageEdit._id }, { $set: req.body }, { new: true })
                    response(res, SuccessCode.SUCCESS, update, SuccessMessage.DATA_SAVED);
                }

            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :viewAfterbeforeImage
     * Description   : viewAfterbeforeImage
     *
     * @return response
    */
    viewAfterbeforeImage: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let imageEdit = await afterBeforeImage.findOne({ _id: req.query._id });
                response(res, SuccessCode.SUCCESS, imageEdit, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :editAfterbeforeImage
     * Description   : editAfterbeforeImage
     *
     * @return response
    */
    deleteAfterbeforeImage: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let imageEdit = await afterBeforeImage.findOne({ _id: req.query._id, status: { $ne: "DELETE" } });
                if (!imageEdit) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let update = await afterBeforeImage.findByIdAndUpdate({ _id: imageEdit._id }, { $set: { status: "DELETE" } }, { new: true })
                    response(res, SuccessCode.SUCCESS, update, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :afterbeforeImagerList
     * Description   : afterbeforeImagerList
     *
     * @return response
    */
    afterbeforeImagerListforuser: async (req, res) => {
        try {
            const data = await afterBeforeImage.find({ status: { $ne: "DELETE" } });
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    videoUpload: async (req, res) => {
        try {
            if (req.file) {
                console.log("==========================>",req.file.path)
                req.body.video = await commonFunction.videoUpload(req.file.path);
            }
            const videoSave = new videoschema({
                videoLink: req.body.video,
            });
             console.log("==============192============>", req.body.video)
            const savevideo = await videoSave.save();
            response(res, SuccessCode.SUCCESS, savevideo, SuccessMessage.DATA_SAVED);
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    
}