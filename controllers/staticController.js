const { staticschema } = require('../models/static');
const { Adminschema } = require("../models/admin");
const { commonResponse: response } = require('../helper/commonResponseHandler');
const { ErrorMessage } = require('../helper/message');
const { ErrorCode } = require('../helper/statusCode');
const { SuccessMessage } = require('../helper/message');
const { SuccessCode } = require('../helper/statusCode');
const commonFunction = require('../helper/commonFunction');
module.exports = {

    /**
     * Function Name :addStatic
     * Description   : addStatic
     *
     * @return response
    */
    addStatic: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                var a = req.body.title;
                var b = a.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
                const static = new staticschema({
                    title: req.body.title,
                    Type: b,
                    description: req.body.description,
                });
                const saveblogs = await static.save();
                response(res, SuccessCode.SUCCESS, saveblogs, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },   
    /**
     * Function Name :  getStaticContent
     * Description   :  getStaticContent in static management
     *
     * @return response
     */
    getstaticList: async (req, res) => {
        try {
            const data = await staticschema.find();
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },

    /**
     * Function Name :  getStaticContent
     * Description   :  getStaticContent in static management
     *
     * @return response
     */
    getStaticContent: async (req, res) => {
        try {
            staticschema.find({ Type: req.query.Type }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.INTERNAL_ERROR, err, ErrorMessage.INTERNAL_ERROR)
                }
                else if (result.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND)
                }
                else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND)
                }
            })
        } catch (error) {
            console.log("error", error)
            response(res, ErrorCode.WENT_WRONG, error, ErrorMessage.SOMETHING_WRONG)
        }
    },

    /** 
     * Function Name :  updateStaticContent
     * Description   :  updateStaticContent in static management
     *
     * @return response
     */
    updateStaticContent: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let static = await staticschema.findOne({ _id: req.params._id });
                if (!static) {
                    response(res, ErrorCode.NOT_FOUND, ErrorMessage.dataNotFound, [])
                } else {
                    console.log(req.body)
                    let obj = {
                        title: req.body.title || static.title ,
                        description: req.body.description || static.description
                    }
                    staticschema.findByIdAndUpdate({ _id: static._id }, { $set: obj }, { new: true }, (err, result) => {
                        if (err) {
                            response(res, ErrorCode.INTERNAL_ERROR, err, ErrorMessage.INTERNAL_ERROR)
                        } else {
                            response(res, SuccessCode.SUCCESS, result, "Updated Successfully.")
                        }
                    })
                }
            }
        } catch (error) {
            console.log("error", error)
            response(res, ErrorCode.WENT_WRONG, error, ErrorMessage.SOMETHING_WRONG)
        }
    },
    /**
     * Function Name :  getStaticContent
     * Description   :  getStaticContent in static management
     *
     * @return response
     */
    getStaticContentbytype: async (req, res) => {
        try {
            staticschema.findOne({ Type: req.params.Type }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.INTERNAL_ERROR, err, ErrorMessage.INTERNAL_ERROR)
                }
                else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND)
                }
                else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND)
                }
            })
        } catch (error) {
            console.log("error", error)
            response(res, ErrorCode.WENT_WRONG, error, ErrorMessage.SOMETHING_WRONG)
        }
    },


}
