var paypal = require("paypal-rest-sdk");
const commonFunction = require('../helper/commonFunction');
const { User, validate } = require("../models/user");
const { package } = require("../models/PackageSchema");
const { createProject } = require("../models/createproject");
const { transactiondetail } = require("../models/transactionStatus");
const { cartdetail } = require("../models/cart");
const storagePakages = require('../models/pakagesStorage')
const plansModel = require('../models/plansModel')
const { commonResponse: response } = require('../helper/commonResponseHandler');
const { ErrorMessage } = require('../helper/message');
const { SuccessMessage } = require('../helper/message');
const { ErrorCode } = require('../helper/statusCode');
const { SuccessCode } = require('../helper/statusCode');
const fs = require('fs');
paypal.configure({
    mode: "sandbox", //sandbox or live
    client_id: "AfrwGgPhrZF75g-lnWIEZCMz-0zvxaFJSsEL2oaSHJ7JOEUDwv3WTtvzXyxJfowuVOty7AjhTC3TWWq9",
    client_secret: "EJmUs61eKjJOkKE937qEQT7MCuKakPmpcrZ19jhm6X5orw7dI24YsK5kde_wOg4YhrosTSAsHIomcSGP",
});
// paypal.configure({
//     mode: "live", //sandbox or live
//     client_id: "AQcgEPrT-bJ8iwEm2nvuBzOHmqpqUBcw5rS8wbsSmzJC-fQlu31TyNLJ-oCdXIOVb7xm4e4VvbZk4e7f",
//     client_secret: "ECx3JLwOj1oGy2QmQIWUq7XEWjqju4837GCSe_Nwtx9PSqBZyAD0yHTc7yezil_rTcP-2OUd-rjDs23K",
// });

module.exports = {
    buyStoragePakage: async (req, res) => {
        const userId = req.user._id;
        try {
            const userResult = await User.findById({ _id: userId });
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let findStoragepackage = await storagePakages.findOne({ _id: req.body.storageId, status: "ACTIVE" });
                if (!findStoragepackage) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    const create_payment_json = {
                        "intent": "sale",
                        "payer": {
                            "payment_method": "paypal"
                        },
                        "redirect_urls": {
                            return_url: "http://localhost:1700/api/v1/paypal/success1",
                            cancel_url: "http://localhost:1700/api/v1/paypal/cancel1",
                        },
                        "transactions": [{
                            "item_list": {
                                "items": [{
                                    "name": findStoragepackage.planName,
                                    "price": findStoragepackage.amount,
                                    "currency": 'USD',
                                    "quantity": "1"
                                }]
                            },
                            "amount": {
                                "currency": 'USD',
                                "total": findStoragepackage.amount
                            }
                        }]
                    };
                    paypal.payment.create(create_payment_json, async (error, payData) => {
                        if (error) {
                            response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                        } else {
                            for (let i = 0; i < payData.links.length; i++) {
                                if (payData.links[i].rel === 'approval_url') {
                                    var tokenId = payData.links[i].href.split('&token=');
                                    const transactionCreate = new transactiondetail({
                                        planName: findStoragepackage.planName,
                                        amount: findStoragepackage.amount,
                                        storagePakages: findStoragepackage._id,
                                        user: userResult._id,
                                        planType: findStoragepackage.planType,
                                        transactionId: payData.id,
                                        tokenId: tokenId[1],
                                        transactionStatus: "PENDING"
                                    });
                                    let saveRes = await transactionCreate.save();
                                    if (saveRes) {
                                        response(res, SuccessCode.SUCCESS, payData.links[i].href, SuccessMessage.PAYMENT_SUCCESS);
                                    } else {
                                        console.log("cck ");
                                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                }
                            }
                        }
                    })
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    success1: async (req, res) => {
        try {
            var transactionResult = await transactiondetail.findOneAndUpdate({ transactionId: req.query.paymentId }, { $set: { transactionStatus: "SUCCESS" } }, { new: true });
            if (transactionResult) {
                console.log("483", transactionResult)
                let obj = {
                    userId: transactionResult.user,
                    storagePackageId: transactionResult.storagePakages,
                    packageType: "STORAGE",
                    amount: transactionResult.amount,
                    planName: transactionResult.planName,
                    startPlans: new Date().getTime(),
                }
                let plansSave = await plansModel(obj).save();
                if (plansSave) {
                    fs.readFile("./success.html", function (error, pgResp) {
                        if (error) {
                            res.writeHead(404);
                            res.write('Contents you are looking are Not Found');
                        } else {
                            res.writeHead(200, { 'Content-Type': 'text/html' });
                            res.write(pgResp);
                        }

                        res.end();
                    });
                }
            }
        }
        catch (error) {
            console.log("==success error ===================================>", error);
            response(res, ErrorCode.SOMETHING_WRONG, error, ErrorMessage.SOMETHING_WRONG);
        }
    },
    failure1: async (req, res) => {
        try {
            var transactionResult = await transactiondetail.findOneAndUpdate({ tokenId: req.query.token }, { $set: { transactionStatus: "FAILURE" } }, { new: true });
            console.log("508", transactionResult)
            fs.readFile("./failure.html", function (error, pgResp) {
                if (error) {
                    res.writeHead(404);
                    res.write('Contents you are looking are Not Found');
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(pgResp);
                }

                res.end();
            });
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    cancel1: async (req, res) => {
        try {
            fs.readFile("./cancel.html", function (error, pgResp) {
                if (error) {
                    res.writeHead(404);
                    res.write('Contents you are looking are Not Found');
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(pgResp);
                }
                res.end();
            });
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    addTocart: async (req, res) => {
        const userId = req.user._id;
        try {
            const userResult = await User.findById({ _id: userId }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                let projectFind = await createProject.findById({ _id: req.body.projectId });
                if (!projectFind) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    let packageFind = await package.findById({ _id: req.body.packageId, status: "ACTIVE" })
                    if (!packageFind) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                    } else {
                        if (projectFind.projectType == "IMAGE" || projectFind.projectType == null || projectFind.projectType == undefined) {
                            let selected = [];
                            let totalImage = req.body.selectedImage.length;
                            for (let i = 0; i < req.body.selectedImage.length; i++) {
                                const element = req.body.selectedImage[i];
                                selected.push(element)
                            }
                            const cartCreate = new cartdetail({
                                amount: packageFind.amount * totalImage,
                                userId: userResult._id,
                                projectId: projectFind._id,
                                packageId: packageFind._id,
                                totalImage: totalImage,
                                selectedImage: selected,
                                projectType: projectFind.projectType || "IMAGE"
                            });
                            let saveRes = await cartCreate.save();
                            if (saveRes) {
                                response(res, SuccessCode.SUCCESS, saveRes, SuccessMessage.ADDCART);
                            } else {
                                response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                            }
                        } else {
                            response(res, ErrorCode.NOT_FOUND, [], "Project different project type.");
                        }
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    linkaddTocart: async (req, res) => {
        const userId = req.user._id;
        try {
            const userResult = await User.findById({ _id: userId }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                let projectFind = await createProject.findById({ _id: req.body.projectId });
                if (!projectFind) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    let packageFind = await package.findById({ _id: req.body.packageId, status: "ACTIVE" })
                    if (!packageFind) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                    } else {
                        if (projectFind.projectType == "LINK") {
                            for (let i = 0; i < projectFind.originalDriveLink.length; i++) {
                                if (req.body.linkId == projectFind.originalDriveLink[i]._id) {
                                    const cartCreate = new cartdetail({
                                        amount: packageFind.amount * projectFind.originalDriveLink[i].totalImage,
                                        userId: userResult._id,
                                        projectId: projectFind._id,
                                        packageId: packageFind._id,
                                        totalImage: projectFind.originalDriveLink[i].totalImage,
                                        link: projectFind.originalDriveLink[i].link,
                                        projectType: projectFind.projectType
                                    });
                                    let saveRes = await cartCreate.save();
                                    if (saveRes) {
                                        response(res, SuccessCode.SUCCESS, saveRes, SuccessMessage.ADDCART);
                                    } else {
                                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                }
                            }
                        } else {
                            response(res, ErrorCode.NOT_FOUND, [], "Project different project type.");
                        }
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getCart: async (req, res) => {
        const userId = req.user._id;
        try {
            const userResult = await User.findById({ _id: userId }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                let cartFind = await cartdetail.find({ userId: userResult._id }).populate('packageId projectId');
                if (cartFind.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let total = 0;
                    for (let i = 0; i < cartFind.length; i++) {
                        total = total + Number(cartFind[i].amount);
                    }
                    response(res, SuccessCode.SUCCESS, { cartFind, total }, SuccessMessage.PAYMENT_SUCCESS);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    deleteCart: async (req, res) => {
        const userId = req.user._id;
        try {
            const userResult = await User.findById({ _id: userId }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                let cartFind = await cartdetail.findById({ _id: req.params._id });
                if (!cartFind) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    let deleteProject = await cartdetail.findByIdAndDelete({ _id: cartFind._id });
                    let cartFind1 = await cartdetail.find({ userId: userResult._id }).populate('packageId projectId');
                    if (cartFind1.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                    } else {
                        let total = 0;
                        for (let i = 0; i < cartFind1.length; i++) {
                            total = total + Number(cartFind1[i].amount);
                        }
                        response(res, SuccessCode.SUCCESS, { cartFind1, total }, SuccessMessage.PAYMENT_SUCCESS);
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    pay: async (req, res) => {
        const userId = req.user._id;
        try {
            const userResult = await User.findById({ _id: userId }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                let totalAmount = 0;
                let cartFind = await cartdetail.find({ userId: userResult._id });
                for (let i = 0; i < cartFind.length; i++) {
                    totalAmount = totalAmount + Number(cartFind[i].amount)
                }
                const create_payment_json = {
                    "intent": "sale",
                    "payer": {
                        "payment_method": "paypal"
                    },
                    "redirect_urls": {
                        return_url: "http://localhost:1700/api/v1/paypal/success",
                        cancel_url: "http://localhost:1700/api/v1/paypal/cancel",
                    },
                    "transactions": [{
                        "item_list": {
                            "items": [{
                                "name": "Order payment",
                                "price": totalAmount,
                                "currency": 'USD',
                                "quantity": "1"
                            }]
                        },
                        "amount": {
                            "currency": 'USD',
                            "total": totalAmount
                        }
                    }]
                };
                paypal.payment.create(create_payment_json, async (error, payData) => {
                    if (error) {
                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                    } else {
                        for (let i = 0; i < payData.links.length; i++) {
                            if (payData.links[i].rel === 'approval_url') {
                                var tokenId = payData.links[i].href.split('&token=');
                                let cartFind = await cartdetail.find({ userId: userResult._id });
                                for (let j = 0; j < cartFind.length; j++) {
                                    let projectFind = await createProject.findById({ _id: cartFind[j].projectId });
                                    if (!projectFind) {
                                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                                    } else {
                                        let packageFind = await package.findById({ _id: cartFind[j].packageId, status: "ACTIVE" });
                                        const transactionCreate = new transactiondetail({
                                            planName: packageFind.packageName,
                                            amount: cartFind[j].amount,
                                            user: userResult._id,
                                            package: packageFind._id,
                                            project: projectFind._id,
                                            transactionId: payData.id,
                                            selectedImage: cartFind[j].selectedImage,
                                            totalImage: cartFind[j].totalImage,
                                            tokenId: tokenId[1],
                                            transactionStatus: "PENDING"
                                        });
                                        let saveRes = await transactionCreate.save();
                                    }
                                }
                                response(res, SuccessCode.SUCCESS, payData.links[i].href, SuccessMessage.PAYMENT_SUCCESS);
                            }
                        }
                    }
                })

            }
        }
        catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    success: async (req, res) => {
        try {
            let findTransaction = await transactiondetail.find({ transactionId: req.query.paymentId });
            for (let i = 0; i < findTransaction.length; i++) {
                var transactionResult = await transactiondetail.findByIdAndUpdate({ _id: findTransaction[i]._id }, { $set: { transactionStatus: "SUCCESS" } }, { new: true });
                let cartFind = await cartdetail.findOne({ amount: (transactionResult.amount).toString() });
                let deleteCart = await cartdetail.findByIdAndDelete({ _id: cartFind._id });
            }
            fs.readFile("./success.html", function (error, pgResp) {
                if (error) {
                    res.writeHead(404);
                    res.write('Contents you are looking are Not Found');
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(pgResp);
                }

                res.end();
            });
        }
        catch (error) {
            console.log("==success error ===================================>", error);
            response(res, ErrorCode.SOMETHING_WRONG, error, ErrorMessage.SOMETHING_WRONG);
        }
    },
    failure: async (req, res) => {
        try {
            let findTransaction = await transactiondetail.find({ tokenId: req.query.token });
            for (let i = 0; i < findTransaction.length; i++) {
                var transactionResult = await transactiondetail.findByIdAndUpdate({ _id: findTransaction[i]._id }, { $set: { transactionStatus: "FAILURE" } }, { new: true });
            }
            fs.readFile("./failure.html", function (error, pgResp) {
                if (error) {
                    res.writeHead(404);
                    res.write('Contents you are looking are Not Found');
                } else {
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.write(pgResp, window.history.go(-1));
                }

                res.end();
            });
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    cancel: async (req, res) => {
        try {
            var transactionResult = await transactiondetail.findOneAndUpdate({ transactionId: req.query.paymentId }, { $set: { transactionStatus: "CANCELED" } }, { new: true });
            if (transactionResult) {
                fs.readFile("./cancel.html", function (error, pgResp) {
                    if (error) {
                        res.writeHead(404);
                        res.write('Contents you are looking are Not Found');
                    } else {
                        res.writeHead(200, { 'Content-Type': 'text/html' });
                        res.write(pgResp);
                    }
                    res.end();
                });
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    transactionList: async (req, res) => {
        const userid = req.user?._id;
        try {
            const userResult = await User.findById({ _id: userid }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                var query = { user: userid };
                var options = {
                    sort: { createdAt: -1 },
                    populate: { path: "project", select: "projectname" }
                };
                transactiondetail.paginate(query, options, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result.docs.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result.docs, SuccessMessage.DATA_FOUND);
                    }
                })
            }
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getTransaction: async (req, res) => {
        const userid = req.user?._id;
        try {
            const userResult = await User.findById({ _id: userid }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let transaction = await transactiondetail.findById({ _id: req.params.id }).populate("project user");
                if (!transaction) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, transaction, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
}