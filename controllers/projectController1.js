const { User, validate } = require("../models/user");
const { Adminschema } = require("../models/admin");
const { createProject } = require("../models/createproject");
const { notification } = require('../models/notification');
const { transactiondetail } = require("../models/transactionStatus");
const messageschema = require("../models/messageSchema");
// const { MultipleImage } = require("../models/ImageSchema");
const { upload } = require("../helper/filehelper");
const { package } = require("../models/PackageSchema");
const storagePakagesModel = require('../models/pakagesStorage');
const plansModel = require('../models/plansModel');
const { commonResponse: response } = require('../helper/commonResponseHandler');
const { ErrorMessage } = require('../helper/message');
const { SuccessMessage } = require('../helper/message');
const { ErrorCode } = require('../helper/statusCode');
const { SuccessCode } = require('../helper/statusCode');
const commonfunction = require('../helper/commonFunction1');
const useFunction = require('../helper/useFunction');
const error = require("mongoose/lib/error");
const multiparty = require('multiparty');
const e = require("express");
const { body } = require("express-validator");
const baseUrl = "http://localhost:1700/";
const JSZip = require('jszip');
const fs = require('fs');
// const zip = new JSZip();
const fileSizeFormatter = (bytes, decimal) => {
    if (bytes === 0) {
        return "0 Bytes";
    }
    const dm = decimal || 2;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "YB", "ZB"];
    const index = Math.floor(Math.log(bytes) / Math.log(1000));
    return (
        parseFloat((bytes / Math.pow(1000, index)).toFixed(dm)) + " " + sizes[index]
    );
};

module.exports = {
    uploadPhoto: async (req, res) => {
        try {
            let image;
            image = await commonfunction.imageUpload3(req.body.image, req.body.imageName);
            response(res, SuccessCode.SUCCESS, image, SuccessMessage.DATA_SAVED);
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    uploadPhoto1: async (req, res) => {
        try {
            let image1;
            if (req.files) {
                for (let i = 0; i < req.files.length; i++) {
                    image1 = await commonfunction.imageUpload2(req.files[i]);

                }
            }
            response(res, SuccessCode.SUCCESS, image1, SuccessMessage.DATA_SAVED);
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    createproject: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let Admin = await Adminschema.findOne({ userType: "ADMIN" });
                const ProjectCreated = new createProject({
                    user: req.user._id,
                    projectname: req.body.projectname,
                    address1: req.body.address1,
                    address2: req.body.address2,
                    city: req.body.city,
                    state: req.body.state,
                    pcode: req.body.pcode,
                });
                const saveProject = await ProjectCreated.save();
                if (saveProject) {
                    var newOtp = req.body.projectname;
                    var text = `${saveProject.projectname} project has been create successfully.`;
                    let subject = `Project create`;
                    // let mailSend = await useFunction.sendMail2(Admin.email, Admin.name, User1.email, newOtp, User1.fname, subject, text);
                    // if (mailSend) {
                    const notificationCreate = new notification({
                        projectId: saveProject._id,
                        user: req.user._id,
                        assignUser: Admin._id,
                        title: "Project create",
                        body: `${saveProject.projectname} project has been create successfully.`,
                        notificationType: "PROJECT_CREATE"
                    });
                    await notificationCreate.save();
                    const data = await createProject.findById({ _id: saveProject._id });
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_SAVED);
                    // }
                }

            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    uploadImageInProject: async (req, res, next) => {
        try {
            const data = await createProject.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let totalImage = data.originalImage.length;
                let imageCount;
                if (totalImage > 0) {
                    for (let i = 0; i < 1; i++) {
                        let count = data.originalImage[totalImage - 1].imageId;
                        imageCount = Number(count)
                        console.log("imageCount=====98======", imageCount);
                    }
                } else {
                    imageCount = 0
                }
                let project2;
                const { files } = req;
                let image1 = [], image2 = []
                let paths = []
                if (req.files) {
                    for (let i = 0; i < req.files.length; i++) {
                        // console.log(req.files[i])
                        let extenstion = await getFileExtension(req.files[i].originalname);
                        let convertLowerCase = extenstion.toLowerCase()
                        let originalname = await changeOriExtensoin(req.files[i].originalname, convertLowerCase)
                        let filename = await changeExtension(req.files[i].filename, convertLowerCase)
                        let path = req.files[i].path
                        let obj1 = {
                            fieldname: 'image',
                            originalname: originalname,
                            encoding: '7bit',
                            mimetype: 'image/png',
                            destination: 'uploads/',
                            filename: filename,
                            path: path,
                            size: req.files[i].size
                        }
                        // let data = await commonfunction.imageUpload1(obj1);
                        let data = await commonfunction.imageUpload2(obj1);
                        console.log("======================================", data);
                        console.log(data.Location);
                        // console.log(data[0].Location);
                        // console.log(data[1].Location);
                        let b = req.files[i].path
                        image1.push(data.Location)
                        // const uImg = data.Location;
                        // let d2 = await commonfunction.imageUploadSm(uImg);                        // image1.push(data[1].Location)
                        // image2.push(data[0].Location)
                        paths.push(b)
                    }
                }
                // console.log("image1=======================",image1);
                // console.log("image2=======================",image2);
                for (let i = 1; i <= image1.length; i++) {
                    const imageId = imageCount + i;
                    let obj2 = {
                        imageId: imageId,
                        image: image1[i - 1],
                        kbImage: image1[i - 1],
                        path: paths[i - 1]
                    }
                    project2 = await createProject.findByIdAndUpdate({ _id: req.params._id }, { $push: { originalImage: obj2 } }, { new: true });
                }
                response(res, SuccessCode.SUCCESS, project2, SuccessMessage.DATA_FOUND);
            }
        }
        catch (error) {
            console.log(error);
            return next(error);
        }
    },
    uploadImageInProjectBase64: async (req, res, next) => {
        try {
            const data = await createProject.findById({ _id: req.body._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let totalImage = data.originalImage.length;
                let project2;
                let a = [], image2 = [];
                for (let i = 0; i < req.body.image.length; i++) {
                    const element = req.body.image[i];
                    // let image = await commonfunction.imageUpload(element, req.body.imageName[i])
                    let image = await commonfunction.imageUpload3(element, req.body.imageName[i])
                    console.log(image[0].Location);
                    console.log(image[1].Location);
                    a.push(image[1].Location)
                    image2.push(image[0].Location)
                }
                for (let i = 1; i <= a.length; i++) {
                    let obj2 = {
                        imageId: totalImage + i,
                        image: a[i - 1],
                        kbImage: image2[i - 1],
                        path: "uploads\2022-10-06T07-48-15.086Z-bhall3+Before.jpg"
                    }
                    project2 = await createProject.findByIdAndUpdate({ _id: req.body._id }, { $push: { originalImage: obj2 } }, { new: true });
                }
                response(res, SuccessCode.SUCCESS, project2, SuccessMessage.DATA_FOUND);
            }
        }
        catch (error) {
            return next(error);
        }
    },


}
const getFileExtension = async (filename) => {
    const extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length);
    return extension;
}
const changeExtension = async (filename, extension) => {
    let root = filename.substring(0, filename.length - extension.length)
    ext = extension.startsWith('.') ? extension : extension.length > 0 ? `${extension}` : ''
    return `${root}${ext}`
}
const changeOriExtensoin = async (filename, extension) => {
    let root = filename.substring(0, filename.length - extension.length)
    ext = extension.startsWith('.') ? extension : extension.length > 0 ? `${extension}` : ''
    return `${root}${ext}`
}
const resize = async (context, image, width, height, x, y, callback) => {
    gm(image)
        .crop(width, height, x, y)
        .toBuffer(format, function (err, buffer) {
            if (err) {
                callback(false);
            } else {
                callback(buffer);
            }
        });
}
const savefileFolder = async (data, name) => {
    let res = await Jimp.read(data)
    res.quality(5).write(name)
}
