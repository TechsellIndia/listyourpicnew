const { User, validate } = require("../models/user");
const { Adminschema } = require("../models/admin");
const { notification } = require('../models/notification')
const { comments } = require('../models/comment')
const commonFunction = require('../helper/commonFunction');
const useFunction = require('../helper/useFunction');
const { commonResponse: response } = require('../helper/commonResponseHandler');
const { ErrorMessage } = require('../helper/message');
const { SuccessMessage } = require('../helper/message');
const { ErrorCode } = require('../helper/statusCode');
const { SuccessCode } = require('../helper/statusCode');
const cloudinary = require('cloudinary');
const { blogs } = require("../models/blogs");
const bcrypt = require("bcryptjs");
const jwt = require('jsonwebtoken');
const otp = require("../models/OtpSchema");
const { createProject } = require("../models/createproject");
const companySetting = require('../models/companySetting');
const generateAuthToken = (user) => {
    const token = jwt.sign({ _id: user._id.toString() }, "thisisnewcourse");
    return token;
};
module.exports = {

    /**
     * Function Name : createCompany
     * Description   : signUp/signIn for user 
     *
     * @return response
    */
    createCompany: async (req, res) => {
        try {
            User.findOne({ email: req.body.email }, async (err, result) => {
                if (err) {
                    response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (result) {
                    response(res, ErrorCode.ALREADY_EXIST, result, ErrorMessage.EMAIL_EXIST);
                }
                else {
                    var newOtp = commonFunction.getOTP();
                    let result2;

                    let otpData = new otp({
                        email: req.body.email,
                        otp: newOtp,
                        expireIn: new Date().getTime() + 300 * 1000,
                    });
                    let otpresponse = otpData.save();
                    let insertObj = {
                        company: req.body.company,
                        email: req.body.email,
                        password: bcrypt.hashSync(req.body.password),
                        fname: req.body.fname,
                        lname: req.body.lname,
                        phone: req.body.phone,
                        extraoption: req.body.extraoption,
                    }
                    result2 = User.create(insertObj);
                    if (result2) {
                        commonFunction.WelcomeMail(req.body.email, req.body.fname, (error, otpSent) => {
                            if (error) {
                                console.log("====", error);
                                response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                            }
                            else {
                                response(res, SuccessCode.SUCCESS, result2, SuccessMessage.SIGNUP_SUCCESSFULLY)
                            }
                        });
                    }
                }
            })
        }
        catch (error) {
            console.log("==============80=====>", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :verifyOTP
     * Description   : verifyOTP of user 
     *
     * @return response
    */
    verifyOTP: async (req, res, next) => {
        try {
            let userResult = await otp.findOne({ email: req.body.email });
            if (!userResult) {
                response(res, ErrorCode.USER_NOT_FOUND, result, ErrorMessage.USER_NOT_FOUND);
            }
            else {
                if (Date.now() > userResult.expireIn) {
                    response(res, ErrorCode.INVALID_CREDENTIALS, [], ErrorMessage.OTP_EXPIRED);
                }
                if (userResult.otp != req.body.otp) {
                    response(res, ErrorCode.INVALID_CREDENTIALS, [], ErrorMessage.INVALID_OTP);
                }
                var updateResult = await User.findOneAndUpdate({ email: userResult.email }, { otpVerified: true }, { new: true })
                const authtoken = generateAuthToken(userResult);
                var result = {
                    _id: updateResult._id,
                    email: updateResult.email,
                    token: authtoken
                };
                response(res, SuccessCode.SUCCESS, result, SuccessMessage.VERIFY_OTP);

            }
        }
        catch (error) {
            return next(error);
        }
    },
    /**
     * Function Name :login
     * Description   : login of user 
     *
     * @return response
    */
    login: async (req, res, next) => {
        try {
            const user = await User.findOne({ email: req.body.email });
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                // console.log("user======", user)
                const validPassword = await bcrypt.compareSync(req.body.password, user.password);
                if (!validPassword) {
                    response(res, ErrorCode.INVALID_CREDENTIALS, [], ErrorMessage.INVALID_CREDENTIAL)
                } else {
                    const authtoken = generateAuthToken(user);
                    // console.log("authtoken======", authtoken)
                    var result = {
                        _id: user._id,
                        email: user.email,
                        token: authtoken
                    };
                    // console.log("result======", result)
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.LOGIN_SUCCESS)
                }
            }
        } catch (error) {

            return next(error);
        }
    },
    /**
     * Function Name :getProfile
     * Description   : getProfile of user 
     *
     * @return response
    */
    getProfile: async (req, res) => {
        const userId = req.user._id;
        try {
            const user = await User.findById({ _id: userId }).select("-password");
            // console.log("use===========", user)
            response(res, SuccessCode.SUCCESS, user, SuccessMessage.DATA_FOUND);
        } catch (error) {
            response(res, ErrorCode.INTERNAL_ERROR, error, ErrorMessage.INTERNAL_ERROR)
        }
    },
    /**
     * Function Name :resendOTP
     * Description   : resendOTP of user 
     *
     * @return response
    */
    resendOTP: async (req, res) => {
        try {
            let userGet = await User.findOne({ email: req.body.email });
            if (!userGet) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                let newOtp = commonFunction.getOTP()
                commonFunction.sendMail(userGet.email, newOtp, userGet.fname, (error, otpSent) => {
                    if (error) {
                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        otp.findOne({ email: req.body.email }, (err, resu) => {
                            if (resu) {
                                otp.findByIdAndUpdate({ _id: resu._id }, { $set: { otp: newOtp, expireIn: new Date().getTime() + 300 * 1000, } }, { new: true }, (error, result) => {
                                    if (result) {
                                        User.findByIdAndUpdate({ _id: userGet._id }, { $set: { otpVerified: false } }, { new: true }, (error1, result2) => {
                                            if (result2) {
                                                let obj = {
                                                    otpResponse: result,
                                                    userResponse: result2
                                                }
                                                response(res, SuccessCode.SUCCESS, obj, "OTP send to your registered Email.")
                                            }
                                        })
                                    } else {
                                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                })
                            } else {
                                let otpData = new otp({
                                    email: req.body.email,
                                    otp: newOtp,
                                    expireIn: new Date().getTime() + 300 * 1000,
                                });
                                let otpresponse = otpData.save();
                                if (otpresponse) {
                                    User.findByIdAndUpdate({ _id: userGet._id }, { $set: { otpVerified: false } }, { new: true }, (error1, result2) => {
                                        if (result2) {
                                            let obj = {
                                                otpResponse: otpresponse,
                                                userResponse: result2
                                            }
                                            response(res, SuccessCode.SUCCESS, obj, "OTP send to your registered Email.")
                                        }
                                    })
                                }
                            }
                        })

                    }
                })
            }
        } catch (error) {

            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    forgotPassword: async (req, res) => {
        try {
            let userGet = await User.findOne({ email: req.body.email });
            if (!userGet) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            else {
                let newOtp = commonFunction.getOTP()
                commonFunction.sendMail(userGet.email, newOtp, userGet.fname, (error, otpSent) => {
                    if (error) {
                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        console.log("=====247==========", otpSent)
                        otp.findOne({ email: req.body.email }, (err, resu) => {
                            if (resu) {
                                otp.findByIdAndUpdate({ _id: resu._id }, { $set: { otp: newOtp, expireIn: new Date().getTime() + 300 * 1000, } }, { new: true }, (error, result) => {
                                    if (result) {
                                        User.findByIdAndUpdate({ _id: userGet._id }, { $set: { otpVerified: false } }, { new: true }, (error1, result2) => {
                                            if (result2) {
                                                let obj = {
                                                    otpResponse: result,
                                                    userResponse: result2
                                                }
                                                response(res, SuccessCode.SUCCESS, obj, "OTP send to your registered Email.")
                                            }
                                        })
                                    } else {
                                        console.log(error);
                                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                                    }
                                })
                            } else {
                                let otpData = new otp({
                                    email: req.body.email,
                                    otp: newOtp,
                                    expireIn: new Date().getTime() + 300 * 1000,
                                });
                                let otpresponse = otpData.save();
                                if (otpresponse) {
                                    User.findByIdAndUpdate({ _id: userGet._id }, { $set: { otpVerified: false } }, { new: true }, (error1, result2) => {
                                        if (result2) {
                                            let obj = {
                                                otpResponse: otpresponse,
                                                userResponse: result2
                                            }
                                            response(res, SuccessCode.SUCCESS, obj, "OTP send to your registered Email.")
                                        }
                                    })
                                }
                            }
                        })
                    }
                })
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    resetPassword: (req, res) => {
        try {
            User.findOne({ email: req.body.email }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.INTERNAL_ERROR, {}, ErrorMessage.INTERNAL_ERROR);
                }
                else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, {}, ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    if (result.otpVerified == true) {
                        if (req.body.newPassword == req.body.confirmPassword) {
                            User.findOneAndUpdate({ _id: result._id }, { $set: { password: bcrypt.hashSync(req.body.newPassword) } }, { new: true }, (updateErr, updateResult) => {
                                if (updateErr) {
                                    response(res, ErrorCode.INTERNAL_ERROR, {}, ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.RESET_SUCCESS);
                                }
                            })
                        }
                        else {
                            response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.NOT_MATCH);
                        }
                    } else {
                        response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.FIRST_VERIFY_OTP);
                    }
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :changePassword
     * Description   : changePassword of user 
     *
     * @return response
    */
    changePassword: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            if (!bcrypt.compareSync(req.body.oldPassword, user.password)) {
                response(res, ErrorCode.NOT_FOUND, {}, ErrorMessage.PASSWORD_NOT_MATCHED);
            } else {
                let updated = await User.findByIdAndUpdate({ _id: user._id }, { $set: { password: bcrypt.hashSync(req.body.newPassword) } }, { new: true });
                if (updated) {
                    response(res, SuccessCode.SUCCESS, updated, SuccessMessage.PASSWORD_UPDATE);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :blogList
     * Description   : blogList
     *
     * @return response
    */
    blogList: async (req, res) => {
        try {
            const data = await blogs.find();
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {

            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getBlog
     * Description   : getBlog
     *
     * @return response
    */
    getBlog: async (req, res) => {
        try {
            let blog = await blogs.findOne({ slug: req.params.slug });
            if (!blog) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, blog, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {

            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :notificationList
     * Description   : notificationList
     *
     * @return response
    */
    notificationList: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await notification.find({ user: userid });
                const dataCount = await notification.find({ user: userid, isRead: false }).count();
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, { data, dataCount }, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :notificationStatus
     * Description   : notificationStatus
     *
     * @return response
    */
    notificationStatus: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            const data1 = await notification.updateMany({ user: user._id, status: "ACTIVE" }, { $set: { isRead: true } }, { new: true });
            if (data1) {
                var query = { user: user._id };
                var options = {
                    sort: { createdAt: -1 }
                };
                notification.paginate(query, options, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result.docs.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result.docs, SuccessMessage.DATA_FOUND);
                    }
                })

            }
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getNotification
     * Description   : getNotification
     *
     * @return response
    */
    getNotification: async (req, res) => {
        try {
            const data = await notification.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {

            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :editProfile
     * Description   : editProfile
     *
     * @return response
    */
    editProfile: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                if (req.file) {
                    let data = await commonFunction.uploadFile(req.file)
                    req.body.image = data.Location;
                    const Save = await User.findByIdAndUpdate({ _id: user._id }, { $set: req.body }, { new: true });
                    response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                } else {
                    const Save = await User.findByIdAndUpdate({ _id: user._id }, { $set: req.body }, { new: true });
                    response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {

            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :createComplaint
     * Description   : createComplaint
     *
     * @return response
    */
    createComplaint: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                console.log("user.email", user.email)
                const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                let d = new Date();
                let hr = d.getHours();
                let min = d.getMinutes();
                let min1, desc;
                let date = d.getDate();
                let date1;
                let month = months[d.getMonth()];
                let year = d.getFullYear();
                if (date < 10) {
                    date1 = '' + 0 + date;
                }
                else {
                    date1 = date
                }
                if (hr < 10) {
                    hr1 = '' + 0 + hr;
                } else {
                    hr1 = hr
                }
                if (min < 10) {
                    min1 = '' + 0 + min;
                } else {
                    min1 = min
                }
                let fullDate = `${date1}-${month}-${year} ${hr1}:${min1}`;
                let screenShot = [], obj;
                if (req.files) {
                    for (let i = 0; i < req.files.length; i++) {
                        let data = await commonFunction.uploadFile(req.files[i])
                        obj = data.Location;
                        screenShot.push(obj)
                    }
                }
                var newOtp = commonFunction.complaintNumber();
                const commentCreated = new comments({
                    user: req.user._id,
                    complaintId: newOtp,
                    fname: user.fname,
                    userType: user.userType,
                    image: user.image || "",
                    screenShot: screenShot,
                    title: req.body.title,
                    comment: req.body.comment,
                    date: fullDate
                });
                const saveProject = await commentCreated.save();
                if (saveProject) {
                    response(res, SuccessCode.SUCCESS, saveProject, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :ComplaintList
     * Description   : ComplaintList
     *
     * @return response
    */
    ComplaintList: async (req, res) => {
        const userid = req.user?._id;
        try {
            const userResult = await User.findById({ _id: userid }).select("-password");;
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                var query = { user: userResult._id };
                var options = {
                    sort: { createdAt: -1 },
                    populate: { path: "user" }
                };
                comments.paginate(query, options, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result.docs.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result.docs, SuccessMessage.DATA_FOUND);
                    }
                })
            }
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getComplaint
     * Description   : getComplaint
     *
     * @return response
    */
    getComplaint: async (req, res) => {
        try {
            const data = await comments.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    replyOnComplaintbyuser: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let queryReply = await comments.findById({ _id: req.params.id });
                if (queryReply) {
                    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    let d = new Date();
                    let hr = d.getHours();
                    let min = d.getMinutes();
                    let min1, desc;
                    let date = d.getDate();
                    let date1;
                    let month = months[d.getMonth()];
                    let year = d.getFullYear();
                    if (date < 10) {
                        date1 = '' + 0 + date;
                    }
                    else {
                        date1 = date
                    }
                    if (hr < 10) {
                        hr1 = '' + 0 + hr;
                    } else {
                        hr1 = hr
                    }
                    if (min < 10) {
                        min1 = '' + 0 + min;
                    } else {
                        min1 = min
                    }
                    let fullDate = `${date1}-${month}-${year} ${hr1}:${min1}`;
                    let screenShot = [], obj1;
                    if (req.files) {
                        for (let i = 0; i < req.files.length; i++) {
                            let data = await commonFunction.uploadFile(req.files[i])
                            obj1 = data.Location;
                            screenShot.push(obj1)
                        }
                    }
                    let obj = {
                        user: req.user._id,
                        fname: user.fname,
                        userType: user.userType,
                        image: user.image,
                        screenShot: screenShot,
                        comment: req.body.comment,
                        date: fullDate
                    }
                    desc = await comments.findByIdAndUpdate({ _id: queryReply._id }, { $push: { reply: obj } }, { new: true });
                    if (desc) {
                        response(res, SuccessCode.SUCCESS, desc, SuccessMessage.DATA_SAVED);
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
    * Function Name :closeComplaintfromUserSide
    * Description   : closeComplaintfromUserSide
    *
    * @return response
   */
    closeComplaintfromUserSide: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await comments.findOne({ _id: req.body._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    const updateData = await comments.findByIdAndUpdate({ _id: data._id }, { $set: { queryStatus: "CLOSE" } }, { new: true });
                    if (updateData) {
                        response(res, SuccessCode.SUCCESS, updateData, SuccessMessage.DATA_SAVED);
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    dashboard: async (req, res, next) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let image = 0;
                let project = await createProject.count({ user: User1._id });
                let imagefind = await createProject.find({ user: User1._id });
                imagefind.forEach(element => {
                    console.log(element.originalImage.length);
                    let a = element.originalImage;
                    let Length = a.length;
                    image += Length;
                });
                let obj = {
                    name: User1.name,
                    email: User1.email,
                    project: project,
                    image: image,
                }
                response(res, SuccessCode.SUCCESS, obj, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :compSetting
     * Description   : compSetting
     *
     * @return response
    */
    compSetting: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            console.log(user);
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let companyFind = await companySetting.findOne({ userId: user._id });
                if (companyFind) {
                    if (req.file) {
                        let data = await commonFunction.uploadFile(req.file)
                        req.body.image = data.Location;
                        const Save = await companySetting.findByIdAndUpdate({ _id: companyFind._id }, { $set: req.body }, { new: true });
                        response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                    } else {
                        const Save = await companySetting.findByIdAndUpdate({ _id: companyFind._id }, { $set: req.body }, { new: true });
                        response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                    }
                } else {
                    if (req.file) {
                        req.body.userId = user._id;
                        let data = await commonFunction.uploadFile(req.file)
                        req.body.image = data.Location;
                        const Save = await companySetting(req.body).save();
                        response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                    } else {
                        req.body.userId = user._id;
                        const Save = await companySetting(req.body).save();
                        response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getCompanySetting: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let companyFind = await companySetting.findOne({ userId: user._id });
                if (companyFind) {
                    response(res, SuccessCode.SUCCESS, companyFind, SuccessMessage.DATA_FOUND);
                } else {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    contactUs: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let admin = await Adminschema.findOne({ userType: "ADMIN" });
                if (!admin) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                } else {
                    let text = `
                    Dear ${admin.name},
                    
                    mobileNumber: ${req.body.mobileNumber}
                    userName: ${req.body.userName}
                    userEmail: ${req.body.email}
                    comment: ${req.body.comment}

                    Thanks and Regards 
                    ${req.body.userName}
                    `
                    commonFunction.contactUs(admin.email, text, (error, otpSent) => {
                        if (error) {
                            response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                        }
                        else {
                            response(res, SuccessCode.SUCCESS, {}, "Mail has been sent successfully.");
                        }
                    })
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    }
}




//*********************Function for upload image *************************************/
function convertImage(image) {
    return new Promise((resolve, reject) => {
        commonFunction.uploadImage(image, (error, upload) => {
            if (error) {
                console.log("Error uploading image")
            }
            else {
                resolve(upload)
            }
        })
    })
}
//*************************End of upload image *****************************/


