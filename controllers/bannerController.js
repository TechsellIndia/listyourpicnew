const router = require("express").Router();
const { Adminschema } = require("../models/admin");
const { User } = require("../models/user");
const fetchuser = require("../middleware/fetchuser");
const bcrypt = require("bcryptjs");
const { package } = require("../models/PackageSchema");
const { createProject } = require("../models/createproject");
const { blogs } = require("../models/blogs");
const { banner } = require("../models/banner");
const { trustedClient } = require("../models/trustedClient");
const { report } = require("../models/report");
const { reportSection } = require("../models/reportSection");
const commonFunction = require('../helper/commonFunction');
const { commonResponse: response } = require('../helper/commonResponseHandler');
const { ErrorMessage } = require('../helper/message');
const { SuccessMessage } = require('../helper/message');
const { ErrorCode } = require('../helper/statusCode');
const { SuccessCode } = require('../helper/statusCode');
const jwt = require('jsonwebtoken');
const { upload } = require("../helper/filehelper");
const messageschema = require("../models/messageSchema");
const { bannerCategory } = require("../models/bannerCategory");
const generateAuthToken = (user) => {
    const token = jwt.sign({ _id: user._id.toString() }, "thisisnewcourse");
    return token;
};
module.exports = {
    /**
     * Function Name :addBanner
     * Description   : addBanner
     *
     * @return response
    */
    addBannerCategory: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const bannerCategorysave = new bannerCategory({
                    categoryName: req.body.categoryName,
                });
                const savebannerCategory = await bannerCategorysave.save();
                response(res, SuccessCode.SUCCESS, savebannerCategory, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :bannerCategoryList
     * Description   : bannerCategoryList
     *
     * @return response
    */
    bannerCategoryList: async (req, res) => {
        try {
            const data = await bannerCategory.find({});
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getbannerCategory
     * Description   : getbannerCategory
     *
     * @return response
    */
    getbannerCategory: async (req, res) => {
        try {
            let blog = await bannerCategory.findById({ _id: req.params.id });
            if (!blog) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, blog, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :editbannerCategory
     * Description   : editbannerCategory
     *
     * @return response
    */
    editbannerCategory: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let editBanner = await bannerCategory.findById({ _id: req.params.id });
                if (editBanner) {
                    let catName = await bannerCategory.findOne({ categoryName: req.body.categoryName });
                    if ((catName._id.toString()) == (editBanner._id.toString())) {
                        const bannerSave = await bannerCategory.findByIdAndUpdate({ _id: editBanner._id }, { $set: req.body }, { new: true });
                        response(res, SuccessCode.SUCCESS, bannerSave, SuccessMessage.DATA_SAVED);
                    } else {
                        response(res, ErrorCode.DATA_FOUND, [], ErrorMessage.CATEGORY_EXIST);
                    }
                }

            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    deletebannerCategoryr: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let blog = await bannerCategory.findById({ _id: req.params.id });
                if (!blog) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let blogDelete = await bannerCategory.findByIdAndDelete({ _id: blog._id });
                    response(res, SuccessCode.SUCCESS, blogDelete, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :addBanner
     * Description   : addBanner
     *
     * @return response
    */
    addBanner: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let category = await bannerCategory.findOne({ _id: req.body.bannerCategory })
                if (req.file) {
                    let data = await commonFunction.uploadFile(req.file)
                    req.body.bannerImage = data.Location;
                    console.log(req.body.bannerImage);
                }
                const bannerSave = new banner({
                    bannerName: req.body.bannerName,
                    bannerCategory: req.body.bannerCategory,
                    categoryName: category.categoryName,
                    bannerImage: req.body.bannerImage,
                });
                const saveBanner = await bannerSave.save();
                response(res, SuccessCode.SUCCESS, saveBanner, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :getBanner
     * Description   : getBanner
     *
     * @return response
    */
    getBanner: async (req, res) => {
        try {
            let blog = await banner.findById({ _id: req.params.id });
            if (!blog) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, blog, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :editBanner
     * Description   : editBanner
     *
     * @return response
    */
    editBanner: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let editBanner = await banner.findById({ _id: req.params.id });
                if (req.file) {
                    let data = await commonFunction.uploadFile(req.file)
                    req.body.bannerImage = data.Location;
                    const bannerSave = await banner.findByIdAndUpdate({ _id: editBanner._id }, { $set: req.body }, { new: true });
                    response(res, SuccessCode.SUCCESS, bannerSave, SuccessMessage.DATA_SAVED);
                } else {
                    const bannerSave = await banner.findByIdAndUpdate({ _id: editBanner._id }, { $set: req.body }, { new: true });
                    response(res, SuccessCode.SUCCESS, bannerSave, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    deleteBanner: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let blog = await banner.findById({ _id: req.params.id });
                if (!blog) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let blogDelete = await banner.findByIdAndDelete({ _id: blog._id });
                    response(res, SuccessCode.SUCCESS, blogDelete, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :bannerList
     * Description   : bannerList
     *
     * @return response
    */
    bannerList: async (req, res) => {
        try {
            if (req.params.bannerCategory) {
                const data = await banner.find({ bannerCategory: req.params.bannerCategory });
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
                }
            } else {
                response(res, SuccessCode.SUCCESS, [], SuccessMessage.DATA_FOUND);
            }

        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :addTrusted_client
     * Description   : addTrusted_client
     *
     * @return response
    */
    addTrusted_client: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                if (req.file) {
                    let data = await commonFunction.uploadFile(req.file)
                    req.body.image = data.Location;
                }
                const clientSave = new trustedClient({
                    clientName: req.body.clientName,
                    image: req.body.image,
                });
                const saveClient = await clientSave.save();
                response(res, SuccessCode.SUCCESS, saveClient, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :trustedClientList
     * Description   : trustedClientList
     *
     * @return response
    */
    trustedClientList: async (req, res) => {
        try {
            const data = await trustedClient.find();
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :createReport
     * Description   : createReport
     *
     * @return response
    */
    createReport: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    const reportSave = new report({
                        project: data._id,
                        user: userid,
                        title: req.body.title,
                        photoLayout: req.body.photoLayout,
                    });
                    const saveReport = await reportSave.save();
                    response(res, SuccessCode.SUCCESS, saveReport, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :reportList
     * Description   : reportList
     *
     * @return response
    */
    listReport: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await report.find({ user: user._id });
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :reportList
     * Description   : reportList
     *
     * @return response
    */
    reportList: async (req, res) => {
        try {
            const data = await report.find({ project: req.params.projectId });
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :viewReport
     * Description   : viewReport
     *
     * @return response
    */
    viewReport: async (req, res) => {
        try {
            const data = await report.findOne({ _id: req.params._id, project: req.body.projectId });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :deleteReport
     * Description   : deleteReport
     *
     * @return response
    */
    deleteReport: async (req, res) => {
        try {
            const data = await report.findOne({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let delete1 = await report.findByIdAndDelete({ _id: data._id })
                response(res, SuccessCode.SUCCESS, delete1, SuccessMessage.DELETE_SUCCESS);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :createSectionOnReport
     * Description   : createSectionOnReport
     *
     * @return response
    */
    createSectionOnReport: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await report.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let total = await reportSection.count();
                    let NO = total + 1;
                    let title = `Section ${NO}`;
                    const obj = new reportSection({
                        report: data._id,
                        title: title,
                        user: userid,
                    });
                    const saveReport = await obj.save();
                    response(res, SuccessCode.SUCCESS, saveReport, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :viewReport
     * Description   : viewReport
     *
     * @return response
    */
    viewSectionOnReport: async (req, res) => {
        try {
            const data = await reportSection.findOne({ _id: req.params._id, report: req.body.reportId });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :editSectionOnReport
     * Description   : editSectionOnReport
     *
     * @return response
    */
    editSectionOnReport: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await reportSection.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let project2 = await reportSection.findByIdAndUpdate({ _id: req.params._id }, { $set: req.body }, { new: true });
                    response(res, SuccessCode.SUCCESS, project2, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :deleteSectionOnReport
     * Description   : deleteSectionOnReport
     *
     * @return response
    */
    deleteSectionOnReport: async (req, res) => {
        try {
            const data = await reportSection.findOne({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let delete1 = await reportSection.findByIdAndDelete({ _id: data._id })
                response(res, SuccessCode.SUCCESS, delete1, SuccessMessage.DELETE_SUCCESS);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :addImageSecton
     * Description   : addImageSecton
     *
     * @return response
    */
    addImageSecton: async (req, res) => {
        const userid = req.user?._id;
        const user = await User.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await reportSection.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    const { files } = req;
                    let obj = {}
                    if (req.files) {
                        for (let i = 0; i < req.files.length; i++) {
                            let data = await commonFunction.uploadFile(req.files[i])
                            obj = {
                                image: data.Location
                            }
                            await reportSection.findByIdAndUpdate({ _id: req.params._id }, { $push: { images: obj } }, { new: true });
                        }
                    }
                    const data1 = await reportSection.findById({ _id: req.params._id });
                    response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :addImageSecton
     * Description   : addImageSecton
     *
     * @return response
    */
    addDescriptionOnImageSection: async (req, res, next) => {
        try {
            const data = await reportSection.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let desc = await reportSection.findOneAndUpdate({ 'images._id': req.body.imageId }, { $set: { 'images.$.description': req.body.description } }, { new: true })
                response(res, SuccessCode.SUCCESS, desc, SuccessMessage.DATA_SAVED);
            }
        }
        catch (error) {
            console.log(error);
            return next(error);
        }
    },
}