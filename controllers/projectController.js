const { User, validate } = require("../models/user");
const { Adminschema } = require("../models/admin");
const { createProject } = require("../models/createproject");
const { notification } = require('../models/notification');
const { transactiondetail } = require("../models/transactionStatus");
const messageschema = require("../models/messageSchema");
// const { MultipleImage } = require("../models/ImageSchema");
const { upload } = require("../helper/filehelper");
const { package } = require("../models/PackageSchema");
const storagePakagesModel = require('../models/pakagesStorage');
const plansModel = require('../models/plansModel');
const { commonResponse: response } = require('../helper/commonResponseHandler');
const { ErrorMessage } = require('../helper/message');
const { SuccessMessage } = require('../helper/message');
const { ErrorCode } = require('../helper/statusCode');
const { SuccessCode } = require('../helper/statusCode');
const commonfunction = require('../helper/commonFunction');
const useFunction = require('../helper/useFunction');
const error = require("mongoose/lib/error");
const multiparty = require('multiparty');
const e = require("express");
const { body } = require("express-validator");
const baseUrl = "http://localhost:1700/";
const JSZip = require('jszip');
const sharp = require("sharp");
const AdmZip = require("adm-zip");
const Jimp = require("jimp");
const fs = require('fs');
const fileSizeFormatter = (bytes, decimal) => {
    if (bytes === 0) {
        return "0 Bytes";
    }
    const dm = decimal || 2;
    const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "YB", "ZB"];
    const index = Math.floor(Math.log(bytes) / Math.log(1000));
    return (parseFloat((bytes / Math.pow(1000, index)).toFixed(dm)) + " " + sizes[index]);
};

module.exports = {
    uploadPhoto: async (req, res) => {
        try {
            let image;
            image = await commonfunction.imageUpload(req.body.image, req.body.imageName);
            response(res, SuccessCode.SUCCESS, image, SuccessMessage.DATA_SAVED);
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    uploadPhoto1: async (req, res) => {
        try {
            let image1;
            if (req.files) {
                for (let i = 0; i < req.files.length; i++) {
                    image1 = await commonfunction.imageUpload2(req.files[i]);

                }
            }
            response(res, SuccessCode.SUCCESS, image1, SuccessMessage.DATA_SAVED);
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    createproject: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let Admin = await Adminschema.findOne({ userType: "ADMIN" });
                const ProjectCreated = new createProject({
                    user: req.user._id,
                    projectname: req.body.projectname,
                    address1: req.body.address1,
                    address2: req.body.address2,
                    city: req.body.city,
                    state: req.body.state,
                    pcode: req.body.pcode,
                });
                const saveProject = await ProjectCreated.save();
                if (saveProject) {
                    var newOtp = req.body.projectname;
                    var text = `${saveProject.projectname} project has been create successfully.`;
                    let subject = `Project create`;
                    // let mailSend = await useFunction.sendMail2(Admin.email, Admin.name, User1.email, newOtp, User1.fname, subject, text);
                    // if (mailSend) {
                    const notificationCreate = new notification({
                        projectId: saveProject._id,
                        user: req.user._id,
                        assignUser: Admin._id,
                        title: "Project create",
                        body: `${saveProject.projectname} project has been create successfully.`,
                        notificationType: "PROJECT_CREATE"
                    });
                    await notificationCreate.save();
                    const data = await createProject.findById({ _id: saveProject._id });
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_SAVED);
                    // }
                }

            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    uploadImageInProject: async (req, res, next) => {
        try {
            const data = await createProject.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let totalImage = data.originalImage.length;
                let imageCount;
                if (totalImage > 0) {
                    for (let i = 0; i < 1; i++) {
                        let count = data.originalImage[totalImage - 1].imageId;
                        imageCount = Number(count)
                        console.log("imageCount=====98======", imageCount);
                    }
                } else {
                    imageCount = 0
                }
                let project2;
                const { files } = req;
                let image1 = [], image2 = []
                let paths = []
                if (req.files) {
                    for (let i = 0; i < req.files.length; i++) {
                        // console.log(req.files[i])
                        let extenstion = await getFileExtension(req.files[i].originalname);
                        let convertLowerCase = extenstion.toLowerCase()
                        let originalname = await changeOriExtensoin(req.files[i].originalname, convertLowerCase)
                        let filename = await changeExtension(req.files[i].filename, convertLowerCase)
                        let path = req.files[i].path
                        let obj1 = {
                            fieldname: 'image',
                            originalname: originalname,
                            encoding: '7bit',
                            mimetype: 'image/png',
                            destination: 'uploads/',
                            filename: filename,
                            path: path,
                            size: req.files[i].size
                        }
                        let data = await commonfunction.imageUpload2(obj1);
                        console.log(data);
                        console.log(data.a.Location);
                        let b = req.files[i].path
                        image1.push(data.a.Location)
                        image2.push(data.b.Location)
                        paths.push(b)
                    }
                }
                // console.log("image1=======================",image1);
                // console.log("image2=======================",image2);
                for (let i = 1; i <= image1.length; i++) {
                    const imageId = imageCount + i;
                    let obj2 = {
                        imageId: imageId,
                        image: image1[i - 1],
                        kbImage: image2[i - 1],
                        path: paths[i - 1]
                    }
                    project2 = await createProject.findByIdAndUpdate({ _id: req.params._id }, { $push: { originalImage: obj2 } }, { new: true });
                }
                response(res, SuccessCode.SUCCESS, project2, SuccessMessage.DATA_FOUND);
            }
        }
        catch (error) {
            console.log(error);
            return next(error);
        }
    },
    uploadImageInProjectBase64: async (req, res, next) => {
        try {
            const data = await createProject.findById({ _id: req.body._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let totalImage = data.originalImage.length;
                let project2;
                let a = [], image1 = [];
                let paths = []
                for (let i = 0; i < req.body.image.length; i++) {
                    const element = req.body.image[i];
                    const type = element.split(';')[0].split('/')[1];
                    let convertLowerCase = type.toLowerCase()
                    let name = `uploads/${req.body.imageName[i]}.${convertLowerCase}`
                    let save = await saveinFolder(element, name)
                    let data = await commonfunction.imageUpload3(element, req.body.imageName[i])
                    image1.push(data[1].Location)
                    a.push(data[0].Location)
                    paths.push(name)
                }
                for (let i = 1; i <= a.length; i++) {
                    let obj2 = {
                        imageId: totalImage + i,
                        image: image1[i - 1],
                        kbImage: a[i - 1],
                        path: paths[i - 1]
                    }
                    project2 = await createProject.findByIdAndUpdate({ _id: req.body._id }, { $push: { originalImage: obj2 } }, { new: true });
                }
                response(res, SuccessCode.SUCCESS, project2, SuccessMessage.DATA_FOUND);
            }
        }
        catch (error) {
            return next(error);
        }
    },
    addDescriptionOnProject: async (req, res) => {
        try {
            const data = await createProject.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let desc = await createProject.findByIdAndUpdate({ _id: data._id }, { $push: { description: req.body.description } }, { new: true })
                response(res, SuccessCode.SUCCESS, desc, SuccessMessage.DATA_SAVED);
            }
        }
        catch (error) {
            console.log(error);
            return next(error);
        }
    },
    addDescriptionOnImage: async (req, res) => {
        try {
            const data = await createProject.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let desc = await createProject.findOneAndUpdate({ 'originalImage._id': req.body.imageId }, { $set: { 'originalImage.$.description': req.body.description } }, { new: true })
                const notificationCreate = new notification({
                    projectId: desc._id,
                    user: req.user._id,
                    title: "Add description.",
                    body: "Add description on project successfully.",
                    notificationType: "ADD_DESC"
                })
                await notificationCreate.save();
                const data = await createProject.findById({ _id: desc._id });
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_SAVED);
            }
        }
        catch (error) {
            console.log(error);
            return next(error);
        }
    },
    getProjectsList: async (req, res) => {
        try {
            const data = await createProject.find({ user: req.user._id }).sort({ "createdAt": -1 });
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getProjectsListwithPagination: (req, res) => {
        try {
            var query = { user: req.user._id };
            var options = {
                page: parseInt(req.query.page) || 1,
                limit: parseInt(req.query.limit) || 5,
                sort: { createdAt: -1 }
            };
            createProject.paginate(query, options, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (result.docs.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    getCompleteProjectsListwithPagination: async (req, res) => {
        try {
            var query = { user: req.user._id, status: "COMPLETE" };
            var options = {
                page: parseInt(req.query.page) || 1,
                limit: parseInt(req.query.limit) || 5,
                sort: { createdAt: -1 }
            };
            createProject.paginate(query, options, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (result.docs.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getIncompleteProjectsListwithPagination: async (req, res) => {
        try {
            var query = { user: req.user._id, status: "INPROCESS" };
            var options = {
                page: parseInt(req.query.page) || 1,
                limit: parseInt(req.query.limit) || 5,
                sort: { createdAt: -1 }
            };
            createProject.paginate(query, options, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (result.docs.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getNewProjectsListwithPagination: async (req, res) => {
        try {
            var query = { user: req.user._id, status: "PENDING" };
            var options = {
                page: parseInt(req.query.page) || 1,
                limit: parseInt(req.query.limit) || 5,
                sort: { createdAt: -1 }
            };
            createProject.paginate(query, options, (err, result) => {
                if (err) {
                    response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                }
                else if (result.docs.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.DATA_FOUND);
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getproject: async (req, res) => {
        let project = await createProject.findOne({ _id: req.params.id }).populate({ path: 'Comment.user', select: 'fname userType image' });
        if (!project) {
            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
        } else {
            response(res, SuccessCode.SUCCESS, project, SuccessMessage.DATA_FOUND);
        }
    },
    editProject: async (req, res) => {
        const { projectname, address1, address2, city, state, pcode } = req.body;
        try {
            const updateProject = {};
            if (projectname) {
                updateProject.projectname = projectname;
            }
            if (address1) {
                updateProject.address1 = address1;
            }
            if (address2) {
                updateProject.address2 = address2;
            }
            if (city) {
                updateProject.city = city;
            }
            if (pcode) {
                updateProject.pcode = pcode;
            }
            if (state) {
                updateProject.state = state;
            }
            let project = await createProject.findById({ _id: req.params.id });
            if (!project) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            // console.log(req.user._id);
            if (project.user.toString() !== req.user._id) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            project = await createProject.findByIdAndUpdate(
                { _id: req.params.id },
                { $set: updateProject },
                { new: true }
            );
            const notificationCreate = new notification({
                projectId: project._id,
                user: req.user._id,
                title: "Project details edit.",
                body: "Project detailed edit successfully.",
                notificationType: "EDIT"
            })
            await notificationCreate.save();
            const data = await createProject.findById({ _id: project._id });
            res.json({ data });
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    deleteProject: async (req, res) => {
        try {
            let project = await createProject.findById({ _id: req.params.id });
            // console.log(project);
            if (!project) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            // console.log(project.user);
            if (project.user.toString() !== req.user._id) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            }
            let deleteProject = await createProject.findByIdAndDelete({ _id: req.params.id });
            if (deleteProject) {
                const notificationCreate = new notification({
                    user: req.user._id,
                    title: "Project Delete",
                    body: `${project.projectname} has been delete successfully.`,
                    notificationType: "DELETE"
                })
                await notificationCreate.save();
            }
            res.json({ Success: "Note is Deleted", project: project });
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getImages: async (req, res) => {
        try {
            const data = await createProject.find({ user: req.user._id });
            let i;
            const images = [];
            response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getEditedImages: async (req, res) => {
        const userId = req.user._id;
        try {
            const user = await User.findById({ _id: userId });
            if (user) {
                const project = await createProject.findById({ _id: req.params.id });
                if (project) {
                    response(res, SuccessCode.SUCCESS, project, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    sendmessage: async (req, res) => {
        const userId = req.user._id;
        try {
            const user = await User.findById({ _id: userId });
            if (user) {
                const message = new messageschema({
                    user: user._id,
                    project: req.params.id,
                    message: req.body.message,
                });
                const savemessage = await message.save();
                response(res, SuccessCode.SUCCESS, savemessage, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getmessagefromuser: async (req, res) => {
        const userId = req.user._id;
        try {
            const user = await User.findById({ _id: userId });
            if (user) {
                const message = await messageschema.find({ project: req.params.id });
                if (message) {
                    response(res, SuccessCode.SUCCESS, message, SuccessMessage.DATA_FOUND);
                } else {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    packagelist: async (req, res) => {
        const userId = req.user._id;
        try {
            const user = await User.findById({ _id: userId });
            if (user) {
                const packages = await package.find();
                if (packages.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, packages, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    uploadeditedImage: async (req, res, next) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await transactiondetail.findOne({ _id: req.params.id });
                if (data) {
                    let totalImage = data.editImage.length;
                    let imageCount;
                    if (totalImage > 0) {
                        for (let i = 0; i < 1; i++) {
                            let count = data.editImage[totalImage - 1].imageId;
                            imageCount = Number(count)
                            console.log("imageCount=====98======", imageCount);
                        }
                    } else {
                        imageCount = 0
                    }
                    let project2;
                    const { files } = req;
                    let image1 = [], image2 = []
                    let paths = []
                    if (req.files) {
                        for (let i = 0; i < req.files.length; i++) {
                            // console.log(req.files[i])
                            let extenstion = await getFileExtension(req.files[i].originalname);
                            let convertLowerCase = extenstion.toLowerCase()
                            let originalname = await changeOriExtensoin(req.files[i].originalname, convertLowerCase)
                            let filename = await changeExtension(req.files[i].filename, convertLowerCase)
                            let path = req.files[i].path
                            let obj1 = {
                                fieldname: 'image',
                                originalname: originalname,
                                encoding: '7bit',
                                mimetype: 'image/png',
                                destination: 'uploads/',
                                filename: filename,
                                path: path,
                                size: req.files[i].size
                            }
                            let data = await commonfunction.imageUpload2(obj1);
                            console.log(data);
                            console.log(data.a.Location);
                            let b = req.files[i].path
                            image1.push(data.a.Location)
                            image2.push(data.b.Location)
                            paths.push(b)
                        }
                    }
                    // console.log("image1=======================",image1);
                    // console.log("image2=======================",image2);
                    for (let i = 1; i <= image1.length; i++) {
                        const imageId = imageCount + i;
                        let obj2 = {
                            imageId: imageId,
                            image: image1[i - 1],
                            kbImage: image2[i - 1],
                            path: paths[i - 1]
                        }
                        project2 = await transactiondetail.findByIdAndUpdate({ _id: req.params.id }, { $push: { editImage: obj2 } }, { new: true });
                    }
                    response(res, SuccessCode.SUCCESS, project2, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            return next(error);
        }
    },
    // uploadeditedImage: async (req, res) => {
    //     const userid = req.user?._id;
    //     const admin = await Adminschema.findById({ _id: userid });
    //     try {
    //         if (!admin) {
    //             response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
    //         } else {
    //             const transaction = await transactiondetail.findOne({ _id: req.params.id });
    //             if (transaction) {
    //                 const { files } = req;
    //                 let image = []
    //                 let obj = {}
    //                 if (req.files) {
    //                     for (let i = 0; i < req.files.length; i++) {
    //                         // let data = await useFunction.getImageUrlPhase2(req.files[i].path)
    //                         // obj = data.secure_url;
    //                         let data = await commonfunction.uploadFile(req.files[i])
    //                         obj = data.Location;
    //                         image.push(obj)
    //                     }
    //                 }
    //                 let obj1 = {
    //                     editImage: image
    //                 }
    //                 const updateProject = await transactiondetail.findByIdAndUpdate({ _id: transaction._id }, { $push: { editImage: image } }, { new: true });
    //                 response(res, SuccessCode.SUCCESS, updateProject, SuccessMessage.UPDATE_SUCCESS);
    //             }
    //         }

    //     } catch (error) {
    //         console.log(error);
    //         response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
    //     }
    // },
    getEditedImagesOnUserside: async (req, res) => {
        const userid = req.user?._id;
        const admin = await User.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const project = await createProject.findById({ _id: req.params.id });
                if (project) {
                    response(res, SuccessCode.SUCCESS, project, SuccessMessage.UPDATE_SUCCESS);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getCompleteProjectsList: async (req, res) => {
        try {
            const data = await createProject.find({ user: req.user._id, status: "COMPLETE" });
            console.log(data);
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getIncompleteProjectsList: async (req, res) => {
        try {
            const data = await createProject.find({ user: req.user._id, status: "INPROCESS" });
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getNewProjectsList: async (req, res) => {
        try {
            const data = await createProject.find({ user: req.user._id, status: "PENDING" });
            if (data.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    commentOnProject: async (req, res) => {
        try {
            const data = await createProject.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                let d = new Date();
                let hr = d.getHours();
                let min = d.getMinutes();
                let min1;
                let date = d.getDate();
                let date1;
                let month = months[d.getMonth()];
                let year = d.getFullYear();
                if (date < 10) {
                    date1 = '' + 0 + date;
                }
                else {
                    date1 = date
                }
                if (hr < 10) {
                    hr1 = '' + 0 + hr;
                } else {
                    hr1 = hr
                }
                if (min < 10) {
                    min1 = '' + 0 + min;
                } else {
                    min1 = min
                }
                let fullDate = `${date1}-${month}-${year} ${hr1}:${min1}`;
                let obj = {
                    user: req.user._id,
                    Comment: req.body.Comment,
                    date: fullDate
                }
                let desc = await createProject.findByIdAndUpdate({ _id: data._id }, { $push: { Comment: obj } }, { new: true })
                const notificationCreate = new notification({
                    projectId: desc._id,
                    user: req.user._id,
                    title: "Comment on project.",
                    body: "Comment on project successfully.",
                    notificationType: "COMMENT"
                })
                await notificationCreate.save();
                const data1 = await createProject.findById({ _id: desc._id });
                response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
            }
        }
        catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    commentOnImage: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    let d = new Date();
                    let hr = d.getHours();
                    let min = d.getMinutes();
                    let min1;
                    let date = d.getDate();
                    let date1;
                    let month = months[d.getMonth()];
                    let year = d.getFullYear();
                    if (date < 10) {
                        date1 = '' + 0 + date;
                    }
                    else {
                        date1 = date
                    }
                    if (hr < 10) {
                        hr1 = '' + 0 + hr;
                    } else {
                        hr1 = hr
                    }
                    if (min < 10) {
                        min1 = '' + 0 + min;
                    } else {
                        min1 = min
                    }
                    let fullDate = `${date1}-${month}-${year} ${hr1}:${min1}`;
                    let obj = {
                        user: req.user._id,
                        fname: User1.fname,
                        userType: User1.userType,
                        image: User1.image,
                        Comment: req.body.Comment,
                        date: fullDate
                    }
                    if (data.assignType == "ALL") {
                        let desc = await createProject.findOneAndUpdate({ 'originalImage._id': req.body.imageId }, { $push: { 'originalImage.$.Comment': obj } }, { new: true })
                        if (desc) {
                            const notificationCreate = new notification({
                                projectId: desc._id,
                                assignUser: desc.editor,
                                title: "Comment on project image.",
                                body: "Comment on project image successfully.",
                                notificationType: "COMMENT"
                            })
                            await notificationCreate.save();
                            const data1 = await createProject.findById({ _id: desc._id });
                            response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
                        } else {
                            const data1 = await createProject.findById({ _id: desc._id });
                            response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
                        }

                    } else if (data.assignType == "SELECTED") {
                        let desc = await createProject.findOneAndUpdate({ 'assignImage._id': req.body.imageId }, { $push: { 'assignImage.$.Comment': obj } }, { new: true })
                        if (desc) {
                            const notificationCreate = new notification({
                                projectId: desc._id,
                                assignUser: desc.editor,
                                title: "Comment on project image.",
                                body: "Comment on project image successfully.",
                                notificationType: "COMMENT"
                            })
                            await notificationCreate.save();
                            const data1 = await createProject.findById({ _id: desc._id });
                            response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
                        } else {
                            const data1 = await createProject.findById({ _id: desc._id });
                            response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
                        }
                    }
                }
            }
        }
        catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    storagePakageslist: async (req, res) => {
        try {
            const storagePakages = await storagePakagesModel.find();
            if (storagePakages.length == 0) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, storagePakages, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    viewStoragePakage: async (req, res) => {
        let storagePakages = await storagePakagesModel.findOne({ _id: req.params.id });
        if (!storagePakages) {
            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
        } else {
            response(res, SuccessCode.SUCCESS, storagePakages, SuccessMessage.DATA_FOUND);
        }
    },
    userStoragePakages: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const storagePakages = await plansModel.findOne({ userId: User1._id, packageType: "STORAGE", status: "ACTIVE" });
                if (!storagePakages) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NO_STORAGE_PACKAGE);
                } else {
                    response(res, SuccessCode.SUCCESS, storagePakages, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    sendSelectedImage: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            console.log(User1);
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const project = await createProject.findById({ _id: req.body.id });
                let Admin = await Adminschema.findOne({ userType: "ADMIN" });
                if (project) {
                    if (req.body.selectType == "ALL") {
                        let desc = await createProject.findByIdAndUpdate({ _id: req.body.id }, { $set: { assignType: "ALL", status: "PENDING" } }, { new: true });
                        if (desc) {
                            const notificationCreate = new notification({
                                projectId: project._id,
                                user: project.user,
                                assignUser: Admin._id,
                                title: "Project image for editing.",
                                body: "All Project image has been send by user for editing.",
                                notificationType: "IMAGE_EDITING"
                            })
                            await notificationCreate.save();
                            response(res, SuccessCode.SUCCESS, desc, SuccessMessage.DATA_SAVED);
                        }
                    } else if (req.body.selectType == "SELECTED") {
                        if (req.body.selectedImage.length == 0) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NO_PHOTO_SELECTED);

                        } else {
                            for (let i = 0; i < req.body.selectedImage.length; i++) {
                                for (let j = 0; j < project.originalImage.length; j++) {
                                    const selectedImage = req.body.selectedImage[i];
                                    const projectImage = project.originalImage[j]._id;
                                    console.log((selectedImage == projectImage), "selectedImage == projectImage", selectedImage, "=======", projectImage);
                                    if (selectedImage == projectImage) {
                                        let obj = {
                                            imageId: project.originalImage[j].imageId,
                                            image: project.originalImage[j].image,
                                            description: project.originalImage[j].description,
                                        }
                                        // let obj1 = project.originalImage[j].image;
                                        await createProject.findOneAndUpdate({ _id: project._id }, { $push: { assignImage: obj } }, { new: true })
                                    }
                                }
                            }
                            let desc = await createProject.findOneAndUpdate({ _id: project._id }, { $set: { assignType: "SELECTED", status: "PENDING" } }, { new: true })
                            if (desc) {
                                const notificationCreate = new notification({
                                    projectId: project._id,
                                    user: project.user,
                                    assignUser: Admin._id,
                                    title: "Project image for editing.",
                                    body: "Selected Project image has been send by user for editing.",
                                    notificationType: "IMAGE_EDITING"
                                })
                                await notificationCreate.save();
                                response(res, SuccessCode.SUCCESS, desc, SuccessMessage.DATA_SAVED);
                            }
                        }
                    }
                } else {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    uploadImage: async (req, res) => {
        try {
            let a = [];
            for (let i = 0; i < req.body.image.length; i++) {
                const element = req.body.image[i];
                let image = await useFunction.getSecureUrl(element)
                a.push(image)
            }
            response(res, SuccessCode.SUCCESS, a, SuccessMessage.DATA_FOUND);

        } catch (error) {
            console.log("526 ==>", error.message);
        }
    },
    deleteCommentOnImage: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    for (let i = 0; i < data.originalImage.length; i++) {
                        const element = data.originalImage[i]._id;
                        if (element == req.body.imageId) {
                            for (let j = 0; j < data.originalImage[i].Comment.length; j++) {
                                const element1 = data.originalImage[i].Comment[j];
                                if (element1._id == req.body.CommentId) {
                                    let desc = await createProject.findOneAndUpdate({ 'originalImage._id': element }, { $pull: { 'originalImage.0.Comment': { _id: req.body.CommentId } } }, { new: true })
                                    response(res, SuccessCode.SUCCESS, desc, SuccessMessage.DATA_SAVED);
                                }
                            }
                        }
                    }

                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    deleteParticularImage: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    for (let i = 0; i < data.originalImage.length; i++) {
                        if (data.originalImage[i].imageId == req.query.imageId) {
                            // await createProject.findOneAndUpdate({ 'originalImage._id': data.originalImage[i]._id }, { $pull: { 'originalImage': { imageId: req.query.imageId } } }, { new: true });
                            await createProject.findOneAndUpdate({ 'originalImage._id': req.query.imageid }, { $pull: { 'originalImage': { imageId: req.query.imageId, _id: req.query.imageid } } }, { new: true });
                        }
                    }
                    const data1 = await createProject.findById({ _id: req.params._id });
                    response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log("974======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    addoriginalDriveLink: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id, user: User1._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let obj = {
                        linkTitle: req.body.linkTitle,
                        link: req.body.link,
                        totalImage: req.body.totalImage
                    }
                    let updateProject = await createProject.findByIdAndUpdate({ _id: data._id }, { $push: { originalDriveLink: obj } }, { new: true })
                    if (updateProject) {
                        response(res, SuccessCode.SUCCESS, updateProject, SuccessMessage.UPDATE_SUCCESS);
                    }
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    deleteOriginallink: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id, user: User1._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let updateProject = await createProject.findByIdAndUpdate({ _id: data._id }, { $pull: { 'originalDriveLink': { _id: req.query.linkId } } }, { new: true })
                    if (updateProject) {
                        response(res, SuccessCode.SUCCESS, updateProject, SuccessMessage.UPDATE_SUCCESS);
                    }
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    deleteOriginallink1: async (req, res) => {
        try {
            const data = await createProject.findById({ _id: req.params._id });
            if (!data) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                // db.collection_name.update({ _id: 1234 }, { $unset : { description : 1} })
                let updateProject = await createProject.update({ _id: data._id }, { $unset: { description: 1 } })
                if (updateProject) {
                    response(res, SuccessCode.SUCCESS, updateProject, SuccessMessage.UPDATE_SUCCESS);
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    totalImage: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.find({ user: User1._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let totalImage = 0;
                    for (let i = 0; i < data.length; i++) {
                        let element = data[i].originalImage.length;
                        totalImage = totalImage + element
                    }
                    response(res, SuccessCode.SUCCESS, totalImage, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    addProjectType: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await User.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id, user: User1._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let updateProject = await createProject.findByIdAndUpdate({ _id: data._id }, { $set: { projectType: req.body.projectType } }, { new: true })
                    if (updateProject) {
                        response(res, SuccessCode.SUCCESS, updateProject, SuccessMessage.UPDATE_SUCCESS);
                    }
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    // downloadAllImage: async (req, res) => {
    //     try {
    //         let project = await createProject.findOne({ _id: req.params._id });
    //         if (!project) {
    //             response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
    //         } else {
    //             let findUser = await User.findById({ _id: project.user });
    //             const images = []
    //             for (let i = 0; i < 45; i++) {
    //                 const element = project.originalImage[i].path;
    //                 images.push(element)
    //             }
    //             console.log(images)
    //             var zip = new JSZip();
    //             var count = 0;
    //             let name = project.projectname.replace(/[^\w]/g, "_");
    //             images.forEach(function (url, i) {
    //                 var filename = images[i];
    //                 filename = filename
    //                     .replace(/[\/\*\|\:\<\>\?\"\\]/gi, "-")
    //                     .replace("httpssequenceimagestaging.blob.core.windows.netretouch", "");
    //                 for (const image of images) {
    //                     const imageData = fs.readFileSync(image);
    //                     zip.file(image, imageData, { binary: true });
    //                 }
    //                 count++;
    //                 console.log("images[i]", filename, "count === images.length", count, "===", images.length)
    //                 if (count === images.length) {
    //                     new Promise((resolve, reject) => {
    //                         zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
    //                             .pipe(fs.createWriteStream(`ZipFolder/${findUser.fname}-${name}.zip`))
    //                             .on('finish', function () {
    //                                 console.log(`${findUser.fname}-${name}.zip save.`);
    //                                 response(res, SuccessCode.SUCCESS, `${findUser.fname}-${name}.zip`, SuccessMessage.DATA_SAVED);
    //                                 resolve();
    //                             });
    //                     })


    //                     // zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
    //                     //     .pipe(fs.createWriteStream(`ZipFolder/${findUser.fname}-${name}.zip`))
    //                     //     .on('finish', function () {
    //                     //         console.log(`${findUser.fname}-${name}.zip save.`);
    //                     //         response(res, SuccessCode.SUCCESS, `${findUser.fname}-${name}.zip`, SuccessMessage.DATA_SAVED);
    //                     //     })
    //                 }
    //             });
    //         }
    //     } catch (error) {
    //         console.log("964======================================", error);
    //         response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
    //     }
    // },
    downloadAllImage: async (req, res) => {
        try {
            let project = await createProject.findOne({ _id: req.params._id });
            if (!project) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let findUser = await User.findById({ _id: project.user });
                const images = []
                for (let i = 0; i < project.originalImage.length; i++) {
                    const element = project.originalImage[i].path;
                    images.push(element)
                }
                let name = project.projectname.replace(/[^\w]/g, "_");
                const zip = new AdmZip();
                const outputFile = `ZipFolder/${findUser.fname}-${name}.zip`;
                var count = 0;
                images.forEach(function (url, i) {
                    var filename = images[i];
                    zip.addLocalFile(filename);
                    count++;
                    if (count === images.length) {
                        zip.writeZip(outputFile);
                        console.log(`Created ${outputFile} successfully`);
                    }
                });
                response(res, SuccessCode.SUCCESS, `${findUser.fname}-${name}.zip`, SuccessMessage.DATA_SAVED);
            }

        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    // downloadOrderImage: async (req, res) => {
    //     try {
    //         let transaction = await transactiondetail.findById({ _id: req.params._id });
    //         if (!transaction) {
    //             response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
    //         } else {
    //             let project = await createProject.findOne({ _id: transaction.project });
    //             if (!project) {
    //                 response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
    //             } else {
    //                 let findUser = await User.findById({ _id: project.user });
    //                 const images = []
    //                 for (let i = 0; i < project.originalImage.length; i++) {
    //                     if (!transaction.selectedImage[0]?._id) {
    //                         for (let j = 0; j < transaction.selectedImage.length; j++) {
    //                             if (((project.originalImage[i]._id).toString() == transaction.selectedImage[j]) == true) {
    //                                 const element = project.originalImage[i].path;
    //                                 images.push(element)
    //                             }
    //                         }
    //                     } else {
    //                         for (let j = 0; j < transaction.selectedImage.length; j++) {
    //                             if (((project.originalImage[i]._id).toString() == transaction.selectedImage[j]._id) == true) {
    //                                 const element = project.originalImage[i].path;
    //                                 images.push(element)
    //                             }
    //                         }
    //                     }
    //                 }
    //                 console.log("+================" + images);
    //                 var zip = new JSZip();
    //                 var count = 0;
    //                 images.forEach(function (url, i) {
    //                     var filename = images[i];
    //                     filename = filename
    //                         .replace(/[\/\*\|\:\<\>\?\"\\]/gi, "-")
    //                         .replace("httpssequenceimagestaging.blob.core.windows.netretouch", "");
    //                     for (const image of images) {
    //                         const imageData = fs.readFileSync(image);
    //                         zip.file(image, imageData, { binary: true });
    //                     }
    //                     count++;
    //                     if (count === images.length) {
    //                         zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
    //                             .pipe(fs.createWriteStream(`ZipFolder/${findUser.fname}-${transaction.transactionId}.zip`))
    //                             .on('finish', function () {
    //                                 console.log(`${findUser.fname}-${transaction.transactionId}.zip save.`);
    //                                 response(res, SuccessCode.SUCCESS, `${findUser.fname}-${transaction.transactionId}.zip`, SuccessMessage.DATA_SAVED);
    //                             })
    //                     }
    //                 });
    //             }
    //         }
    //     } catch (error) {
    //         console.log("964======================================", error);
    //         response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
    //     }
    // },
    downloadOrderImage: async (req, res) => {
        try {
            let transaction = await transactiondetail.findById({ _id: req.params._id });
            if (!transaction) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let project = await createProject.findOne({ _id: transaction.project });
                if (!project) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let findUser = await User.findById({ _id: project.user });
                    const images = []
                    for (let i = 0; i < project.originalImage.length; i++) {
                        if (!transaction.selectedImage[0]?._id) {
                            for (let j = 0; j < transaction.selectedImage.length; j++) {
                                if (((project.originalImage[i]._id).toString() == transaction.selectedImage[j]) == true) {
                                    const element = project.originalImage[i].path;
                                    images.push(element)
                                }
                            }
                        } else {
                            for (let j = 0; j < transaction.selectedImage.length; j++) {
                                if (((project.originalImage[i]._id).toString() == transaction.selectedImage[j]._id) == true) {
                                    const element = project.originalImage[i].path;
                                    images.push(element)
                                }
                            }
                        }
                    }
                    const zip = new AdmZip();
                    const outputFile = `ZipFolder/${findUser.fname}-${transaction.transactionId}.zip`;
                    var count = 0;
                    images.forEach(function (url, i) {
                        var filename = images[i];
                        zip.addLocalFile(filename);
                        count++;
                        if (count === images.length) {
                            zip.writeZip(outputFile);
                            console.log(`Created ${outputFile} successfully`);
                        }
                    });
                    response(res, SuccessCode.SUCCESS, `${findUser.fname}-${transaction.transactionId}.zip`, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    // downloadEditImage: async (req, res) => {
    //     try {
    //         let transaction = await transactiondetail.findById({ _id: req.params._id });
    //         if (!transaction) {
    //             response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
    //         } else {
    //             let project = await createProject.findOne({ _id: transaction.project });
    //             if (!project) {
    //                 response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
    //             } else {
    //                 let findUser = await User.findById({ _id: project.user });
    //                 const images = []
    //                 for (let j = 0; j < transaction.editImage.length; j++) {
    //                     const element = transaction.editImage[j].path;
    //                     images.push(element)
    //                 }
    //                 console.log("+================" + images);
    //                 var zip = new JSZip();
    //                 var count = 0;
    //                 images.forEach(function (url, i) {
    //                     var filename = images[i];
    //                     filename = filename
    //                         .replace(/[\/\*\|\:\<\>\?\"\\]/gi, "-")
    //                         .replace("httpssequenceimagestaging.blob.core.windows.netretouch", "");
    //                     for (const image of images) {
    //                         const imageData = fs.readFileSync(image);
    //                         zip.file(image, imageData, { binary: true });
    //                     }
    //                     count++;
    //                     if (count === images.length) {
    //                         zip.generateNodeStream({ type: 'nodebuffer', streamFiles: true })
    //                             .pipe(fs.createWriteStream(`ZipFolder/${findUser.fname}editImage${transaction.transactionId}.zip`))
    //                             .on('finish', function () {
    //                                 console.log(`${findUser.fname}editImage${transaction.transactionId}.zip save.`);
    //                                 response(res, SuccessCode.SUCCESS, `${findUser.fname}editImage${transaction.transactionId}.zip`, SuccessMessage.DATA_SAVED);
    //                             })
    //                     }
    //                 });
    //             }
    //         }
    //     } catch (error) {
    //         console.log("964======================================", error);
    //         response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
    //     }
    // },
    downloadEditImage: async (req, res) => {
        try {
            let transaction = await transactiondetail.findById({ _id: req.params._id });
            if (!transaction) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
            } else {
                let project = await createProject.findOne({ _id: transaction.project });
                if (!project) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let findUser = await User.findById({ _id: project.user });
                    const images = []
                    for (let j = 0; j < transaction.editImage.length; j++) {
                        const element = transaction.editImage[j].path;
                        images.push(element)
                    }
                    console.log("+================" + images);
                    const zip = new AdmZip();
                    const outputFile = `ZipFolder/${findUser.fname}editImage${transaction.transactionId}.zip`;
                    var count = 0;
                    images.forEach(function (url, i) {
                        var filename = images[i];
                        zip.addLocalFile(filename);
                        count++;
                        if (count === images.length) {
                            zip.writeZip(outputFile);
                            console.log(`Created ${outputFile} successfully`);
                        }
                    });
                    response(res, SuccessCode.SUCCESS, `${findUser.fname}editImage${transaction.transactionId}.zip`, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
}
const getFileExtension = async (filename) => {
    const extension = filename.substring(filename.lastIndexOf('.') + 1, filename.length);
    return extension;
}
const changeExtension = async (filename, extension) => {
    let root = filename.substring(0, filename.length - extension.length)
    ext = extension.startsWith('.') ? extension : extension.length > 0 ? `${extension}` : ''
    return `${root}${ext}`
}
const changeOriExtensoin = async (filename, extension) => {
    let root = filename.substring(0, filename.length - extension.length)
    ext = extension.startsWith('.') ? extension : extension.length > 0 ? `${extension}` : ''
    return `${root}${ext}`
}
// Base64 string
const saveinFolder = async (data, name) => {
    const base64Data = data.replace(/^data:image\/\w+;base64,/, "");
    let res = await Jimp.read(Buffer.from(base64Data, 'base64'))
    res.quality(5).write(name)
}

