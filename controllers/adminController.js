const router = require("express").Router();
const { Adminschema } = require("../models/admin");
const { User } = require("../models/user");
const { comments } = require('../models/comment')
const fetchuser = require("../middleware/fetchuser");
const { notification } = require('../models/notification')
const bcrypt = require("bcryptjs");
const { package } = require("../models/PackageSchema");
const { transactiondetail } = require("../models/transactionStatus");
const { createProject } = require("../models/createproject");
const { blogs } = require("../models/blogs");
const commonFunction = require('../helper/commonFunction');
const { commonResponse: response } = require('../helper/commonResponseHandler');
const { ErrorMessage } = require('../helper/message');
const { SuccessMessage } = require('../helper/message');
const { ErrorCode } = require('../helper/statusCode');
const { SuccessCode } = require('../helper/statusCode');
const jwt = require('jsonwebtoken');
const { upload } = require("../helper/filehelper");
const messageschema = require("../models/messageSchema");
const compressImages = require("compress-images")
const fs = require('fs');

const generateAuthToken = (user) => {
    const token = jwt.sign({ _id: user._id.toString() }, "thisisnewcourse");
    return token;
};
module.exports = {
    /**
     * Function Name :uploadPhoto
     * Description   : uploadPhoto
     *
     * @return response
    */
    uploadPhoto: async (req, res) => {
        try {
            let image, image1;
            if (req.file) {
                console.log(req.file)
                const image = req.file

                if (req.file.mimetype == "image/png" || req.file.mimetype == "image/jpeg" || req.file.mimetype == "image/jpeg") {
                    fs.readFile(req.file.path, function (error, data) {
                        if (error) throw error

                        const filePath = "temp-uploads/" + (new Date().getTime()) + "-" + req.file.originalname
                        const compressedFilePath = "uploads/"
                        const compression = 40

                        fs.writeFile(filePath, data, async function (error) {
                            if (error) {
                                throw error

                            } else {
                                await compressImages(filePath, compressedFilePath, { compress_force: false, statistic: true, autoupdate: true }, false,
                                    { jpg: { engine: "mozjpeg", command: ["-quality", compression] } },
                                    { png: { engine: "pngquant", command: ["--quality=" + compression + "-" + compression, "-o"] } },
                                    { svg: { engine: "svgo", command: "--multipass" } },
                                    { gif: { engine: "gifsicle", command: ["--colors", "64", "--use-col=web"] } },
                                    async function (error, completed, statistic) {
                                        console.log("-------------")
                                        console.log("=================error================", error)

                                        console.log("=======completed===============>", completed)
                                        console.log("====statistic==================>", statistic)
                                        console.log("-------------")
                                        image1 = await commonFunction.uploadFile(statistic.path_out_new,req.file.originalname);
                                        response(res, SuccessCode.SUCCESS, image1, "File has been compressed and saved.")
                                        fs.unlink(filePath, function (error) {
                                            if (error) throw error
                                        })
                                    })
                            }
                        })

                        fs.unlink(req.file.path, function (error) {
                            if (error) throw error
                        })
                    })
                } else {
                    response(res, SuccessCode.SUCCESS, {}, "Please select an image")
                }
            } else {
                response(res, SuccessCode.SUCCESS, {}, "Please select an image")
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :login
     * Description   : login of user 
     *
     * @return response
    */
    adminLogin: async (req, res) => {
        try {
            const user = await Adminschema.findOne({ email: req.body.email });
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const validPassword = await bcrypt.compareSync(req.body.password, user.password);
                if (!validPassword) {
                    response(res, ErrorCode.INVALID_CREDENTIALS, [], ErrorMessage.INVALID_CREDENTIAL)
                }
                else {
                    const authtoken = generateAuthToken(user);
                    var result = {
                        _id: user._id,
                        email: user.email,
                        userType: user.userType,
                        token: authtoken
                    };
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.LOGIN_SUCCESS);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :editProfile
     * Description   : editProfile
     *
     * @return response
    */
    editProfile: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                if (req.file) {
                    let data = await commonFunction.uploadFile(req.file)
                    req.body.image = data.Location;
                    const Save = await Adminschema.findByIdAndUpdate({ _id: user._id }, { $set: req.body }, { new: true });
                    response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                } else {
                    const Save = await Adminschema.findByIdAndUpdate({ _id: user._id }, { $set: req.body }, { new: true });
                    response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :editemployeeManagerProfile
     * Description   : editemployeeManagerProfile
     *
     * @return response
    */
    editemployeeManagerProfile: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let employee = await Adminschema.findById({ _id: req.body._id });
                if (employee) {
                    if (req.body.password) {
                        req.body.password = bcrypt.hashSync(req.body.password);
                    }
                    const Save = await Adminschema.findByIdAndUpdate({ _id: employee._id }, { $set: req.body }, { new: true });
                    if (Save) {
                        response(res, SuccessCode.SUCCESS, Save, SuccessMessage.DATA_SAVED);
                    }
                } else {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    verifyOTP: async (req, res) => {
        try {
            let userResult = await Adminschema.findOne({ email: req.body.email });
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, {}, ErrorMessage.USER_NOT_FOUND);
            }
            else {
                if (Date.now() > userResult.expireIn) {
                    response(res, ErrorCode.INVALID_CREDENTIALS, {}, ErrorMessage.OTP_EXPIRED);
                }
                else if (userResult.otp != req.body.otp) {
                    response(res, ErrorCode.INVALID_CREDENTIALS, {}, ErrorMessage.INVALID_OTP);
                } else {
                    var updateResult = await Adminschema.findOneAndUpdate({ email: userResult.email }, { otpVerified: true }, { new: true })
                    var result = {
                        _id: updateResult._id,
                        email: updateResult.email,
                    };
                    response(res, SuccessCode.SUCCESS, result, SuccessMessage.VERIFY_OTP);
                }
            }
        }
        catch (error) {
            console.log("error", error)
            response(res, ErrorCode.SOMETHING_WRONG, error, ErrorMessage.SOMETHING_WRONG);
        }
    },
    forgotPassword: async (req, res) => {
        try {
            var userResult = await Adminschema.findOne({ email: req.body.email },)
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, {}, ErrorMessage.NOT_REGISTERED);
            } else {
                var otp3 = commonFunction.getOTP();
                var otpTime4 = new Date().getTime() + 5 * 60 * 1000;
                commonFunction.sendMail(req.body.email, otp3, userResult.name, (error, otpSent) => {
                    if (error) {
                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else {
                        Adminschema.findOneAndUpdate({ email: req.body.email, status: { $ne: "DELETE" }, }, { $set: { otp: otp3, expireIn: otpTime4, otpVerified: false } }, { new: true }, (error, result) => {
                            if (error) {
                                response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                            } else {
                                response(res, SuccessCode.SUCCESS, result, SuccessMessage.OTP_SEND);
                            }
                        })
                    }
                });
            }
        } catch (error) {
            console.log("110====>", error);
            response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.SOMETHING_WRONG);
        }
    },
    resetPassword: (req, res) => {
        try {
            Adminschema.findOne({ email: req.body.email }, (err, result) => {
                if (err) {
                    response(res, ErrorCode.INTERNAL_ERROR, {}, ErrorMessage.INTERNAL_ERROR);
                }
                else if (!result) {
                    response(res, ErrorCode.NOT_FOUND, {}, ErrorMessage.USER_NOT_FOUND);
                }
                else {
                    if (result.otpVerified == true) {
                        if (req.body.newPassword == req.body.confirmPassword) {
                            Adminschema.findOneAndUpdate({ _id: result._id }, { $set: { password: bcrypt.hashSync(req.body.newPassword) } }, { new: true }, (updateErr, updateResult) => {
                                if (updateErr) {
                                    response(res, ErrorCode.INTERNAL_ERROR, {}, ErrorMessage.INTERNAL_ERROR);
                                }
                                else {
                                    response(res, SuccessCode.SUCCESS, updateResult, SuccessMessage.RESET_SUCCESS);
                                }
                            })
                        }
                        else {
                            response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.NOT_MATCH);
                        }
                    } else {
                        response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.FIRST_VERIFY_OTP);
                    }
                }
            })
        }
        catch (error) {
            response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :changePassword
     * Description   : changePassword of user 
     *
     * @return response
    */
    changePassword: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            }
            if (!bcrypt.compareSync(req.body.oldPassword, user.password)) {
                response(res, ErrorCode.NOT_FOUND, {}, ErrorMessage.PASSWORD_NOT_MATCHED);
            } else {
                let updated = await Adminschema.findByIdAndUpdate({ _id: user._id }, { $set: { password: bcrypt.hashSync(req.body.newPassword) } }, { new: true });
                if (updated) {
                    response(res, SuccessCode.SUCCESS, updated, SuccessMessage.PASSWORD_UPDATE);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.WENT_WRONG, {}, ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :create_employee
     * Description   : create_employee
     *
     * @return response
    */
    createManager: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                Adminschema.findOne({ email: req.body.email }, async (err, result) => {
                    if (err) {
                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result) {
                        response(res, ErrorCode.ALREADY_EXIST, result, ErrorMessage.EMAIL_EXIST);
                    }
                    else {
                        req.body.email = req.body.email;
                        req.body.password = bcrypt.hashSync(req.body.password);
                        req.body.name = req.body.name;
                        req.body.userType = req.body.userType;
                        req.body.image = req.body.image;
                        req.body.mobileNumber = req.body.mobileNumber;
                        req.body.address = req.body.address;
                        let save = await Adminschema.create(req.body);
                        response(res, SuccessCode.SUCCESS, save, SuccessMessage.SIGNUP_SUCCESSFULLY);
                    }
                })
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :create_employee
     * Description   : create_employee
     *
     * @return response
    */
    create_employee: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: { $ne: "EMPLOYEE" } });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                Adminschema.findOne({ email: req.body.email }, async (err, result) => {
                    if (err) {
                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result) {
                        response(res, ErrorCode.ALREADY_EXIST, result, ErrorMessage.EMAIL_EXIST);
                    }
                    else {
                        req.body.email = req.body.email;
                        req.body.password = bcrypt.hashSync(req.body.password);
                        req.body.name = req.body.name;
                        req.body.userType = "EMPLOYEE";
                        let save = await Adminschema.create(req.body);
                        response(res, SuccessCode.SUCCESS, save, SuccessMessage.SIGNUP_SUCCESSFULLY);
                    }
                })
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :deleteEmp_manager
     * Description   : deleteEmp_manager
     *
     * @return response
    */
    deleteEmp_manager: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let blog = await Adminschema.findById({ _id: req.params.id, userType: { $ne: "ADMIN" } });
                if (!blog) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let blogDelete = await Adminschema.findByIdAndDelete({ _id: blog._id });
                    response(res, SuccessCode.SUCCESS, blogDelete, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :create_package
     * Description   : create_package
     *
     * @return response
    */
    create_package: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                package.findOne({ packageName: req.body.packageName }, async (err, result) => {
                    if (err) {
                        response(res, ErrorCode.INTERNAL_ERROR, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result) {
                        response(res, ErrorCode.ALREADY_EXIST, result, ErrorMessage.PACKAGE_EXIST);
                    }
                    else {
                        const packagecreate = new package({
                            packageName: req.body.packageName,
                            amount: req.body.amount,
                            validity: req.body.validity,
                            imageQuantity: req.body.imageQuantity,
                        });
                        const savePackage = await packagecreate.save();
                        response(res, SuccessCode.SUCCESS, savePackage, SuccessMessage.SIGNUP_SUCCESSFULLY);
                    }
                })
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :packagelist
     * Description   : packagelist
     *
     * @return response
    */
    packagelist: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const packages = await package.find();
                if (packages.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, packages, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :delete_package
     * Description   : delete_package
     *
     * @return response
    */
    delete_package: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let result = await package.findOne({ _id: req.params.id });
                if (!result) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let deletePackage = await package.findByIdAndDelete({ _id: result._id });
                    if (deletePackage) {
                        response(res, SuccessCode.SUCCESS, deletePackage, SuccessMessage.DATA_FOUND);
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :blogs
     * Description   : blogs
     *
     * @return response
    */
    blogs: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                if (req.file) {
                    let data = await commonFunction.uploadFile(req.file)
                    req.body.image = data.Location;
                    // req.body.image = await commonFunction.uploadProfileImage(req.file.path);
                }
                var a = req.body.title;
                var b = a.toLowerCase().replace(/ /g, '-').replace(/[^\w-]+/g, '');
                console.log(a, "==============", b);
                const blog = new blogs({
                    user: userid,
                    title: req.body.title,
                    slug: b,
                    image: req.body.image,
                    blogs: req.body.blogs,
                });
                const saveblogs = await blog.save();
                response(res, SuccessCode.SUCCESS, saveblogs, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    deleteBlog: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let blog = await blogs.findById({ _id: req.params.id });
                if (!blog) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let blogDelete = await blogs.findByIdAndDelete({ _id: blog._id });
                    response(res, SuccessCode.SUCCESS, blogDelete, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :editBlog
     * Description   : editBlog
     *
     * @return response
    */
    editBlog: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let editBanner = await blogs.findOne({ slug: req.params.slug });
                if (req.file) {
                    let data = await commonFunction.uploadFile(req.file)
                    req.body.image = data.Location;

                    req.body.image = await commonFunction.uploadProfileImage(req.file.path);
                    let obj = {
                        title: req.body.title || editBanner.title,
                        image: req.body.image || editBanner.image,
                        blogs: req.body.blogs || editBanner.blogs,
                    }
                    const bannerSave = await blogs.findByIdAndUpdate({ _id: editBanner._id }, { $set: obj }, { new: true });
                    response(res, SuccessCode.SUCCESS, bannerSave, SuccessMessage.DATA_SAVED);
                } else {
                    let obj = {
                        title: req.body.title || editBanner.title,
                        image: editBanner.image,
                        blogs: req.body.blogs || editBanner.blogs,
                    }
                    const bannerSave = await blogs.findByIdAndUpdate({ _id: editBanner._id }, { $set: obj }, { new: true });
                    response(res, SuccessCode.SUCCESS, bannerSave, SuccessMessage.DATA_SAVED);
                }

            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
    * Function Name :getAdmin_companyProfile
    * Description   : getAdmin_companyProfile
    *
    * @return response
   */
    getAdmin_companyProfile: async (req, res) => {
        const userId = req.user._id;
        try {
            const user = await Adminschema.findById({ _id: userId }).select("-password");
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
            } else {
                response(res, SuccessCode.SUCCESS, user, SuccessMessage.DATA_FOUND);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
    * Function Name :getalluser
    * Description   : getalluser
    *
    * @return response
   */
    getalluser: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const user = await User.find();
                let users = [];
                if (user.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    for (let i = 0; i < user.length; i++) {
                        const element = user[i]._id;
                        let projectCount = await createProject.findOne({ user: element }).count();
                        let obj = {
                            user: user[i],
                            project: projectCount
                        }
                        users.push(obj);
                    }
                    response(res, SuccessCode.SUCCESS, users, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :getparticularUser
     * Description   : getparticularUser
     *
     * @return response
    */
    getparticularUser: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let user = await User.findById({ _id: req.params.id });
                if (!user) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let project = await createProject.findOne({ user: user._id }).count();
                    response(res, SuccessCode.SUCCESS, { user, project }, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {

        }
    },
    /**
     * Function Name :deleteUser
     * Description   : deleteUser
     *
     * @return response
    */
    deleteUser: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let deleteUser = await User.findById({ _id: req.params.id });
                if (!deleteUser) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let blogDelete = await User.findByIdAndDelete({ _id: deleteUser._id });
                    response(res, SuccessCode.SUCCESS, blogDelete, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getallemployee
     * Description   : getallemployee
     *
     * @return response
    */
    getallemployee: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const user = await Adminschema.find({ userType: { $ne: "ADMIN" } });
                if (user.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    let data = [];
                    for (let i = 0; i < user.length; i++) {
                        const element = user[i]._id;
                        let project = await createProject.find({ 'assignTransactionId.editor': element }).count();
                        let obj = {
                            user: user[i],
                            projectCount: project
                        }
                        data.push(obj)
                    }
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :getparticularEmployee
     * Description   : getparticularEmployee
     *
     * @return response
    */
    getparticularEmployee: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let user = await Adminschema.findById({ _id: req.params.id });
                if (!user) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, user, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {

        }
    },
    /**
     * Function Name :getallemployee
     * Description   : getallemployee
     *
     * @return response
    */
    getallManager: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const user = await Adminschema.find({ userType: "MANAGER" });
                if (user.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, user, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }

    },
    /**
     * Function Name :getparticularEmployee
     * Description   : getparticularEmployee
     *
     * @return response
    */
    getparticularManage: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let user = await Adminschema.findById({ _id: req.params.id, userType: "MANAGER" });
                if (!user) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, user, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {

        }
    },
    /**
     * Function Name :getProjectsList
     * Description   : getProjectsList
     *
     * @return response
    */
    getProjectsList: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid, userType: "ADMIN" });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                var query = { status: { $ne: "DELETE" } };
                var options = {
                    page: 1,
                    limit: 100,
                    sort: { createdAt: -1 },
                    populate: { path: 'user', select: 'email company fname lname userType' }
                };
                createProject.paginate(query, options, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result.docs.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result.docs, SuccessMessage.DATA_FOUND);
                    }
                })
            }
        }
        catch (error) {
            consol.log("errorerrorerror", error)
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getproject
     * Description   : getproject
     *
     * @return response
    */
    getproject: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let project = await createProject.findById({ _id: req.params.id });
                if (!project) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, project, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :updateStatus
     * Description   : updateStatus
     *
     * @return response
    */
    updateStatus: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            console.log("admin===>", admin)
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let assignedproject = await createProject.findOne({ _id: req.body.id });
                console.log("assignedproject", assignedproject)
                if (!assignedproject) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    assignedproject = await createProject.findByIdAndUpdate({ _id: assignedproject._id }, { $set: { status: req.body.status } }, { new: true });
                    response(res, SuccessCode.SUCCESS, assignedproject, SuccessMessage.DETAIL_GET);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    transactionStatusUpdate: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            console.log("admin===>", admin)
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let assignedproject = await transactiondetail.findOne({ _id: req.body.id });
                console.log("assignedproject", assignedproject)
                if (!assignedproject) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    assignedproject = await transactiondetail.findByIdAndUpdate({ _id: assignedproject._id }, { $set: { assignStatus: req.body.status } }, { new: true });
                    response(res, SuccessCode.SUCCESS, assignedproject, SuccessMessage.DETAIL_GET);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getCompleteProjectsList
     * Description   : getCompleteProjectsList
     *
     * @return response
    */
    getCompleteProjectsList: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.find({ status: "COMPLETE" }).populate('user');
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getIncompleteProjectsList
     * Description   : getIncompleteProjectsList
     *
     * @return response
    */
    getIncompleteProjectsList: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.find({ user: req.body.userId }, { status: "INPROCESS" });
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getNewProjectsList
     * Description   : getNewProjectsList
     *
     * @return response
    */
    getNewProjectsList: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.find({ user: req.body.userId }, { status: "PENDING" });
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :blogList
     * Description   : blogList
     *
     * @return response
    */
    blogList: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await blogs.find();
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, data, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :getBlog
     * Description   : getBlog
     *
     * @return response
    */
    getBlog: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let blog = await blogs.findOne({ slug: req.params.slug });
                if (!blog) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, blog, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :notificationListforAdmin
     * Description   : notificationListforAdmin
     *
     * @return response
    */
    notificationListforAdmin: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await notification.find({ $ne: { notificationType: "BROADCAST" } });
                const dataCount = await notification.find({ $ne: { notificationType: "BROADCAST" }, adminIsRead: false }).count();
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, { data, dataCount }, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    notificationStatus: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            const data1 = await notification.updateMany({ $ne: { notificationType: "BROADCAST" }, status: "ACTIVE" }, { $set: { adminIsRead: true } }, { new: true });
            if (data1) {
                var query = {};
                var options = {
                    sort: { createdAt: -1 }
                };
                notification.paginate(query, options, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result.docs.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result.docs, SuccessMessage.DATA_FOUND);
                    }
                })

            }
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :ComplaintList
     * Description   : ComplaintList
     *
     * @return response
    */
    ComplaintList: async (req, res) => {
        const userid = req.user?._id;
        try {
            const userResult = await Adminschema.findById({ _id: userid }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                var query = {};
                var options = {
                    sort: { createdAt: -1 },
                    populate: { path: "user" }
                };
                comments.paginate(query, options, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result.docs.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result.docs, SuccessMessage.DATA_FOUND);
                    }
                })
            }
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :userComplaintList
     * Description   : userComplaintList
     *
     * @return response
    */
    userComplaintList: async (req, res) => {
        const userid = req.user?._id;
        try {
            const userResult = await Adminschema.findById({ _id: userid }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                var query = { user: req.body.userId };
                var options = {
                    sort: { createdAt: -1 },
                    populate: { path: "user" }
                };
                comments.paginate(query, options, (err, result) => {
                    if (err) {
                        response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.INTERNAL_ERROR);
                    }
                    else if (result.docs.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                    }
                    else {
                        response(res, SuccessCode.SUCCESS, result.docs, SuccessMessage.DATA_FOUND);
                    }
                })
            }
        }
        catch (error) {
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    replyOnComplaintbyAdmin: async (req, res) => {
        const userid = req.user?._id;
        const adminData = await Adminschema.findById({ _id: userid });
        try {
            if (!adminData) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let queryReply = await comments.findById({ _id: req.params.id });
                if (queryReply) {
                    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    let d = new Date();
                    let hr = d.getHours();
                    let min = d.getMinutes();
                    let min1, desc;
                    let date = d.getDate();
                    let date1;
                    let month = months[d.getMonth()];
                    let year = d.getFullYear();
                    if (date < 10) {
                        date1 = '' + 0 + date;
                    }
                    else {
                        date1 = date
                    }
                    if (hr < 10) {
                        hr1 = '' + 0 + hr;
                    } else {
                        hr1 = hr
                    }
                    if (min < 10) {
                        min1 = '' + 0 + min;
                    } else {
                        min1 = min
                    }
                    let screenShot = [], obj1;
                    if (req.files) {
                        for (let i = 0; i < req.files.length; i++) {
                            let data = await commonFunction.getImageUrlPhase2(req.files[i].path)
                            obj1 = data.secure_url;
                            screenShot.push(obj1)
                        }
                    }
                    let fullDate = `${date1}-${month}-${year} ${hr1}:${min1}`;
                    let obj = {
                        admin: req.user._id,
                        fname: adminData.name,
                        userType: adminData.userType,
                        image: adminData.image,
                        screenShot: screenShot,
                        comment: req.body.comment,
                        date: fullDate
                    }
                    desc = await comments.findByIdAndUpdate({ _id: queryReply._id }, { $push: { reply: obj } }, { new: true });
                    if (desc) {
                        response(res, SuccessCode.SUCCESS, desc, SuccessMessage.DATA_SAVED);
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
    * Function Name :closeComplaintfromAdminSide
    * Description   : closeComplaintfromAdminSide
    *
    * @return response
   */
    closeComplaintfromAdminSide: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await comments.findOne({ _id: req.body._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    const updateData = await comments.findByIdAndUpdate({ _id: data._id }, { $set: { queryStatus: "CLOSE" } }, { new: true })
                    if (updateData) {
                        response(res, SuccessCode.SUCCESS, updateData, SuccessMessage.DATA_FOUND);
                    }
                    response(res, SuccessCode.SUCCESS, updateData, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    commentOnImageAdminSide: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await Adminschema.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
                    let d = new Date();
                    let hr = d.getHours();
                    let min = d.getMinutes();
                    let min1;
                    let date = d.getDate();
                    let date1;
                    let month = months[d.getMonth()];
                    let year = d.getFullYear();
                    if (date < 10) {
                        date1 = '' + 0 + date;
                    }
                    else {
                        date1 = date
                    }
                    if (hr < 10) {
                        hr1 = '' + 0 + hr;
                    } else {
                        hr1 = hr
                    }
                    if (min < 10) {
                        min1 = '' + 0 + min;
                    } else {
                        min1 = min
                    }
                    let fullDate = `${date1}-${month}-${year} ${hr1}:${min1}`;
                    let obj = {
                        admin: req.user._id,
                        fname: User1.name,
                        userType: User1.userType,
                        image: User1.image,
                        Comment: req.body.Comment,
                        date: fullDate
                    }
                    if (data.assignType == "ALL") {
                        let desc = await createProject.findOneAndUpdate({ 'originalImage._id': req.body.imageId }, { $push: { 'originalImage.$.Comment': obj } }, { new: true })
                        const data1 = await createProject.findById({ _id: desc._id });
                        response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
                    } else if (data.assignType == "SELECTED") {
                        let desc = await createProject.findOneAndUpdate({ 'assignImage._id': req.body.imageId }, { $push: { 'assignImage.$.Comment': obj } }, { new: true })
                        const data1 = await createProject.findById({ _id: desc._id });
                        response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
                    }
                }
            }
        }
        catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    dashboard: async (req, res, next) => {
        const userid = req.user?._id;
        const User1 = await Adminschema.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let image = 0;
                let project = await createProject.count();
                let imagefind = await createProject.find();
                let manager = await Adminschema.count({ userType: "MANAGER" })
                let employee = await Adminschema.count({ userType: "EMPLOYEE" })
                let user = await User.count({ userType: "COMPANY" })
                imagefind.forEach(element => {
                    let a = element.originalImage;
                    let Length = a.length;
                    image += Length;
                });
                let obj = {
                    name: User1.name,
                    email: User1.email,
                    project: project,
                    image: image,
                    manager: manager,
                    employee: employee,
                    user: user
                }
                response(res, SuccessCode.SUCCESS, obj, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    transactionList: async (req, res) => {
        const userid = req.user?._id;
        try {
            const userResult = await Adminschema.findById({ _id: userid }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let tranData = await transactiondetail.find().sort({ "createdAt": -1 }).populate('project user')
                if (tranData.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                }
                else {
                    response(res, SuccessCode.SUCCESS, tranData, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error)
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    /**
     * Function Name :gettransaction
     * Description   : gettransaction
     *
     * @return response
    */
    getTransaction: async (req, res) => {
        const userid = req.user?._id;
        try {
            const userResult = await Adminschema.findById({ _id: userid }).select("-password");
            if (!userResult) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let transaction = await transactiondetail.findById({ _id: req.params.id }).populate("project user");
                if (!transaction) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, transaction, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    assignproject: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid, userType: { $ne: "EMPLOYEE" } });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let updateProject;
                let transactionData = await transactiondetail.findById({ _id: req.body.id });
                if (!transactionData) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let project = await createProject.findById({ _id: transactionData.project });
                    if (!project) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                    } else {
                        let user = await Adminschema.findById({ _id: req.body.employeeId, userType: "EMPLOYEE" });
                        if (!user) {
                            response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                        } else {
                            if (project.assignTransactionId.length > 0) {
                                for (let i = 0; i < project.assignTransactionId.length; i++) {
                                    const transactionId = project.assignTransactionId[i].transactionId;
                                    console.log(i, "=============", transactionId == req.body.id, "==============================", transactionId, "==", req.body.id);
                                    if ((transactionId == req.body.id) == true) {
                                        let remo = await createProject.findByIdAndUpdate({ _id: project._id, }, { $pull: { 'assignTransactionId': { transactionId: project.assignTransactionId[i].transactionId } } }, { new: true });
                                        console.log("remo------------", remo);
                                        if (remo) {
                                            const d = new Date();
                                            let c = d.toLocaleString();
                                            let obj = {
                                                editor: user._id,
                                                transactionId: transactionData._id,
                                                assignDate: c
                                            }
                                            updateProject = await createProject.findByIdAndUpdate({ _id: project._id }, { $set: { status: "INPROCESS" }, $push: { assignTransactionId: obj } }, { new: true });
                                            if (updateProject) {
                                                let transactionData1 = await transactiondetail.findByIdAndUpdate({ _id: transactionData._id }, { $set: { assignStatus: "INPROCESS" } }, { new: true });
                                            }

                                        }
                                    } else if ((transactionId == req.body.id) == false) {
                                        console.log(i, "=============", transactionId == req.body.id, "==============================", transactionId, "==", req.body.id);
                                        console.log("================================================1603===========================");
                                        let remo = await createProject.findByIdAndUpdate({ _id: project._id, }, { $pull: { 'assignTransactionId': { transactionId: req.body.id } } }, { new: true });
                                        console.log("remo------------", remo);
                                        if (remo) {
                                            const d = new Date();
                                            let c = d.toLocaleString();
                                            let obj = {
                                                editor: user._id,
                                                transactionId: transactionData._id,
                                                assignDate: c
                                            }
                                            updateProject = await createProject.findByIdAndUpdate({ _id: project._id }, { $set: { status: "INPROCESS" }, $push: { assignTransactionId: obj } }, { new: true });
                                            if (updateProject) {
                                                let transactionData1 = await transactiondetail.findByIdAndUpdate({ _id: transactionData._id }, { $set: { assignStatus: "INPROCESS" } }, { new: true });
                                            }
                                        }
                                    }
                                }
                            } else {
                                console.log("==========================1600==================================================");
                                const d = new Date();
                                let c = d.toLocaleString();
                                let obj = {
                                    editor: user._id,
                                    transactionId: transactionData._id,
                                    assignDate: c
                                }
                                updateProject = await createProject.findByIdAndUpdate({ _id: project._id }, { $set: { status: "INPROCESS" }, $push: { assignTransactionId: obj } }, { new: true });
                                if (updateProject) {
                                    let transactionData1 = await transactiondetail.findByIdAndUpdate({ _id: transactionData._id }, { $set: { assignStatus: "INPROCESS" } }, { new: true });
                                }
                            }
                            response(res, SuccessCode.SUCCESS, updateProject, SuccessMessage.DATA_SAVED);
                        }
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    removeAssignUser: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid, userType: { $ne: "EMPLOYEE" } });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let project = await createProject.findById({ _id: req.params.id });
                if (!project) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    await createProject.update({ _id: project._id }, { $pull: { "assignTransactionId": { "_id": req.body.transactionId } } }, { multi: true });
                    let data1 = await createProject.findById({ _id: project._id });
                    response(res, SuccessCode.SUCCESS, data1, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getprojectlistofEmployee: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const employee = await Adminschema.findById({ _id: req.body.employeeId });
                if (!employee) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let project = await createProject.find({ editor: employee._id });
                    if (project.length == 0) {
                        response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                    } else {
                        response(res, SuccessCode.SUCCESS, project, SuccessMessage.DATA_SAVED);
                    }
                }
            }
        }
        catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getProjectsListForemployee: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let project = await createProject.find({ editor: admin._id });
                if (project.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.DATA_NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, project, SuccessMessage.DATA_SAVED);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    getEmployeeProjectCount: async (req, res) => {
        const userid = req.user?._id;
        const admin = await Adminschema.findById({ _id: userid });
        try {
            if (!admin) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let project = await createProject.find({ editor: req.query.employeeId }).count();
                let obj = {
                    userId: req.query.employeeId,
                    projectCount: project
                }
                response(res, SuccessCode.SUCCESS, obj, SuccessMessage.DATA_SAVED);
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    addEditDriveLink: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await Adminschema.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let obj = {
                        linkTitle: req.body.linkTitle,
                        link: req.body.link
                    }
                    let updateProject = await createProject.findByIdAndUpdate({ _id: data._id }, { $push: { editDriveLink: obj } }, { new: true })
                    if (updateProject) {
                        response(res, SuccessCode.SUCCESS, updateProject, SuccessMessage.UPDATE_SUCCESS);
                    }
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    deleteEditDriveLink: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await Adminschema.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id, user: User1._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let updateProject = await createProject.findByIdAndUpdate({ _id: data._id }, { $pull: { 'editDriveLink': { _id: req.query.linkId } } }, { new: true })
                    if (updateProject) {
                        response(res, SuccessCode.SUCCESS, updateProject, SuccessMessage.UPDATE_SUCCESS);
                    }
                }
            }
        } catch (error) {
            console.log("964======================================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    broadcastNotification: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await Adminschema.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                let Users = await User.find({ status: "ACTIVE" });
                for (let i = 0; i < Users.length; i++) {
                    const userId = Users[i]._id;
                    if (i == 0) {
                        const notificationCreate = new notification({
                            user: userId,
                            assignUser: User1._id,
                            title: req.body.title,
                            body: req.body.content,
                            notificationType: "BROADCAST"
                        })
                        await notificationCreate.save();
                    } else {
                        const notificationCreate = new notification({
                            user: userId,
                            title: req.body.title,
                            body: req.body.content,
                            notificationType: "BROADCAST"
                        })
                        await notificationCreate.save();
                    }
                }
                response(res, SuccessCode.SUCCESS, {}, SuccessMessage.BROADCAST);
            }
        } catch (error) {
            console.log("1745============broadcastNotification==========================", error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    broadcastListforAdmin: async (req, res) => {
        const userid = req.user?._id;
        const user = await Adminschema.findById({ _id: userid });
        try {
            if (!user) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await notification.find({ assignUser: userid, notificationType: "BROADCAST" }).sort({ "createdAt": -1 });
                const dataCount = await notification.find({ assignUser: userid, notificationType: "BROADCAST" }).count();
                if (data.length == 0) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    response(res, SuccessCode.SUCCESS, { data, dataCount }, SuccessMessage.DATA_FOUND);
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    },
    createOrder: async (req, res) => {
        const userid = req.user?._id;
        const User1 = await Adminschema.findById({ _id: userid });
        try {
            if (!User1) {
                response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.USER_NOT_FOUND);
            } else {
                const data = await createProject.findById({ _id: req.params._id });
                if (!data) {
                    response(res, ErrorCode.NOT_FOUND, [], ErrorMessage.NOT_FOUND);
                } else {
                    let selectedImage = [];
                    for (let i = 0; i < data.originalImage.length; i++) {
                        let imageUrl = data.originalImage[i]._id;
                        selectedImage.push(imageUrl);
                    }
                    let tranId = await randomOTPGenerate(24);
                    let transactionId = `PAYID-${tranId}`;
                    let tokId = await randomOTPGenerate(17);
                    let tokenId = `EC-${tokId}`;
                    const transactionCreate = new transactiondetail({
                        selectedImage: selectedImage,
                        assignStatus: "PENDING",
                        user: data.user,
                        project: data._id,
                        transactionId: transactionId,
                        tokenId: tokenId,
                        transactionStatus: "SUCCESS"
                    });
                    let saveRes = await transactionCreate.save();
                    if (saveRes) {
                        response(res, SuccessCode.SUCCESS, saveRes, SuccessMessage.PAYMENT_SUCCESS);
                    }
                }
            }
        } catch (error) {
            console.log(error);
            response(res, ErrorCode.SOMETHING_WRONG, [], ErrorMessage.SOMETHING_WRONG);
        }
    }
}




//*********************Function for upload image *************************************/
function convertImage(image) {
    return new Promise((resolve, reject) => {
        commonFunction.uploadImage(image, (error, upload) => {
            if (error) {
                console.log("Error uploading image")
            }
            else {
                resolve(upload)
            }
        })
    })
}

const randomOTPGenerate = async (length) => {
    var digits = 'ABCDEFGHIJ123456789KLMNOPQRSTUV123456789WXYZ';
    let OTP = '';
    for (let i = 0; i < length; i++) {
        OTP += digits[Math.floor(Math.random() * 20)];
    }
    return OTP;
};
//*************************End of upload image *****************************/


