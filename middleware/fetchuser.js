var jwt = require("jsonwebtoken");
const JWT_SECRET = "thisisnewcourse";

const fetchuser = (req, res, next) => {
  //get the user from the jwt token and add id to req object
  const token = req.header("token");
  if (!token) {
    res.status(401).send({ error: "Please authenticate using a valid token" });
  }

  try {
    const data = jwt.verify(token, JWT_SECRET);
    // console.log(data._id);
    req.user = data;
    // console.log(req.user.id);
    next();
  } catch {
    res.status(401).send({ error: "Please a authenticate using a valid token" });
  }
};

module.exports = fetchuser;
