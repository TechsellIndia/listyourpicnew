const config = require('../config/config');
var nodemailer = require('nodemailer');
const twilio = require('twilio');
const accountSid = 'ACf786a64203b2524f8ee2878ee632bbe7';
const authToken = '7aa1973b369cf9bdae1ac11b83498472';
const client = require('twilio')(accountSid, authToken);
const userModel = require('../models/user');
var cloudinary = require('cloudinary').v2;
const async = require('async');

cloudinary.config({
    cloud_name: global.gConfig.cloudinary.cloud_name,
    api_key: global.gConfig.cloudinary.api_key,
    api_secret: global.gConfig.cloudinary.api_secret
});
cloudinary.config({
    cloud_name: "listyourpics",
    api_key: "385339736967384",
    api_secret: "Ehs4EJc1UoUHo6YaeI93b45qFBk"
});
module.exports = {
    getOTP() {
        var otp = Math.floor(1000 + Math.random() * 9000);
        return otp;
    },
    sendMail: (email, text, userName, callback) => {
        let html = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
        <head>
        <!--[if gte mso 9]>
        <xml>
          <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
          </o:OfficeDocumentSettings>
        </xml>
        <![endif]-->
          <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
          <meta name="viewport" content="width=device-width, initial-scale=1.0">
          <meta name="x-apple-disable-message-reformatting">
          <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
          <title></title>
          
            <style type="text/css">
              table, td { color: #000000; } @media only screen and (min-width: 670px) {
          .u-row {
            width: 650px !important;
          }
          .u-row .u-col {
            vertical-align: top;
          }
        
          .u-row .u-col-100 {
            width: 650px !important;
          }
        
        }
        
        @media (max-width: 670px) {
          .u-row-container {
            max-width: 100% !important;
            padding-left: 0px !important;
            padding-right: 0px !important;
          }
          .u-row .u-col {
            min-width: 320px !important;
            max-width: 100% !important;
            display: block !important;
          }
          .u-row {
            width: calc(100% - 40px) !important;
          }
          .u-col {
            width: 100% !important;
          }
          .u-col > div {
            margin: 0 auto;
          }
        }
        body {
          margin: 0;
          padding: 0;
        }
        
        table,
        tr,
        td {
          vertical-align: top;
          border-collapse: collapse;
        }
        
        p {
          margin: 0;
        }
        
        .ie-container table,
        .mso-container table {
          table-layout: fixed;
        }
        
        * {
          line-height: inherit;
        }
        
        a[x-apple-data-detectors='true'] {
          color: inherit !important;
          text-decoration: none !important;
        }
        
        </style>
          
          
        
        <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css"><!--<![endif]-->
        
        </head>
        
        <body class="clean-body u_body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
          <!--[if IE]><div class="ie-container"><![endif]-->
          <!--[if mso]><div class="mso-container"><![endif]-->
          <table style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%" cellpadding="0" cellspacing="0">
          <tbody>
          <tr style="vertical-align: top">
            <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
            
        
        <div class="u-row-container" style="padding: 0px;background-color: transparent">
          <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
              <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
              
        <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
        <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
          <div style="background-color: #ffffff;width: 100% !important;">
          <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
          
        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
          <tbody>
            <tr>
              <td style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;" align="left">
                
        <table width="100%" cellpadding="0" cellspacing="0" border="0">
          <tr>
            <td style="padding-right: 0px;padding-left: 0px;" align="center">
             
            <img align="center" border="0"
            src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
            alt="Image" title="Image"
            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
            width="100" />
              
            </td>
          </tr>
        </table>
        
              </td>
            </tr>
          </tbody>
        </table>
        
          <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
          </div>
        </div>
        <!--[if (mso)|(IE)]></td><![endif]-->
              <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
            </div>
          </div>
        </div>
        
        
        
        <div class="u-row-container" style="padding: 0px;background-color: transparent">
          <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
              <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
              
        <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
        <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
          <div style="width: 100% !important;">
          <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
          
        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
          <tbody>
            <tr>
              <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px;font-family:'Montserrat',sans-serif;" align="left">
                
          <div style="color: #1b262c; line-height: 140%; text-align: center; word-wrap: break-word;">
            <p style="font-size: 14px; line-height: 140%;"><strong><span style="font-size: 24px; line-height: 33.6px;">Welcome to ListYourPics</span></strong></p>
          </div>
        
              </td>
            </tr>
          </tbody>
        </table>
        
        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
          <tbody>
            <tr>
              <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;" align="left">
                
          <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
            <p style="font-size: 14px; line-height: 140%;">
            Dear ${userName},
            <br><br>
            OTP for your E-mail verification is  ${text}. Please use this  OTP (One-Time-Password) to login to your own ListYourPics and access the unlimited possibilities of ListYourPics.
            <br><br>
            This OTP is valid for the next 05 minutes and can be used only once.<br><br>
    
            <br><br>
            Thanks and regards <br>
            Team Listyourpic
            </p>
          </div>
        
              </td>
            </tr>
          </tbody>
        </table>
        
          <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
          </div>
        </div>
        <!--[if (mso)|(IE)]></td><![endif]-->
              <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
            </div>
          </div>
        </div>
        
        
        
        <div class="u-row-container" style="padding: 0px;background-color: transparent">
          <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
            <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
              <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
              
        <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
        <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
          <div style="width: 100% !important;">
          <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
          
        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
          <tbody>
            <tr>
              <td style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;" align="left">
                
          <div style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
            <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPics | All RIghts Reserved</span></p>
          </div>
        
              </td>
            </tr>
          </tbody>
        </table>
        
          <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
          </div>
        </div>
        <!--[if (mso)|(IE)]></td><![endif]-->
              <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
            </div>
          </div>
        </div>
        
        
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
            </td>
          </tr>
          </tbody>
          </table>
          <!--[if mso]></div><![endif]-->
          <!--[if IE]></div><![endif]-->
        </body>
        </html>`
        var transporter = nodemailer.createTransport({
          host: 'vps.houszzz.com',
          port: 25,
          secure: false, 
          auth: {
            user: 'noreply@listyourpics.com',  //         'noreply@listyourpics.com' ==>'Ef!8U&j*O'     noreply@houszzz.com ===> d$aQt7otw
            pass: 'Ef!8U&j*O'       
          }
        });
        var mailOptions = {
          from: 'noreply@listyourpics.com',
          to: email,
          subject: "Email OTP for ListYourPics",
          html: html
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                callback(error, null)
            } else {
                callback(null, info.response)
            }
        });
    },
    //   sendMail: (email, text, userName, callback) => {
//     let html = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
//     <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
//     <head>
//     <!--[if gte mso 9]>
//     <xml>
//       <o:OfficeDocumentSettings>
//         <o:AllowPNG/>
//         <o:PixelsPerInch>96</o:PixelsPerInch>
//       </o:OfficeDocumentSettings>
//     </xml>
//     <![endif]-->
//       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
//       <meta name="viewport" content="width=device-width, initial-scale=1.0">
//       <meta name="x-apple-disable-message-reformatting">
//       <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
//       <title></title>
      
//         <style type="text/css">
//           table, td { color: #000000; } @media only screen and (min-width: 670px) {
//       .u-row {
//         width: 650px !important;
//       }
//       .u-row .u-col {
//         vertical-align: top;
//       }
    
//       .u-row .u-col-100 {
//         width: 650px !important;
//       }
    
//     }
    
//     @media (max-width: 670px) {
//       .u-row-container {
//         max-width: 100% !important;
//         padding-left: 0px !important;
//         padding-right: 0px !important;
//       }
//       .u-row .u-col {
//         min-width: 320px !important;
//         max-width: 100% !important;
//         display: block !important;
//       }
//       .u-row {
//         width: calc(100% - 40px) !important;
//       }
//       .u-col {
//         width: 100% !important;
//       }
//       .u-col > div {
//         margin: 0 auto;
//       }
//     }
//     body {
//       margin: 0;
//       padding: 0;
//     }
    
//     table,
//     tr,
//     td {
//       vertical-align: top;
//       border-collapse: collapse;
//     }
    
//     p {
//       margin: 0;
//     }
    
//     .ie-container table,
//     .mso-container table {
//       table-layout: fixed;
//     }
    
//     * {
//       line-height: inherit;
//     }
    
//     a[x-apple-data-detectors='true'] {
//       color: inherit !important;
//       text-decoration: none !important;
//     }
    
//     </style>
      
      
    
//     <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css"><!--<![endif]-->
    
//     </head>
    
//     <body class="clean-body u_body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
//       <!--[if IE]><div class="ie-container"><![endif]-->
//       <!--[if mso]><div class="mso-container"><![endif]-->
//       <table style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%" cellpadding="0" cellspacing="0">
//       <tbody>
//       <tr style="vertical-align: top">
//         <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
//         <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
        
    
//     <div class="u-row-container" style="padding: 0px;background-color: transparent">
//       <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
//         <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
//           <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
          
//     <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
//     <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
//       <div style="background-color: #ffffff;width: 100% !important;">
//       <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
      
//     <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
//       <tbody>
//         <tr>
//           <td style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;" align="left">
            
//     <table width="100%" cellpadding="0" cellspacing="0" border="0">
//       <tr>
//         <td style="padding-right: 0px;padding-left: 0px;" align="center">
         
//         <img align="center" border="0"
//         src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
//         alt="Image" title="Image"
//         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
//         width="100" />
          
//         </td>
//       </tr>
//     </table>
    
//           </td>
//         </tr>
//       </tbody>
//     </table>
    
//       <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
//       </div>
//     </div>
//     <!--[if (mso)|(IE)]></td><![endif]-->
//           <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
//         </div>
//       </div>
//     </div>
    
    
    
//     <div class="u-row-container" style="padding: 0px;background-color: transparent">
//       <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
//         <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
//           <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
          
//     <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
//     <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
//       <div style="width: 100% !important;">
//       <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
      
//     <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
//       <tbody>
//         <tr>
//           <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px;font-family:'Montserrat',sans-serif;" align="left">
            
//       <div style="color: #1b262c; line-height: 140%; text-align: center; word-wrap: break-word;">
//         <p style="font-size: 14px; line-height: 140%;"><strong><span style="font-size: 24px; line-height: 33.6px;">Welcome to ListYourPics</span></strong></p>
//       </div>
    
//           </td>
//         </tr>
//       </tbody>
//     </table>
    
//     <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
//       <tbody>
//         <tr>
//           <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;" align="left">
            
//       <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
//         <p style="font-size: 14px; line-height: 140%;">
//         Dear ${userName},
//         <br><br>
//         OTP for your E-mail verification is  ${text}. Please use this  OTP (One-Time-Password) to login to your own ListYourPics and access the unlimited possibilities of ListYourPics.
//         <br><br>
//         This OTP is valid for the next 05 minutes and can be used only once.<br><br>

//         <br><br>
//         Thanks and regards <br>
//         Team Listyourpic
//         </p>
//       </div>
    
//           </td>
//         </tr>
//       </tbody>
//     </table>
    
//       <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
//       </div>
//     </div>
//     <!--[if (mso)|(IE)]></td><![endif]-->
//           <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
//         </div>
//       </div>
//     </div>
    
    
    
//     <div class="u-row-container" style="padding: 0px;background-color: transparent">
//       <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
//         <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
//           <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
          
//     <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
//     <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
//       <div style="width: 100% !important;">
//       <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
      
//     <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
//       <tbody>
//         <tr>
//           <td style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;" align="left">
            
//       <div style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
//         <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPics | All RIghts Reserved</span></p>
//       </div>
    
//           </td>
//         </tr>
//       </tbody>
//     </table>
    
//       <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
//       </div>
//     </div>
//     <!--[if (mso)|(IE)]></td><![endif]-->
//           <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
//         </div>
//       </div>
//     </div>
    
    
//         <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
//         </td>
//       </tr>
//       </tbody>
//       </table>
//       <!--[if mso]></div><![endif]-->
//       <!--[if IE]></div><![endif]-->
//     </body>
//     </html>`

//     var transporter = nodemailer.createTransport({
//       service: 'gmail',
//       auth: {
//         "user": "testingnodejsapi@gmail.com",
//         "pass": "Mobiloitte@1"
//       }
//     });
//     var mailOptions = {
//       from: "<do_not_reply@gmail.com>",
//       to: email,
//       subject: 'Email OTP for ListYourPics',
//       //text: text,
//       html: html
//     };
//     transporter.sendMail(mailOptions, function (error, info) {
//       if (error) {
//         callback(error, null)
//       } else {
//         callback(null, info.response)
//       }
//     });
//   },
    sendMail1: (email, adminName, userEmail, complaintID, userName, subject, text) => {
    return new Promise((resolve, reject) => {
      let html = `<!DOCTYPE HTML
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">
  
  <head>
    <!--[if gte mso 9]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="x-apple-disable-message-reformatting">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <title></title>
  
    <style type="text/css">
      table,
      td {
        color: #000000;
      }
  
      @media only screen and (min-width: 670px) {
        .u-row {
          width: 650px !important;
        }
  
        .u-row .u-col {
          vertical-align: top;
        }
  
        .u-row .u-col-100 {
          width: 650px !important;
        }
  
      }
  
      @media (max-width: 670px) {
        .u-row-container {
          max-width: 100% !important;
          padding-left: 0px !important;
          padding-right: 0px !important;
        }
  
        .u-row .u-col {
          min-width: 320px !important;
          max-width: 100% !important;
          display: block !important;
        }
  
        .u-row {
          width: calc(100% - 40px) !important;
        }
  
        .u-col {
          width: 100% !important;
        }
  
        .u-col>div {
          margin: 0 auto;
        }
      }
  
      body {
        margin: 0;
        padding: 0;
      }
  
      table,
      tr,
      td {
        vertical-align: top;
        border-collapse: collapse;
      }
  
      p {
        margin: 0;
      }
  
      .ie-container table,
      .mso-container table {
        table-layout: fixed;
      }
  
      * {
        line-height: inherit;
      }
  
      a[x-apple-data-detectors='true'] {
        color: inherit !important;
        text-decoration: none !important;
      }
    </style>
  
  
  
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css">
    <!--<![endif]-->
  
  </head>
  
  <body class="clean-body u_body"
    style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
    <!--[if IE]><div class="ie-container"><![endif]-->
    <!--[if mso]><div class="mso-container"><![endif]-->
    <table
      style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%"
      cellpadding="0" cellspacing="0">
      <tbody>
        <tr style="vertical-align: top">
          <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
            <div class="u-row-container" style="padding: 0px;background-color: transparent">
              <div class="u-row"
                style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
  
                  <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
                  <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
                    <div style="background-color: #ffffff;width: 100% !important;">
                      <!--[if (!mso)&(!IE)]><!-->
                      <div
                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                        <!--<![endif]-->
  
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                          cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td
                                style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;"
                                align="left">
  
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                  <tr>
                                    <td style="padding-right: 0px;padding-left: 0px;" align="center">
                                      <!-- <a>Smart Contract as Service Plateform </a>-->
  
                                      <img align="center" border="0"
                                        src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
                                        alt="Image" title="Image"
                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
                                        width="100" />
  
                                    </td>
                                  </tr>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <!--[if (!mso)&(!IE)]><!-->
                      </div>
                      <!--<![endif]-->
                    </div>
                  </div>
                  <!--[if (mso)|(IE)]></td><![endif]-->
                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
            <div class="u-row-container" style="padding: 0px;background-color: transparent">
              <div class="u-row"
                style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
  
                  <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
                  <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                      <!--[if (!mso)&(!IE)]><!-->
                      <div
                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                        <!--<![endif]-->
  
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                          cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td
                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
                                align="left">
  
                                <div style="color: #1b262c; line-height: 140%;  word-wrap: break-word;">
                                  <p style="font-size: 14px; line-height: 140%;"><strong><span
                                        style="font-size: 24px; line-height: 33.6px;"> Dear ${adminName}</span></strong>
                                  </p>
                                </div>
  
                              </td>
                            </tr>
                          </tbody>
                        </table>
  
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                          cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td
                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
                                align="left">
  
                                <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
                                  <p style="font-size: 14px; line-height: 140%;">${text}</p>
                                </div>
  
                              </td>
                            </tr>
                          </tbody>
                        </table>
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                        cellspacing="0" width="100%" border="0">
                        <tbody>
                          <tr>
                            <td
                              style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
                              align="left">
                              <div style="color: #1b262c; line-height: 140%; text-align: centre; word-wrap: break-word;">
                              <tr>
                              <th>Name</th>
                              <td>${userName}</td>
    
                            </tr>
                            <tr>
                              <th>Email</th>
                              <td>${userEmail}</td>
                            </tr>
                            <tr>
                              <th>Complaint ID</th>
                              <td>${complaintID}</td>
                            </tr>                            
                              </div>

                            </td>
                          </tr>
                         
                        </tbody>
                      </table>
                        <!--[if (!mso)&(!IE)]><!-->
                      </div>
                      <!--<![endif]-->
                    </div>
                  </div>
                  <!--[if (mso)|(IE)]></td><![endif]-->
                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
  
  
  
            <div class="u-row-container" style="padding: 10px;background-color: transparent">
              <div class="u-row"
                style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
  
                  <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
                  <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                      <!--[if (!mso)&(!IE)]><!-->
                      <div
                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                        <!--<![endif]-->
  
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                          cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td
                                style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;"
                                align="left">
  
                                <div
                                  style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
                                  <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span
                                      style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPicss | All
                                      RIghts Reserved</span></p>
                                </div>
  
                              </td>
                            </tr>
                          </tbody>
                        </table>
  
                        <!--[if (!mso)&(!IE)]><!-->
                      </div>
                      <!--<![endif]-->
                    </div>
                  </div>
                  <!--[if (mso)|(IE)]></td><![endif]-->
                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
  
  
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
  </body>
  
  </html>`

  var transporter = nodemailer.createTransport({
          host: 'vps.houszzz.com',
          port: 25,
          secure: false, 
          auth: {
            user: 'noreply@listyourpics.com',  //         'noreply@listyourpics.com' ==>'Ef!8U&j*O'     noreply@houszzz.com ===> d$aQt7otw
            pass: 'Ef!8U&j*O'       
          }
        });
        var mailOptions = {
          from: 'noreply@listyourpics.com',
          to: email,
          subject: subject,
          html: html
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                callback(error, null)
            } else {
                callback(null, info.response)
            }
        });
    })
  },
    sendMail2: (email, adminName, userEmail, projectName, userName, subject, text) => {
    return new Promise((resolve, reject) => {

      let html = `<!DOCTYPE HTML
    PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
  <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    xmlns:o="urn:schemas-microsoft-com:office:office">
  
  <head>
    <!--[if gte mso 9]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="x-apple-disable-message-reformatting">
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!--<![endif]-->
    <title></title>
  
    <style type="text/css">
      table,
      td {
        color: #000000;
      }
  
      @media only screen and (min-width: 670px) {
        .u-row {
          width: 650px !important;
        }
  
        .u-row .u-col {
          vertical-align: top;
        }
  
        .u-row .u-col-100 {
          width: 650px !important;
        }
  
      }
  
      @media (max-width: 670px) {
        .u-row-container {
          max-width: 100% !important;
          padding-left: 0px !important;
          padding-right: 0px !important;
        }
  
        .u-row .u-col {
          min-width: 320px !important;
          max-width: 100% !important;
          display: block !important;
        }
  
        .u-row {
          width: calc(100% - 40px) !important;
        }
  
        .u-col {
          width: 100% !important;
        }
  
        .u-col>div {
          margin: 0 auto;
        }
      }
  
      body {
        margin: 0;
        padding: 0;
      }
  
      table,
      tr,
      td {
        vertical-align: top;
        border-collapse: collapse;
      }
  
      p {
        margin: 0;
      }
  
      .ie-container table,
      .mso-container table {
        table-layout: fixed;
      }
  
      * {
        line-height: inherit;
      }
  
      a[x-apple-data-detectors='true'] {
        color: inherit !important;
        text-decoration: none !important;
      }
    </style>
  
  
  
    <!--[if !mso]><!-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css">
    <!--<![endif]-->
  
  </head>
  
  <body class="clean-body u_body"
    style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
    <!--[if IE]><div class="ie-container"><![endif]-->
    <!--[if mso]><div class="mso-container"><![endif]-->
    <table
      style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%"
      cellpadding="0" cellspacing="0">
      <tbody>
        <tr style="vertical-align: top">
          <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
  
  
            <div class="u-row-container" style="padding: 0px;background-color: transparent">
              <div class="u-row"
                style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
  
                  <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
                  <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
                    <div style="background-color: #ffffff;width: 100% !important;">
                      <!--[if (!mso)&(!IE)]><!-->
                      <div
                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                        <!--<![endif]-->
  
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                          cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td
                                style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;"
                                align="left">
  
                                <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                  <tr>
                                    <td style="padding-right: 0px;padding-left: 0px;" align="center">
                                      <!-- <a>Smart Contract as Service Plateform </a>-->
  
                                      <img align="center" border="0"
                                        src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
                                        alt="Image" title="Image"
                                        style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
                                        width="100" />
  
                                    </td>
                                  </tr>
                                </table>
  
                              </td>
                            </tr>
                          </tbody>
                        </table>
  
                        <!--[if (!mso)&(!IE)]><!-->
                      </div>
                      <!--<![endif]-->
                    </div>
                  </div>
                  <!--[if (mso)|(IE)]></td><![endif]-->
                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
  
  
  
            <div class="u-row-container" style="padding: 0px;background-color: transparent">
              <div class="u-row"
                style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
  
                  <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
                  <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                      <!--[if (!mso)&(!IE)]><!-->
                      <div
                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                        <!--<![endif]-->
  
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                          cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td
                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
                                align="left">
  
                                <div style="color: #1b262c; line-height: 140%;  word-wrap: break-word;">
                                  <p style="font-size: 14px; line-height: 140%;"><strong><span
                                        style="font-size: 24px; line-height: 33.6px;"> Dear ${adminName}</span></strong>
                                  </p>
                                </div>
  
                              </td>
                            </tr>
                          </tbody>
                        </table>
  
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                          cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td
                                style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
                                align="left">
  
                                <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
                                  <p style="font-size: 14px; line-height: 140%;">${text}</p>
                                </div>
  
                              </td>
                            </tr>
                           
                          </tbody>
                        </table>
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                        cellspacing="0" width="100%" border="0">
                        <tbody>
                          <tr>
                            <td
                              style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
                              align="left">

                              <div style="color: #1b262c; line-height: 140%; text-align: centre; word-wrap: break-word;">
                              <tr>
                              <th>Name</th>
                              <td>${userName}</td>
    
                            </tr>
                            <tr>
                              <th>Email</th>
                              <td>${userEmail}</td>
                            </tr>
                            <tr>
                              <th>Project Name</th>
                              <td>${projectName}</td>
                            </tr>                            
                              </div>

                            </td>
                          </tr>
                         
                        </tbody>
                      </table>
                        <!--[if (!mso)&(!IE)]><!-->
                      </div>
                      <!--<![endif]-->
                    </div>
                  </div>
                  <!--[if (mso)|(IE)]></td><![endif]-->
                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
  
  
  
            <div class="u-row-container" style="padding: 10px;background-color: transparent">
              <div class="u-row"
                style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
                <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
                  <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
  
                  <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
                  <div class="u-col u-col-100"
                    style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
                    <div style="width: 100% !important;">
                      <!--[if (!mso)&(!IE)]><!-->
                      <div
                        style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
                        <!--<![endif]-->
  
                        <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
                          cellspacing="0" width="100%" border="0">
                          <tbody>
                            <tr>
                              <td
                                style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;"
                                align="left">
  
                                <div
                                  style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
                                  <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span
                                      style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPicss | All
                                      RIghts Reserved</span></p>
                                </div>
  
                              </td>
                            </tr>
                          </tbody>
                        </table>
  
                        <!--[if (!mso)&(!IE)]><!-->
                      </div>
                      <!--<![endif]-->
                    </div>
                  </div>
                  <!--[if (mso)|(IE)]></td><![endif]-->
                  <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
                </div>
              </div>
            </div>
  
  
            <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
          </td>
        </tr>
      </tbody>
    </table>
    <!--[if mso]></div><![endif]-->
    <!--[if IE]></div><![endif]-->
  </body>
  
  </html>`
  var transporter = nodemailer.createTransport({
          host: 'vps.houszzz.com',
          port: 25,
          secure: false, 
          auth: {
            user: 'noreply@listyourpics.com',  //         'noreply@listyourpics.com' ==>'Ef!8U&j*O'     noreply@houszzz.com ===> d$aQt7otw
            pass: 'Ef!8U&j*O'       
          }
        });
        var mailOptions = {
          from: 'noreply@listyourpics.com',
          to: email,
          subject: subject,
          html: html
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                callback(error, null)
            } else {
                callback(null, info.response)
            }
        });
    });
  },
    WelcomeMail: (email, userName, callback) => {
    let html = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
      <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
      <head>
      <!--[if gte mso 9]>
      <xml>
        <o:OfficeDocumentSettings>
          <o:AllowPNG/>
          <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="x-apple-disable-message-reformatting">
        <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
        <title></title>
        
          <style type="text/css">
            table, td { color: #000000; } @media only screen and (min-width: 670px) {
        .u-row {
          width: 650px !important;
        }
        .u-row .u-col {
          vertical-align: top;
        }
      
        .u-row .u-col-100 {
          width: 650px !important;
        }
      
      }
      
      @media (max-width: 670px) {
        .u-row-container {
          max-width: 100% !important;
          padding-left: 0px !important;
          padding-right: 0px !important;
        }
        .u-row .u-col {
          min-width: 320px !important;
          max-width: 100% !important;
          display: block !important;
        }
        .u-row {
          width: calc(100% - 40px) !important;
        }
        .u-col {
          width: 100% !important;
        }
        .u-col > div {
          margin: 0 auto;
        }
      }
      body {
        margin: 0;
        padding: 0;
      }
      
      table,
      tr,
      td {
        vertical-align: top;
        border-collapse: collapse;
      }
      
      p {
        margin: 0;
      }
      
      .ie-container table,
      .mso-container table {
        table-layout: fixed;
      }
      
      * {
        line-height: inherit;
      }
      
      a[x-apple-data-detectors='true'] {
        color: inherit !important;
        text-decoration: none !important;
      }
      
      </style>
        
        
      
      <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css"><!--<![endif]-->
      
      </head>
      
      <body class="clean-body u_body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
        <!--[if IE]><div class="ie-container"><![endif]-->
        <!--[if mso]><div class="mso-container"><![endif]-->
        <table style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%" cellpadding="0" cellspacing="0">
        <tbody>
        <tr style="vertical-align: top">
          <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
          <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
          
      
      <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
          <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
            
      <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
      <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
        <div style="background-color: #ffffff;width: 100% !important;">
        <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
        
      <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
        <tbody>
          <tr>
            <td style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;" align="left">
              
      <table width="100%" cellpadding="0" cellspacing="0" border="0">
        <tr>
          <td style="padding-right: 0px;padding-left: 0px;" align="center">
            
            <img align="center" border="0"
            src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
            alt="Image" title="Image"
            style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
            width="100" />
  
            
          </td>
        </tr>
      </table>
      
            </td>
          </tr>
        </tbody>
      </table>
      
        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
        </div>
      </div>
      <!--[if (mso)|(IE)]></td><![endif]-->
            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>
      
      
      
      <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
          <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
            
      <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
      <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
        <div style="width: 100% !important;">
        <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
        
      <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
        <tbody>
          <tr>
            <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px;font-family:'Montserrat',sans-serif;" align="left">
              
        <div style="color: #1b262c; line-height: 140%; text-align: center; word-wrap: break-word;">
          <p style="font-size: 14px; line-height: 140%;"><strong><span style="font-size: 24px; line-height: 33.6px;">Welcome!</span></strong></p>
        </div>
      
            </td>
          </tr>
        </tbody>
      </table>
      
      <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
        <tbody>
          <tr>
            <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;" align="left">
              
        <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
          <p style="font-size: 14px; line-height: 140%;">Hi ${userName},<br><br>
           Thanks for choosing  ListYourPics® <br> 
           you can learn more about ListYourPics <a href="http://listyourpics.com/">here </a>
           <br><br>
           Thanks and regards <br>
           Team Listyourpic 
            </p>
        </div>
      
            </td>
          </tr>
        </tbody>
      </table>
      
        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
        </div>
      </div>
      <!--[if (mso)|(IE)]></td><![endif]-->
            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>
      
      
      
      <div class="u-row-container" style="padding: 0px;background-color: transparent">
        <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
          <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
            <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
            
      <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
      <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
        <div style="width: 100% !important;">
        <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
        
      <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
        <tbody>
          <tr>
            <td style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;" align="left">
              
        <div style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
          <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPics | All RIghts Reserved</span></p>
        </div>
      
            </td>
          </tr>
        </tbody>
      </table>
      
        <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
        </div>
      </div>
      <!--[if (mso)|(IE)]></td><![endif]-->
            <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
          </div>
        </div>
      </div>
      
      
          <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
          </td>
        </tr>
        </tbody>
        </table>
        <!--[if mso]></div><![endif]-->
        <!--[if IE]></div><![endif]-->
      </body>
      </html>`
     var transporter = nodemailer.createTransport({
          host: 'vps.houszzz.com',
          port: 25,
          secure: false, 
          auth: {
            user: 'noreply@listyourpics.com',  //         'noreply@listyourpics.com' ==>'Ef!8U&j*O'     noreply@houszzz.com ===> d$aQt7otw
            pass: 'Ef!8U&j*O'       
          }
        });
        var mailOptions = {
          from: 'noreply@listyourpics.com',
          to: email,
          subject: "Welcome",
          html: html
        };
        transporter.sendMail(mailOptions, function (error, info) {
            if (error) {
                callback(error, null)
            } else {
                callback(null, info.response)
            }
        });
  },
    // sendMail: (email, text, userName, callback) => {
    //     let html = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    //     <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    //     <head>
    //     <!--[if gte mso 9]>
    //     <xml>
    //       <o:OfficeDocumentSettings>
    //         <o:AllowPNG/>
    //         <o:PixelsPerInch>96</o:PixelsPerInch>
    //       </o:OfficeDocumentSettings>
    //     </xml>
    //     <![endif]-->
    //       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    //       <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //       <meta name="x-apple-disable-message-reformatting">
    //       <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    //       <title></title>
          
    //         <style type="text/css">
    //           table, td { color: #000000; } @media only screen and (min-width: 670px) {
    //       .u-row {
    //         width: 650px !important;
    //       }
    //       .u-row .u-col {
    //         vertical-align: top;
    //       }
        
    //       .u-row .u-col-100 {
    //         width: 650px !important;
    //       }
        
    //     }
        
    //     @media (max-width: 670px) {
    //       .u-row-container {
    //         max-width: 100% !important;
    //         padding-left: 0px !important;
    //         padding-right: 0px !important;
    //       }
    //       .u-row .u-col {
    //         min-width: 320px !important;
    //         max-width: 100% !important;
    //         display: block !important;
    //       }
    //       .u-row {
    //         width: calc(100% - 40px) !important;
    //       }
    //       .u-col {
    //         width: 100% !important;
    //       }
    //       .u-col > div {
    //         margin: 0 auto;
    //       }
    //     }
    //     body {
    //       margin: 0;
    //       padding: 0;
    //     }
        
    //     table,
    //     tr,
    //     td {
    //       vertical-align: top;
    //       border-collapse: collapse;
    //     }
        
    //     p {
    //       margin: 0;
    //     }
        
    //     .ie-container table,
    //     .mso-container table {
    //       table-layout: fixed;
    //     }
        
    //     * {
    //       line-height: inherit;
    //     }
        
    //     a[x-apple-data-detectors='true'] {
    //       color: inherit !important;
    //       text-decoration: none !important;
    //     }
        
    //     </style>
          
          
        
    //     <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css"><!--<![endif]-->
        
    //     </head>
        
    //     <body class="clean-body u_body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
    //       <!--[if IE]><div class="ie-container"><![endif]-->
    //       <!--[if mso]><div class="mso-container"><![endif]-->
    //       <table style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%" cellpadding="0" cellspacing="0">
    //       <tbody>
    //       <tr style="vertical-align: top">
    //         <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
    //         <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
            
        
    //     <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //       <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
    //         <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //           <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
              
    //     <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //     <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //       <div style="background-color: #ffffff;width: 100% !important;">
    //       <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
          
    //     <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
    //       <tbody>
    //         <tr>
    //           <td style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;" align="left">
                
    //     <table width="100%" cellpadding="0" cellspacing="0" border="0">
    //       <tr>
    //         <td style="padding-right: 0px;padding-left: 0px;" align="center">
             
    //         <img align="center" border="0"
    //         src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
    //         alt="Image" title="Image"
    //         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
    //         width="100" />
              
    //         </td>
    //       </tr>
    //     </table>
        
    //           </td>
    //         </tr>
    //       </tbody>
    //     </table>
        
    //       <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
    //       </div>
    //     </div>
    //     <!--[if (mso)|(IE)]></td><![endif]-->
    //           <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //         </div>
    //       </div>
    //     </div>
        
        
        
    //     <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //       <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
    //         <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //           <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
              
    //     <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //     <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //       <div style="width: 100% !important;">
    //       <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
          
    //     <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
    //       <tbody>
    //         <tr>
    //           <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px;font-family:'Montserrat',sans-serif;" align="left">
                
    //       <div style="color: #1b262c; line-height: 140%; text-align: center; word-wrap: break-word;">
    //         <p style="font-size: 14px; line-height: 140%;"><strong><span style="font-size: 24px; line-height: 33.6px;">Welcome to ListYourPics</span></strong></p>
    //       </div>
        
    //           </td>
    //         </tr>
    //       </tbody>
    //     </table>
        
    //     <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
    //       <tbody>
    //         <tr>
    //           <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;" align="left">
                
    //       <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
    //         <p style="font-size: 14px; line-height: 140%;">
    //         Dear ${userName},
    //         <br><br>
    //         OTP for your E-mail verification is  ${text}. Please use this  OTP (One-Time-Password) to login to your own ListYourPics and access the unlimited possibilities of ListYourPics.
    //         <br><br>
    //         This OTP is valid for the next 05 minutes and can be used only once.<br><br>
    
    //         <br><br>
    //         Thanks and regards <br>
    //         Team Listyourpic
    //         </p>
    //       </div>
        
    //           </td>
    //         </tr>
    //       </tbody>
    //     </table>
        
    //       <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
    //       </div>
    //     </div>
    //     <!--[if (mso)|(IE)]></td><![endif]-->
    //           <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //         </div>
    //       </div>
    //     </div>
        
        
        
    //     <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //       <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
    //         <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //           <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
              
    //     <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //     <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //       <div style="width: 100% !important;">
    //       <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
          
    //     <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
    //       <tbody>
    //         <tr>
    //           <td style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;" align="left">
                
    //       <div style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
    //         <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPics | All RIghts Reserved</span></p>
    //       </div>
        
    //           </td>
    //         </tr>
    //       </tbody>
    //     </table>
        
    //       <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
    //       </div>
    //     </div>
    //     <!--[if (mso)|(IE)]></td><![endif]-->
    //           <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //         </div>
    //       </div>
    //     </div>
        
        
    //         <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
    //         </td>
    //       </tr>
    //       </tbody>
    //       </table>
    //       <!--[if mso]></div><![endif]-->
    //       <!--[if IE]></div><![endif]-->
    //     </body>
    //     </html>`

    //     var transporter = nodemailer.createTransport({
    //         service: 'gmail',
    //         auth: {
    //             "user": global.gConfig.nodemailer.user,
    //             "pass": global.gConfig.nodemailer.pass
    //         }
    //     });
    //     var mailOptions = {
    //         from: "<do_not_reply@gmail.com>",
    //         to: email,
    //         subject: 'Email OTP for ListYourPics',
    //         //text: text,
    //         html: html
    //     };
    //     transporter.sendMail(mailOptions, function (error, info) {
    //         if (error) {
    //             callback(error, null)
    //         } else {
    //             callback(null, info.response)
    //         }
    //     });
    // },
    // sendMail1: (email, adminName, userEmail, complaintID, userName, subject, text) => {
    //     return new Promise((resolve, reject) => {
    //         let html = `<!DOCTYPE HTML
    //     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    //   <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    //     xmlns:o="urn:schemas-microsoft-com:office:office">
      
    //   <head>
    //     <!--[if gte mso 9]>
    //       <xml>
    //         <o:OfficeDocumentSettings>
    //           <o:AllowPNG/>
    //           <o:PixelsPerInch>96</o:PixelsPerInch>
    //         </o:OfficeDocumentSettings>
    //       </xml>
    //       <![endif]-->
    //     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //     <meta name="x-apple-disable-message-reformatting">
    //     <!--[if !mso]><!-->
    //     <meta http-equiv="X-UA-Compatible" content="IE=edge">
    //     <!--<![endif]-->
    //     <title></title>
      
    //     <style type="text/css">
    //       table,
    //       td {
    //         color: #000000;
    //       }
      
    //       @media only screen and (min-width: 670px) {
    //         .u-row {
    //           width: 650px !important;
    //         }
      
    //         .u-row .u-col {
    //           vertical-align: top;
    //         }
      
    //         .u-row .u-col-100 {
    //           width: 650px !important;
    //         }
      
    //       }
      
    //       @media (max-width: 670px) {
    //         .u-row-container {
    //           max-width: 100% !important;
    //           padding-left: 0px !important;
    //           padding-right: 0px !important;
    //         }
      
    //         .u-row .u-col {
    //           min-width: 320px !important;
    //           max-width: 100% !important;
    //           display: block !important;
    //         }
      
    //         .u-row {
    //           width: calc(100% - 40px) !important;
    //         }
      
    //         .u-col {
    //           width: 100% !important;
    //         }
      
    //         .u-col>div {
    //           margin: 0 auto;
    //         }
    //       }
      
    //       body {
    //         margin: 0;
    //         padding: 0;
    //       }
      
    //       table,
    //       tr,
    //       td {
    //         vertical-align: top;
    //         border-collapse: collapse;
    //       }
      
    //       p {
    //         margin: 0;
    //       }
      
    //       .ie-container table,
    //       .mso-container table {
    //         table-layout: fixed;
    //       }
      
    //       * {
    //         line-height: inherit;
    //       }
      
    //       a[x-apple-data-detectors='true'] {
    //         color: inherit !important;
    //         text-decoration: none !important;
    //       }
    //     </style>
      
      
      
    //     <!--[if !mso]><!-->
    //     <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css">
    //     <!--<![endif]-->
      
    //   </head>
      
    //   <body class="clean-body u_body"
    //     style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
    //     <!--[if IE]><div class="ie-container"><![endif]-->
    //     <!--[if mso]><div class="mso-container"><![endif]-->
    //     <table
    //       style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%"
    //       cellpadding="0" cellspacing="0">
    //       <tbody>
    //         <tr style="vertical-align: top">
    //           <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
    //             <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
    //             <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //               <div class="u-row"
    //                 style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
    //                 <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //                   <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
      
    //                   <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //                   <div class="u-col u-col-100"
    //                     style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //                     <div style="background-color: #ffffff;width: 100% !important;">
    //                       <!--[if (!mso)&(!IE)]><!-->
    //                       <div
    //                         style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
    //                         <!--<![endif]-->
      
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                           cellspacing="0" width="100%" border="0">
    //                           <tbody>
    //                             <tr>
    //                               <td
    //                                 style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;"
    //                                 align="left">
      
    //                                 <table width="100%" cellpadding="0" cellspacing="0" border="0">
    //                                   <tr>
    //                                     <td style="padding-right: 0px;padding-left: 0px;" align="center">
    //                                       <!-- <a>Smart Contract as Service Plateform </a>-->
      
    //                                       <img align="center" border="0"
    //                                         src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
    //                                         alt="Image" title="Image"
    //                                         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
    //                                         width="100" />
      
    //                                     </td>
    //                                   </tr>
    //                                 </table>
    //                               </td>
    //                             </tr>
    //                           </tbody>
    //                         </table>
    //                         <!--[if (!mso)&(!IE)]><!-->
    //                       </div>
    //                       <!--<![endif]-->
    //                     </div>
    //                   </div>
    //                   <!--[if (mso)|(IE)]></td><![endif]-->
    //                   <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //                 </div>
    //               </div>
    //             </div>
    //             <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //               <div class="u-row"
    //                 style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
    //                 <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //                   <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
      
    //                   <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //                   <div class="u-col u-col-100"
    //                     style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //                     <div style="width: 100% !important;">
    //                       <!--[if (!mso)&(!IE)]><!-->
    //                       <div
    //                         style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
    //                         <!--<![endif]-->
      
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                           cellspacing="0" width="100%" border="0">
    //                           <tbody>
    //                             <tr>
    //                               <td
    //                                 style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
    //                                 align="left">
      
    //                                 <div style="color: #1b262c; line-height: 140%;  word-wrap: break-word;">
    //                                   <p style="font-size: 14px; line-height: 140%;"><strong><span
    //                                         style="font-size: 24px; line-height: 33.6px;"> Dear ${adminName}</span></strong>
    //                                   </p>
    //                                 </div>
      
    //                               </td>
    //                             </tr>
    //                           </tbody>
    //                         </table>
      
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                           cellspacing="0" width="100%" border="0">
    //                           <tbody>
    //                             <tr>
    //                               <td
    //                                 style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
    //                                 align="left">
      
    //                                 <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
    //                                   <p style="font-size: 14px; line-height: 140%;">${text}</p>
    //                                 </div>
      
    //                               </td>
    //                             </tr>
    //                           </tbody>
    //                         </table>
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                         cellspacing="0" width="100%" border="0">
    //                         <tbody>
    //                           <tr>
    //                             <td
    //                               style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
    //                               align="left">
    //                               <div style="color: #1b262c; line-height: 140%; text-align: centre; word-wrap: break-word;">
    //                               <tr>
    //                               <th>Name</th>
    //                               <td>${userName}</td>
        
    //                             </tr>
    //                             <tr>
    //                               <th>Email</th>
    //                               <td>${userEmail}</td>
    //                             </tr>
    //                             <tr>
    //                               <th>Complaint ID</th>
    //                               <td>${complaintID}</td>
    //                             </tr>                            
    //                               </div>
    
    //                             </td>
    //                           </tr>
                             
    //                         </tbody>
    //                       </table>
    //                         <!--[if (!mso)&(!IE)]><!-->
    //                       </div>
    //                       <!--<![endif]-->
    //                     </div>
    //                   </div>
    //                   <!--[if (mso)|(IE)]></td><![endif]-->
    //                   <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //                 </div>
    //               </div>
    //             </div>
      
      
      
    //             <div class="u-row-container" style="padding: 10px;background-color: transparent">
    //               <div class="u-row"
    //                 style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
    //                 <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //                   <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
      
    //                   <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //                   <div class="u-col u-col-100"
    //                     style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //                     <div style="width: 100% !important;">
    //                       <!--[if (!mso)&(!IE)]><!-->
    //                       <div
    //                         style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
    //                         <!--<![endif]-->
      
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                           cellspacing="0" width="100%" border="0">
    //                           <tbody>
    //                             <tr>
    //                               <td
    //                                 style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;"
    //                                 align="left">
      
    //                                 <div
    //                                   style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
    //                                   <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span
    //                                       style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPicss | All
    //                                       RIghts Reserved</span></p>
    //                                 </div>
      
    //                               </td>
    //                             </tr>
    //                           </tbody>
    //                         </table>
      
    //                         <!--[if (!mso)&(!IE)]><!-->
    //                       </div>
    //                       <!--<![endif]-->
    //                     </div>
    //                   </div>
    //                   <!--[if (mso)|(IE)]></td><![endif]-->
    //                   <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //                 </div>
    //               </div>
    //             </div>
      
      
    //             <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
    //           </td>
    //         </tr>
    //       </tbody>
    //     </table>
    //     <!--[if mso]></div><![endif]-->
    //     <!--[if IE]></div><![endif]-->
    //   </body>
      
    //   </html>`

    //         var transporter = nodemailer.createTransport({
    //             service: 'gmail',
    //             auth: {
    //                 "user": global.gConfig.nodemailer.user,
    //                 "pass": global.gConfig.nodemailer.pass
    //             }
    //         });
    //         var mailOptions = {
    //             from: "<do_not_reply@gmail.com>",
    //             to: email,
    //             subject: subject,
    //             //text: text,
    //             html: html
    //         };
    //         transporter.sendMail(mailOptions, function (error, info) {
    //             if (error) {
    //                 return reject(error)
    //             } else {
    //                 return resolve(info.response)
    //             }
    //         });
    //     })
    // },
    // sendMail2: (email, adminName, userEmail, projectName, userName, subject, text) => {
    //     return new Promise((resolve, reject) => {

    //         let html = `<!DOCTYPE HTML
    //     PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    //   <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
    //     xmlns:o="urn:schemas-microsoft-com:office:office">
      
    //   <head>
    //     <!--[if gte mso 9]>
    //       <xml>
    //         <o:OfficeDocumentSettings>
    //           <o:AllowPNG/>
    //           <o:PixelsPerInch>96</o:PixelsPerInch>
    //         </o:OfficeDocumentSettings>
    //       </xml>
    //       <![endif]-->
    //     <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    //     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //     <meta name="x-apple-disable-message-reformatting">
    //     <!--[if !mso]><!-->
    //     <meta http-equiv="X-UA-Compatible" content="IE=edge">
    //     <!--<![endif]-->
    //     <title></title>
      
    //     <style type="text/css">
    //       table,
    //       td {
    //         color: #000000;
    //       }
      
    //       @media only screen and (min-width: 670px) {
    //         .u-row {
    //           width: 650px !important;
    //         }
      
    //         .u-row .u-col {
    //           vertical-align: top;
    //         }
      
    //         .u-row .u-col-100 {
    //           width: 650px !important;
    //         }
      
    //       }
      
    //       @media (max-width: 670px) {
    //         .u-row-container {
    //           max-width: 100% !important;
    //           padding-left: 0px !important;
    //           padding-right: 0px !important;
    //         }
      
    //         .u-row .u-col {
    //           min-width: 320px !important;
    //           max-width: 100% !important;
    //           display: block !important;
    //         }
      
    //         .u-row {
    //           width: calc(100% - 40px) !important;
    //         }
      
    //         .u-col {
    //           width: 100% !important;
    //         }
      
    //         .u-col>div {
    //           margin: 0 auto;
    //         }
    //       }
      
    //       body {
    //         margin: 0;
    //         padding: 0;
    //       }
      
    //       table,
    //       tr,
    //       td {
    //         vertical-align: top;
    //         border-collapse: collapse;
    //       }
      
    //       p {
    //         margin: 0;
    //       }
      
    //       .ie-container table,
    //       .mso-container table {
    //         table-layout: fixed;
    //       }
      
    //       * {
    //         line-height: inherit;
    //       }
      
    //       a[x-apple-data-detectors='true'] {
    //         color: inherit !important;
    //         text-decoration: none !important;
    //       }
    //     </style>
      
      
      
    //     <!--[if !mso]><!-->
    //     <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css">
    //     <!--<![endif]-->
      
    //   </head>
      
    //   <body class="clean-body u_body"
    //     style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
    //     <!--[if IE]><div class="ie-container"><![endif]-->
    //     <!--[if mso]><div class="mso-container"><![endif]-->
    //     <table
    //       style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%"
    //       cellpadding="0" cellspacing="0">
    //       <tbody>
    //         <tr style="vertical-align: top">
    //           <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
    //             <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
      
      
    //             <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //               <div class="u-row"
    //                 style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
    //                 <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //                   <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
      
    //                   <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //                   <div class="u-col u-col-100"
    //                     style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //                     <div style="background-color: #ffffff;width: 100% !important;">
    //                       <!--[if (!mso)&(!IE)]><!-->
    //                       <div
    //                         style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
    //                         <!--<![endif]-->
      
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                           cellspacing="0" width="100%" border="0">
    //                           <tbody>
    //                             <tr>
    //                               <td
    //                                 style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;"
    //                                 align="left">
      
    //                                 <table width="100%" cellpadding="0" cellspacing="0" border="0">
    //                                   <tr>
    //                                     <td style="padding-right: 0px;padding-left: 0px;" align="center">
    //                                       <!-- <a>Smart Contract as Service Plateform </a>-->
      
    //                                       <img align="center" border="0"
    //                                         src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
    //                                         alt="Image" title="Image"
    //                                         style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
    //                                         width="100" />
      
    //                                     </td>
    //                                   </tr>
    //                                 </table>
      
    //                               </td>
    //                             </tr>
    //                           </tbody>
    //                         </table>
      
    //                         <!--[if (!mso)&(!IE)]><!-->
    //                       </div>
    //                       <!--<![endif]-->
    //                     </div>
    //                   </div>
    //                   <!--[if (mso)|(IE)]></td><![endif]-->
    //                   <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //                 </div>
    //               </div>
    //             </div>
      
      
      
    //             <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //               <div class="u-row"
    //                 style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
    //                 <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //                   <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
      
    //                   <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //                   <div class="u-col u-col-100"
    //                     style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //                     <div style="width: 100% !important;">
    //                       <!--[if (!mso)&(!IE)]><!-->
    //                       <div
    //                         style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
    //                         <!--<![endif]-->
      
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                           cellspacing="0" width="100%" border="0">
    //                           <tbody>
    //                             <tr>
    //                               <td
    //                                 style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
    //                                 align="left">
      
    //                                 <div style="color: #1b262c; line-height: 140%;  word-wrap: break-word;">
    //                                   <p style="font-size: 14px; line-height: 140%;"><strong><span
    //                                         style="font-size: 24px; line-height: 33.6px;"> Dear ${adminName}</span></strong>
    //                                   </p>
    //                                 </div>
      
    //                               </td>
    //                             </tr>
    //                           </tbody>
    //                         </table>
      
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                           cellspacing="0" width="100%" border="0">
    //                           <tbody>
    //                             <tr>
    //                               <td
    //                                 style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
    //                                 align="left">
      
    //                                 <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
    //                                   <p style="font-size: 14px; line-height: 140%;">${text}</p>
    //                                 </div>
      
    //                               </td>
    //                             </tr>
                               
    //                           </tbody>
    //                         </table>
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                         cellspacing="0" width="100%" border="0">
    //                         <tbody>
    //                           <tr>
    //                             <td
    //                               style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;"
    //                               align="left">
    
    //                               <div style="color: #1b262c; line-height: 140%; text-align: centre; word-wrap: break-word;">
    //                               <tr>
    //                               <th>Name</th>
    //                               <td>${userName}</td>
        
    //                             </tr>
    //                             <tr>
    //                               <th>Email</th>
    //                               <td>${userEmail}</td>
    //                             </tr>
    //                             <tr>
    //                               <th>Project Name</th>
    //                               <td>${projectName}</td>
    //                             </tr>                            
    //                               </div>
    
    //                             </td>
    //                           </tr>
                             
    //                         </tbody>
    //                       </table>
    //                         <!--[if (!mso)&(!IE)]><!-->
    //                       </div>
    //                       <!--<![endif]-->
    //                     </div>
    //                   </div>
    //                   <!--[if (mso)|(IE)]></td><![endif]-->
    //                   <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //                 </div>
    //               </div>
    //             </div>
      
      
      
    //             <div class="u-row-container" style="padding: 10px;background-color: transparent">
    //               <div class="u-row"
    //                 style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
    //                 <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //                   <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
      
    //                   <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //                   <div class="u-col u-col-100"
    //                     style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //                     <div style="width: 100% !important;">
    //                       <!--[if (!mso)&(!IE)]><!-->
    //                       <div
    //                         style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;">
    //                         <!--<![endif]-->
      
    //                         <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0"
    //                           cellspacing="0" width="100%" border="0">
    //                           <tbody>
    //                             <tr>
    //                               <td
    //                                 style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;"
    //                                 align="left">
      
    //                                 <div
    //                                   style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
    //                                   <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span
    //                                       style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPicss | All
    //                                       RIghts Reserved</span></p>
    //                                 </div>
      
    //                               </td>
    //                             </tr>
    //                           </tbody>
    //                         </table>
      
    //                         <!--[if (!mso)&(!IE)]><!-->
    //                       </div>
    //                       <!--<![endif]-->
    //                     </div>
    //                   </div>
    //                   <!--[if (mso)|(IE)]></td><![endif]-->
    //                   <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //                 </div>
    //               </div>
    //             </div>
      
      
    //             <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
    //           </td>
    //         </tr>
    //       </tbody>
    //     </table>
    //     <!--[if mso]></div><![endif]-->
    //     <!--[if IE]></div><![endif]-->
    //   </body>
      
    //   </html>`

    //         var transporter = nodemailer.createTransport({
    //             service: 'gmail',
    //             auth: {
    //                 "user": global.gConfig.nodemailer.user,
    //                 "pass": global.gConfig.nodemailer.pass
    //             }
    //         });
    //         var mailOptions = {
    //             from: "<do_not_reply@gmail.com>",
    //             to: email,
    //             subject: subject,
    //             //text: text,
    //             html: html
    //         };
    //         transporter.sendMail(mailOptions, function (error, info) {
    //             if (error) {
    //                 return reject(error)
    //             } else {
    //                 return resolve(info.response)
    //             }
    //         });
    //     });
    // },
    // WelcomeMail: (email, userName, callback) => {
    //     let html = `<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional //EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    //       <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
    //       <head>
    //       <!--[if gte mso 9]>
    //       <xml>
    //         <o:OfficeDocumentSettings>
    //           <o:AllowPNG/>
    //           <o:PixelsPerInch>96</o:PixelsPerInch>
    //         </o:OfficeDocumentSettings>
    //       </xml>
    //       <![endif]-->
    //         <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    //         <meta name="viewport" content="width=device-width, initial-scale=1.0">
    //         <meta name="x-apple-disable-message-reformatting">
    //         <!--[if !mso]><!--><meta http-equiv="X-UA-Compatible" content="IE=edge"><!--<![endif]-->
    //         <title></title>
            
    //           <style type="text/css">
    //             table, td { color: #000000; } @media only screen and (min-width: 670px) {
    //         .u-row {
    //           width: 650px !important;
    //         }
    //         .u-row .u-col {
    //           vertical-align: top;
    //         }
          
    //         .u-row .u-col-100 {
    //           width: 650px !important;
    //         }
          
    //       }
          
    //       @media (max-width: 670px) {
    //         .u-row-container {
    //           max-width: 100% !important;
    //           padding-left: 0px !important;
    //           padding-right: 0px !important;
    //         }
    //         .u-row .u-col {
    //           min-width: 320px !important;
    //           max-width: 100% !important;
    //           display: block !important;
    //         }
    //         .u-row {
    //           width: calc(100% - 40px) !important;
    //         }
    //         .u-col {
    //           width: 100% !important;
    //         }
    //         .u-col > div {
    //           margin: 0 auto;
    //         }
    //       }
    //       body {
    //         margin: 0;
    //         padding: 0;
    //       }
          
    //       table,
    //       tr,
    //       td {
    //         vertical-align: top;
    //         border-collapse: collapse;
    //       }
          
    //       p {
    //         margin: 0;
    //       }
          
    //       .ie-container table,
    //       .mso-container table {
    //         table-layout: fixed;
    //       }
          
    //       * {
    //         line-height: inherit;
    //       }
          
    //       a[x-apple-data-detectors='true'] {
    //         color: inherit !important;
    //         text-decoration: none !important;
    //       }
          
    //       </style>
            
            
          
    //       <!--[if !mso]><!--><link href="https://fonts.googleapis.com/css?family=Montserrat:400,700&display=swap" rel="stylesheet" type="text/css"><!--<![endif]-->
          
    //       </head>
          
    //       <body class="clean-body u_body" style="margin: 0;padding: 0;-webkit-text-size-adjust: 100%;background-color: #ffffff;color: #000000">
    //         <!--[if IE]><div class="ie-container"><![endif]-->
    //         <!--[if mso]><div class="mso-container"><![endif]-->
    //         <table style="border-collapse: collapse;table-layout: fixed;border-spacing: 0;mso-table-lspace: 0pt;mso-table-rspace: 0pt;vertical-align: top;min-width: 320px;Margin: 0 auto;background-color: #ffffff;width:100%" cellpadding="0" cellspacing="0">
    //         <tbody>
    //         <tr style="vertical-align: top">
    //           <td style="word-break: break-word;border-collapse: collapse !important;vertical-align: top">
    //           <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td align="center" style="background-color: #ffffff;"><![endif]-->
              
          
    //       <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //         <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #dff1ff;">
    //           <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //             <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #dff1ff;"><![endif]-->
                
    //       <!--[if (mso)|(IE)]><td align="center" width="650" style="background-color: #ffffff;width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //       <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //         <div style="background-color: #ffffff;width: 100% !important;">
    //         <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
            
    //       <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
    //         <tbody>
    //           <tr>
    //             <td style="overflow-wrap:break-word;word-break:break-word;padding:13px 0px 15px;font-family:'Montserrat',sans-serif;" align="left">
                  
    //       <table width="100%" cellpadding="0" cellspacing="0" border="0">
    //         <tr>
    //           <td style="padding-right: 0px;padding-left: 0px;" align="center">
                
    //             <img align="center" border="0"
    //             src="https://res.cloudinary.com/listyourpics/image/upload/v1653630166/i0fugotwu56jouwyrmje.png"
    //             alt="Image" title="Image"
    //             style="outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;clear: both;display: inline-block !important;border: none;height: auto;float: none;width: 54%;max-width: 100px;"
    //             width="100" />
      
                
    //           </td>
    //         </tr>
    //       </table>
          
    //             </td>
    //           </tr>
    //         </tbody>
    //       </table>
          
    //         <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
    //         </div>
    //       </div>
    //       <!--[if (mso)|(IE)]></td><![endif]-->
    //             <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //           </div>
    //         </div>
    //       </div>
          
          
          
    //       <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //         <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #f3fbfd;">
    //           <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //             <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #f3fbfd;"><![endif]-->
                
    //       <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //       <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //         <div style="width: 100% !important;">
    //         <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
            
    //       <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
    //         <tbody>
    //           <tr>
    //             <td style="overflow-wrap:break-word;word-break:break-word;padding:40px 10px 10px;font-family:'Montserrat',sans-serif;" align="left">
                  
    //         <div style="color: #1b262c; line-height: 140%; text-align: center; word-wrap: break-word;">
    //           <p style="font-size: 14px; line-height: 140%;"><strong><span style="font-size: 24px; line-height: 33.6px;">Welcome!</span></strong></p>
    //         </div>
          
    //             </td>
    //           </tr>
    //         </tbody>
    //       </table>
          
    //       <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
    //         <tbody>
    //           <tr>
    //             <td style="overflow-wrap:break-word;word-break:break-word;padding:10px 50px 20px;font-family:'Montserrat',sans-serif;" align="left">
                  
    //         <div style="color: #1b262c; line-height: 140%; text-align: left; word-wrap: break-word;">
    //           <p style="font-size: 14px; line-height: 140%;">Hi ${userName},<br><br>
    //           Thanks for choosing  ListYourPics® <br> 
    //           you can learn more about ListYourPics <a href="http://listyourpics.com/">here </a>
    //           <br><br>
    //           Thanks and regards <br>
    //           Team Listyourpic 
    //             </p>
    //         </div>
          
    //             </td>
    //           </tr>
    //         </tbody>
    //       </table>
          
    //         <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
    //         </div>
    //       </div>
    //       <!--[if (mso)|(IE)]></td><![endif]-->
    //             <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //           </div>
    //         </div>
    //       </div>
          
          
          
    //       <div class="u-row-container" style="padding: 0px;background-color: transparent">
    //         <div class="u-row" style="Margin: 0 auto;min-width: 320px;max-width: 650px;overflow-wrap: break-word;word-wrap: break-word;word-break: break-word;background-color: #151418;">
    //           <div style="border-collapse: collapse;display: table;width: 100%;background-color: transparent;">
    //             <!--[if (mso)|(IE)]><table width="100%" cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 0px;background-color: transparent;" align="center"><table cellpadding="0" cellspacing="0" border="0" style="width:650px;"><tr style="background-color: #151418;"><![endif]-->
                
    //       <!--[if (mso)|(IE)]><td align="center" width="650" style="width: 650px;padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;" valign="top"><![endif]-->
    //       <div class="u-col u-col-100" style="max-width: 320px;min-width: 650px;display: table-cell;vertical-align: top;">
    //         <div style="width: 100% !important;">
    //         <!--[if (!mso)&(!IE)]><!--><div style="padding: 0px;border-top: 0px solid transparent;border-left: 0px solid transparent;border-right: 0px solid transparent;border-bottom: 0px solid transparent;"><!--<![endif]-->
            
    //       <table style="font-family:'Montserrat',sans-serif;" role="presentation" cellpadding="0" cellspacing="0" width="100%" border="0">
    //         <tbody>
    //           <tr>
    //             <td style="overflow-wrap:break-word;word-break:break-word;padding:18px;font-family:'Montserrat',sans-serif;" align="left">
                  
    //         <div style="color: #ffffff; line-height: 140%; text-align: center; word-wrap: break-word;">
    //           <p dir="rtl" style="font-size: 14px; line-height: 140%;"><span style="font-size: 14px; line-height: 19.6px;">Copyright @ 2022 ListYourPics | All RIghts Reserved</span></p>
    //         </div>
          
    //             </td>
    //           </tr>
    //         </tbody>
    //       </table>
          
    //         <!--[if (!mso)&(!IE)]><!--></div><!--<![endif]-->
    //         </div>
    //       </div>
    //       <!--[if (mso)|(IE)]></td><![endif]-->
    //             <!--[if (mso)|(IE)]></tr></table></td></tr></table><![endif]-->
    //           </div>
    //         </div>
    //       </div>
          
          
    //           <!--[if (mso)|(IE)]></td></tr></table><![endif]-->
    //           </td>
    //         </tr>
    //         </tbody>
    //         </table>
    //         <!--[if mso]></div><![endif]-->
    //         <!--[if IE]></div><![endif]-->
    //       </body>
    //       </html>`
    //     var transporter = nodemailer.createTransport({
    //         service: 'gmail',
    //         auth: {
    //             "user": global.gConfig.nodemailer.user,
    //             "pass": global.gConfig.nodemailer.pass
    //         }
    //     });
    //     var mailOptions = {
    //         from: "<do_not_reply@gmail.com>",
    //         to: email,
    //         subject: "Welcome",
    //         //text: text,
    //         html: html
    //     };
    //     transporter.sendMail(mailOptions, function (error, info) {
    //         if (error) {
    //             callback(error, null)
    //         } else {
    //             callback(null, info.response)
    //         }
    //     });
    // },
    uploadProfileImage(profilePic) {
        return new Promise((resolve, reject) => {
            cloudinary.uploader.upload(profilePic, function (error, result) {
                if (error) {
                    console.log("==========error")
                    reject(error);
                }
                else {
                    console.log("result", result);
                    resolve(result.secure_url)
                }
            });
        })
    },
    getImageUrlPhase2: async (files) => {
        var result = await cloudinary.uploader.upload(files, { resource_type: "auto", transformation: { duration: 30 } })
        console.log("82", result)
        return result;
    },
    complaintNumber() {
        var otp = Math.floor(1000 + Math.random() * 900000);
        return otp;
    },
    uploadImage: (img, callback) => {
        cloudinary.uploader.upload(img, (error, result) => {
            if (error) {
                callback(error, null)
            }
            else {
                callback(null, result.secure_url)
            }
        })
    },
    getSecureUrl: async (base64) => {
        var result = await cloudinary.uploader.upload(base64, { resource_type: "auto" });
        return result.secure_url;
    },
    videoUpload(base64) {
        return new Promise((resolve, reject) => {
            cloudinary.uploader.upload(base64, {
                resource_type: "video",
            },
                function (error, result) {
                    if (error) {
                        console.log("==========error")
                        reject(error);
                    }
                    else {
                        console.log("result", result);
                        resolve(result.secure_url)
                    }
                });
        })
    },


  testMail: (email,otp, callback) => {
        var transporter = nodemailer.createTransport({
        host: 'vps.houszzz.com',  // listyourpics.com
        port: 25,
        secure: false, // true for 465, false for other ports
        auth: {
          user: 'noreply@houszzz.com', // your domain email address         'noreply@listyourpics.com'
          pass: 'd$aQt7otw' // your password                d$aQt7otw           'Ef!8U&j*O'
        }
      });
      var mailOptions = {
        from: 'noreply@houszzz.com',
        to: email,
        subject: "Hello",
        text : otp
      };
    //   var transporter = nodemailer.createTransport({
    //     host: 'mail.listyourpics.com',
    //     port: 25,
    //     secure: false, // true for 465, false for other ports
    //     auth: {
    //       user: 'noreply@listyourpics.com', // your domain email address
    //       pass: 'Ef!8U&j*O' // your password
    //     }
    //   });
    //   var mailOptions = {
    //     from: 'noreply@listyourpics.com',
    //     to: email,
    //     subject: "Hello",
    //     text : otp
    //   };
      // var transporter = nodemailer.createTransport({
      //     service: 'gmail',
      //     auth: {
      //         "user": global.gConfig.nodemailer.user,
      //         "pass": global.gConfig.nodemailer.pass
      //     }
      // });
      // var mailOptions = {
      //     from: "<do_not_reply@gmail.com>",
      //     to: email,
      //     subject: "Welcome",
      //     //text: text,
      //     html: html
      // };
      transporter.sendMail(mailOptions, function (error, info) {
          if (error) {
              console.log(error)
              callback(error, null)
          } else {
              callback(null, info.response)
          }
      });
  },


}