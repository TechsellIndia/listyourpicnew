const config = require('../config/config');
var nodemailer = require('nodemailer');
const twilio = require('twilio');
const axios = require('axios');
const accountSid = 'ACf786a64203b2524f8ee2878ee632bbe7';
const authToken = '7aa1973b369cf9bdae1ac11b83498472';
const client = require('twilio')(accountSid, authToken);
const userModel = require('../models/user');
var cloudinary = require('cloudinary');
const async = require('async');
const AWS = require('aws-sdk');
const sharp = require("sharp");
const fs = require('fs');
const AWSCredentials = {
    accessKey: 'AKIAR3CAGJID5VW473FN',
    secret: '7jBImU4UM/spFPhK913R5aVFg6Lr0x2uFBaYkTZs',
    bucketName: 'list-your-pic-bucket'
};

const s3 = new AWS.S3({
    accessKeyId: AWSCredentials.accessKey,
    secretAccessKey: AWSCredentials.secret
});
cloudinary.config({
    cloud_name: global.gConfig.cloudinary.cloud_name,
    api_key: global.gConfig.cloudinary.api_key,
    api_secret: global.gConfig.cloudinary.api_secret
});
cloudinary.config({
    cloud_name: 'listyourpics',
    api_key: '385339736967384',
    api_secret: 'Ehs4EJc1UoUHo6YaeI93b45qFBk'
});
module.exports = {
    imageUpload: async (base64, imageName) => {
        const base64Data = new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        console.log(base64Data)
        const type = base64.split(';')[0].split('/')[1];
        let convertLowerCase = type.toLowerCase()
        const params = {
            Bucket: "list-your-pic-bucket",
            Key: `${imageName}.${convertLowerCase}`,
            Body: base64Data,
            ContentEncoding: 'base64',
            ContentType: `image/${type}`
        }
        return s3.upload(params).promise();
    },
    imageUpload1: (file) => {
        let fileName = file.originalname.split("/").pop().split(".")[0]
        let type = file.filename.substring(file.filename.lastIndexOf('.') + 1, file.filename.length);
        let convertLowerCase = type.toLowerCase()
        const fileStream = fs.createReadStream(file.path);
        const uploadParams = {
            Bucket: "list-your-pic-bucket",
            Body: fileStream,
            Key: `${fileName}.${convertLowerCase}`,
        };
        return s3.upload(uploadParams).promise();
    },

    imageUpload2: async (file) => {
        let fileName = file.originalname.split("/").pop().split(".")[0]
        let type = file.filename.substring(file.filename.lastIndexOf('.') + 1, file.filename.length);
        let convertLowerCase = type.toLowerCase()
        const fileStream = fs.createReadStream(file.path);
        const firstS3Upload = {
            Bucket: "list-your-pic-bucket",
            Body: fileStream,
            Key: `${fileName}.${convertLowerCase}`,
        };
        let a = await s3.upload(firstS3Upload).promise();
        const response = await axios.get(a.Location, {
            responseType: "arraybuffer",
        });
        const buffer = new Buffer.from(response.data, "utf-8").toString("base64");
        let t1 = "data:image/jpeg;base64,";
        let base64 = t1.concat(buffer);
        const base64Data = new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        var matches = base64.match(/^data:([A-Za-z-+/]+);base64,(.+)$/),
            response1 = {};
        if (matches.length !== 3) {
            return new Error('Invalid input string');
        }
        response1.type = matches[1];
        response1.data = new Buffer(matches[2], 'base64');
        let decodedImg = response1;
        let imageBuffer = decodedImg.data;
        let fileName1 = `${fileName}.${convertLowerCase}`;
        fs.writeFileSync("./uploads/" + fileName1, base64Data, 'utf8');
        const compressedImage200Promise1 = sharp(base64Data)
            .toFormat('jpeg')
            .jpeg({
                force: true,
            })
            .resize({
                width: 500,
                withoutEnlargement: true,
            })
            .toBuffer();
        const [compressedImage200,] = await Promise.all([
            compressedImage200Promise1
        ]);
        const Thirds3upload = s3.upload({
            Bucket: "list-your-pic-bucket",
            Key: "200/" + `${fileName}.${convertLowerCase}`,
            Body: compressedImage200,
            ContentEncoding: 'base64',
            ContentType: `image/${type}`
        }).promise();

        // const secondS3Upload = s3.upload({
        //     Bucket: "list-your-pic-bucket",
        //     Key: `${fileName}.${convertLowerCase}`,
        //     Body: compressedImage800,
        //     ContentEncoding: 'base64',
        //     ContentType: `image/${type}`
        // }).promise();
        return await Promise.all([Thirds3upload]);
    },
    imageUpload3: async (base64, imageName) => {
        const base64Data = new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ""), 'base64');
        const type = base64.split(';')[0].split('/')[1];
        let convertLowerCase = type.toLowerCase()

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var matches = base64.match(/^data:([A-Za-z-+/]+);base64,(.+)$/),
            response = {};
        if (matches.length !== 3) {
            return new Error('Invalid input string');
        }
        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');
        let decodedImg = response;
        let imageBuffer = decodedImg.data;
        let fileName = `${imageName}.${convertLowerCase}`;
        fs.writeFileSync("./uploads/" + fileName, base64Data, 'utf8');
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        const compressedImage200Promise = sharp(base64Data)
            .toFormat('jpeg')
            .jpeg({
                force: true,
            })
            .resize({
                width: 500,
                withoutEnlargement: true,
            })
            .toBuffer();
        const compressedImage800Promise = base64Data;
        console.log("compressedImage800Promise=================>", compressedImage800Promise);
        const [compressedImage200, compressedImage800] = await Promise.all([
            compressedImage200Promise, compressedImage800Promise,
        ]);
        // Upload the compressed images appropriate folders in the Compressed Images bucket
        const firstS3Upload = s3.upload({
            Bucket: "list-your-pic-bucket",
            Key: "200/" + `${imageName}.${convertLowerCase}`,
            Body: compressedImage200,
            ContentEncoding: 'base64',
            ContentType: `image/${type}`
        }).promise();

        const secondS3Upload = s3.upload({
            Bucket: "list-your-pic-bucket",
            Key: `${imageName}.${convertLowerCase}`,
            Body: compressedImage800,
            ContentEncoding: 'base64',
            ContentType: `image/${type}`
        }).promise();
        return await Promise.all([firstS3Upload, secondS3Upload]);

    },

}