const mongoose = require("mongoose");
const { Schema } = mongoose;
const afterBeforeImageSchema = new Schema({
    name: {
        type: String,
    },
    afterImage: {
        type: String
    },
    beforeImage: {
        type: String
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    }
});
const afterBeforeImage = mongoose.model("afterBeforeImage", afterBeforeImageSchema);
module.exports = { afterBeforeImage };
