const { array } = require("joi");
const mongoosePaginate = require("mongoose-paginate");
const mongoose = require("mongoose");
const { Schema } = mongoose;
const commentSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
    },
    complaintId: {
        type: String
    },
    fname: {
        type: String
    },
    userType: {
        type: String
    },
    image: {
        type: String
    },
    title: {
        type: String
    },
    screenShot: {
        type: Array
    },
    comment: {
        type: String,
    },
    date: {
        type: String
    },
    reply: [{
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "user"
        },
        admin: {
            type: mongoose.Schema.Types.ObjectId,
            ref: "AdminSchema"
        },
        fname: {
            type: String
        },
        userType: {
            type: String
        },
        image: {
            type: String
        },
        screenShot: {
            type: Array
        },
        comment: {
            type: String
        },
        date: {
            type: String
        }
    }],
    queryStatus: {
        type: String,
        enum: ["PENDING", "CLOSE"],
        default: "PENDING"
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    }
},
    { timestamps: true });
commentSchema.plugin(mongoosePaginate);
const comments = mongoose.model("comment", commentSchema);
module.exports = { comments };
