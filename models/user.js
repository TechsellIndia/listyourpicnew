const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const Joi = require("joi");
const passwordComplexity = require("joi-password-complexity");
const mongoosePaginate = require("mongoose-paginate");
var mongooseAggregatePaginate = require("mongoose-aggregate-paginate");
const bcrypt = require("bcrypt-nodejs");
const userSchema = new mongoose.Schema({
  company: { type: String, required: true },
  email: { type: String, required: true },
  password: { type: String, required: true },
  fname: { type: String, required: true },
  lname: { type: String, required: true },
  phone: { type: String, required: true },
  image: { type: String },
  otpVerified: {
    type: Boolean,
    default: false
  },
  extraoption: { type: String },
  userType: {
    type: String,
    enum: ["ADMIN", "COMPANY"],
    default: "COMPANY",
    uppercase: true
  },
  status: {
    type: String,
    enum: ["ACTIVE", "BLOCK", "DELETE"],
    default: "ACTIVE"
  }
}, { timestamps: true });
userSchema.methods.generateAuthToken = function () {
  const token = jwt.sign({ _id: this._id }, process.env.JWTPRIVATEKEY, {
    expiresIn: "365d",
  });
  return token;
};
userSchema.plugin(mongoosePaginate);
const User = mongoose.model("user", userSchema);
const validate = (data) => {
  const schema = Joi.object({
    company: Joi.string().required().label("company"),
    email: Joi.string().email().required().label("email"),
    password: passwordComplexity().required().label("password"),
    fname: Joi.string().required().label("fname"),
    lname: Joi.string().required().label("lname"),
    phone: Joi.string().required().label("phone"),
    extraoption: Joi.string().required().label("option"),
  });
  return schema.validate(data);
};
module.exports = { User, validate };