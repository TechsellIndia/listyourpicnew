const mongoose = require("mongoose");
const { Schema } = mongoose;
const bannerCategorySchema = new Schema({
    categoryName: {
        type: String,
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    }
});
const bannerCategory = mongoose.model("bannerCategory", bannerCategorySchema);
module.exports = { bannerCategory };

bannerCategory.find({}, (err, result) => {
    if (err) {
        console.log("Default bannerCategory error", err);
    }
    else if (result.length != 0) {
        console.log("Default bannerCategory content");
    }
    else {
        var obj1 = {
            categoryName: "IMAGE ENHANCEMENT"
        };
        var obj2 = {
            categoryName: "TWILIGHT"
        };
        let obj3 = {
            categoryName: "FLOOR PLANS"
        };
        let obj4 = {
            categoryName: "ITEM REMOVAL"
        };
        let obj5 = {
            categoryName: "VIRTUAL STAGGING"
        };
        let obj6 = {
            categoryName: "ARIAL EDITING"
        };
        let obj7 = {
            categoryName: "360DEG IMAGE ENHANCEMENT"
        };
        let obj8 = {
            categoryName: "VIRTUAL RENOVATION"
        };
        mongoose.model("bannerCategory", bannerCategorySchema).create(obj1, obj2, obj3, obj4, obj5, obj6, obj7, obj8, (staticErr, staticResult) => {
            if (staticErr) {
                console.log("content bannerCategory error.", staticErr);
            }
            else {
                console.log("content bannerCategory created.", staticResult)
            }
        })
    }
})
