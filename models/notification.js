const mongoosePaginate = require("mongoose-paginate");
const mongoose = require("mongoose");
const { Schema } = mongoose;
const Notification = new Schema({
    user: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    assignUser: { type: mongoose.Schema.Types.ObjectId, ref: "AdminSchema" },
    projectId: {
        type: mongoose.Schema.Types.ObjectId, ref: "createProject"
    },
    title: {
        type: String
    },
    body: {
        type: String
    },
    notificationType: {
        type: String
    },
    isRead: {
        type: Boolean,
        default: false
    },
    adminIsRead: {
        type: Boolean,
        default: false
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    },
},
    {
        timestamps: true,
    }
);
Notification.plugin(mongoosePaginate);
const notification = mongoose.model("notification", Notification);
module.exports = { notification };
