const mongoose = require("mongoose");
const { Schema } = mongoose;
const reportSectionAssigned = new Schema({
    report: { type: mongoose.Schema.Types.ObjectId, ref: "report" },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    title: {
        type: String
    },
    description: {
        type: String,
        default: ""
    },
    images: [{
        image: { type: String },
        description: { type: String, default: "" }
    }],
});
const reportSection = mongoose.model("reportSection", reportSectionAssigned);
module.exports = { reportSection };
