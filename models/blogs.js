const mongoose = require("mongoose");
const { Schema } = mongoose;
const blogSchema = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "adminSchema",
  },
  title: {
    type: String,
    required: true,
  },
  image: {
    type: String
  },
  slug: {
    type: String
  },
  blogs: {
    type: String,
    required: true,
  },
});
const blogs = mongoose.model("blogs", blogSchema);
module.exports = { blogs };
