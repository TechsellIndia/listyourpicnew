const mongoose = require("mongoose");
const { Schema } = mongoose;
const OtpSchema = new Schema(
  {
    email: { type: String, required: true },
    otp: { type: String },
    expireIn: { type: Number },
  },
  {
    timestamps: true,
  }
);

const otp = mongoose.model("otp", OtpSchema, "otp");
module.exports = otp;
