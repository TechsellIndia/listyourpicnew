const mongoosePaginate = require("mongoose-paginate");
const mongoose = require("mongoose");
const { Schema } = mongoose;
const createprojectSchema = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    assignTransactionId: [{
      editor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "adminSchema",
      },
      transactionId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "transactiondetail",
      },
      assignDate: {
        type: String,
      }
    }],
    projectname: {
      type: String,
      required: true,
    },
    address1: {
      type: String,
      required: true,
    },
    address2: {
      type: String,
      required: true,
    },
    city: {
      type: String,
      required: true,
    },
    state: {
      type: String,
      required: true,
    },
    pcode: {
      type: String,
      required: true,
    },
    description: [{
      type: String
    }],
    Comment: [{
      user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
      },
      Comment: {
        type: String
      },
      date: {
        type: String
      },
    }],
    originalImage: [{
      image: { type: String },
      kbImage: { type: String },
      path: {
        type: String
      },
      description: { type: String },
      Comment: [{
        user: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "user"
        },
        admin: {
          type: mongoose.Schema.Types.ObjectId,
          ref: "AdminSchema"
        },
        fname: {
          type: String
        },
        userType: {
          type: String
        },
        image: {
          type: String
        },
        Comment: {
          type: String
        },
        date: {
          type: String
        }
      }],
      imageId: { type: String },
    }],
    originalDriveLink: [{
      linkTitle: {
        type: String
      },
      link: {
        type: String
      },
      totalImage:{
        type: String
      }
    }],
    editDriveLink: [{
      linkTitle: {
        type: String
      },
      link: {
        type: String
      }
    }],
    totalImage: {
      type: String
    },
    projectType: {
      type: String,
      enum: ['IMAGE', 'LINK'],
    },
    status: {
      type: String, enum: ['PENDING', 'INPROCESS', 'COMPLETE'],
      default: 'PENDING'
    },
  },
  { timestamps: true }
);
createprojectSchema.plugin(mongoosePaginate);
const createProject = mongoose.model("createProject", createprojectSchema);
module.exports = { createProject };
