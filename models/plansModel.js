const mongoose = require("mongoose");
const schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate");

var plan = new schema(
    {
        userId: {
            type: schema.Types.ObjectId,
            ref: "users"
        },
        imagePackageId: {
            type: schema.Types.ObjectId,
            ref: "package"
        },
        storagePackageId: {
            type: schema.Types.ObjectId,
            ref: "storagePakages"
        },
        packageType: {
            type: String,
            enum: ["STORAGE", "IMAGE"],
        },
        planType: {
            type: String
        },
        amount: {
            type: String
        },
        planName: {
            type: String
        },
        startPlans: {
            type: Number,
        },
        status: {
            type: String,
            enum: ["ACTIVE", "BLOCK", "DELETE"],
            default: "ACTIVE"
        }
    },
    {
        timestamps: true
    }
);

plan.plugin(mongoosePaginate);
module.exports = mongoose.model("plan", plan);
