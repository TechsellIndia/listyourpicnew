const mongoose = require("mongoose");
const { Schema } = mongoose;
const bannerSchema = new Schema({
    bannerName: {
        type: String,
    },
    bannerImage: {
        type: String
    },
    bannerCategory: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "bannerCategory",
    },
    categoryName: {
        type: String
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    }
});
const banner = mongoose.model("banner", bannerSchema);
module.exports = { banner };
