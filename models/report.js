const mongoose = require("mongoose");
const { Schema } = mongoose;
const reportAssigned = new Schema({
    project: { type: mongoose.Schema.Types.ObjectId, ref: "createProject" },
    user: { type: mongoose.Schema.Types.ObjectId, ref: "user" },
    title: {
        type: String
    },
    photoLayout: {
        type: String,
        enum: ["TWO", "THREE", "FOUR"],
        default: "TWO"
    }
});
const report = mongoose.model("report", reportAssigned);
module.exports = { report };
