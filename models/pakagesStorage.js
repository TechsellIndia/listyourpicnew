const mongoose = require("mongoose");
const schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate");

var storagePakagesModel = new schema(
    {
        planName: {
            type: String
        },
        planType: {
            type: String
        },
        currency: {
            type: String
        },
        amount: {
            type: Number
        },
        status: {
            type: String,
            enum: ["ACTIVE", "DELETE", "BLOCK"],
            default: "ACTIVE"
        }
    },
    {
        timestamps: true
    }
);
storagePakagesModel.plugin(mongoosePaginate);
module.exports = mongoose.model("storagePakages", storagePakagesModel);
mongoose.model("storagePakages", storagePakagesModel).find({}, (err, result) => {
    if (err) {
        console.log("Default storage Pakages error", err);
    }
    else if (result.length != 0) {
        console.log("Default storage Pakages content");
    }
    else {
        var obj1 = {
            planType: "FREE",
            planName: "FREE PLAN",
            currency: "$",
            amount: 0,
        };
        var obj2 = {
            planType: "ENTERPRISE",
            planName: "ENTERPRISE PLAN",
            currency: "$",
            amount: 1000,
        };

        mongoose.model("storagePakages", storagePakagesModel).create(obj1, obj2, (staticErr, staticResult) => {
            if (staticErr) {
                console.log("content storage Pakages error.", staticErr);
            }
            else {
                console.log("content storage Pakages created.", staticResult)
            }
        })
    }
})
