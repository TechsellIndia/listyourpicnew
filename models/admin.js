const mongoose = require("mongoose");
const { Schema } = mongoose;
const bcrypt = require("bcryptjs");
const AdminSchema = new mongoose.Schema(
    {
        name: {
            type: String
        },
        email: {
            type: String,
        },
        password: {
            type: String,
        },
        image: {
            type: String
        },
        mobileNumber: {
            type: String,
            required: true
        },
        address: {
            type: String
        },
        otp: { type: String },
        expireIn: { type: Number },
        otpVerified: {
            type: Boolean,
            default: false
        },
        permissions: {
            addEmployee: {
                type: Boolean,
                default: false
            },
            viewEmployee: {
                type: Boolean,
                default: false
            },
            EditEmployee: {
                type: Boolean,
                default: false
            },
            deleteEmployee: {
                type: Boolean,
                default: false
            },
            addManager: {
                type: Boolean,
                default: false
            },
            viewManager: {
                type: Boolean,
                default: false
            },
            EditManager: {
                type: Boolean,
                default: false
            },
            deleteManager: {
                type: Boolean,
                default: false
            },
            viewProject: {
                type: Boolean,
                default: false
            },
            EditProject: {
                type: Boolean,
                default: false
            },
            viewUser: {
                type: Boolean,
                default: false
            },
            EditUser: {
                type: Boolean,
                default: false
            },
            deleteUser: {
                type: Boolean,
                default: false
            },
            bannerManagement: {
                type: Boolean,
                default: false
            },
            blogManagement: {
                type: Boolean,
                default: false
            },
            staticContentManagement: {
                type: Boolean,
                default: false
            },
        },
        userType: {
            type: String,
            enum: ["ADMIN", "MANAGER", "EMPLOYEE"],
            default: "MANAGER",
            uppercase: true
        },
    },
    { timestamps: true }
);
AdminSchema.methods.generateAuthToken = function () {
    const token = jwt.sign({ _id: this._id }, process.env.JWTPRIVATEKEY, {
        expiresIn: "7d",
    });
    return token;
};

const Adminschema = mongoose.model("AdminSchema", AdminSchema);

// const validate = (data) => {
//   const schema = Joi.object({
//     email: Joi.string().email().required().label("email"),
//     password: passwordComplexity().required().label("password"),
//     role: Joi.string().required().label("role"),
//   });
//   return schema.validate(data);
// };

// const MultipleImage = mongoose.model("MultipleImage", mulitipleImageSchema);
module.exports = { Adminschema };
Adminschema.findOne({ userType: "ADMIN" }, async (err, result) => {
    if (err) {
        console.log(err);
    }
    else if (result) {
        console.log("Default admin created");
    }
    else {
        let insertObj = {
            email: "admin@listyourpics.com",
            password: bcrypt.hashSync("Admin"),
            name: "Admin",
            userType: "ADMIN",
            mobileNumber: "7983270586",
            image: "https://res.cloudinary.com/mobiloittetech/image/upload/v1650975252/xkngdqly2kpzedphq04t.png",
            otp: 1234,
            otpVerified: true,
            permissions: {
                "addEmployee": true,
                "viewEmployee": true,
                "EditEmployee": true,
                "deleteEmployee": true,
                "addManager": true,
                "viewManager": true,
                "EditManager": true,
                "deleteManager": true,
                "viewProject": true,
                "EditProject": true,
                "viewUser": true,
                "EditUser": true,
                "deleteUser": true,
                "bannerManagement": true,
                "blogManagement": true,
                "projectManagement": true,
                "staticContentManagement": true
            }
        }
        let save = await Adminschema.create(insertObj);
        console.log("Admin create succesfully", save);
    }
})
