const mongoose = require("mongoose");
const { Schema } = mongoose;
const bcrypt = require("bcryptjs");
const videoSchema = new mongoose.Schema(
    {
        videoLink: {
            type: String,
        },
        status: {
            type: String,
            enum: ["ACTIVE", "BLOCKED", "DELETE"],
            default: "ACTIVE"
        }
    },
    { timestamps: true }
);
const videoschema = mongoose.model("video", videoSchema);
module.exports = { videoschema };