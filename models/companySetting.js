const mongoose = require("mongoose");
const schema = mongoose.Schema;
const mongoosePaginate = require("mongoose-paginate");

var companySetting = new schema(
    {
        userId: {
            type: schema.Types.ObjectId,
            ref: "users"
        },
        companyName: {
            type: String
        },
        address: {
            type: String
        },
        city: {
            type: String
        },
        state: {
            type: String
        },
        country: {
            type: String
        },
        phone: {
            type: String
        },
        landlineNo: {
            type: String
        },
        email: {
            type: String
        },
        image: {
            type: String
        },
        status: {
            type: String,
            enum: ["ACTIVE", "BLOCK", "DELETE"],
            default: "ACTIVE"
        }
    },
    {
        timestamps: true
    }
);

companySetting.plugin(mongoosePaginate);
module.exports = mongoose.model("companySetting", companySetting);
