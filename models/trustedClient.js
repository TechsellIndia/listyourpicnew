const mongoose = require("mongoose");
const { Schema } = mongoose;
const trustedClientSchema = new Schema({
    clientName: {
        type: String,
    },
    image: {
        type: String
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    }
});
const trustedClient = mongoose.model("trustedClient", trustedClientSchema);
module.exports = { trustedClient };
