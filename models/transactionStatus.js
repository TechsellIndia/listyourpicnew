const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate");
const { Schema } = mongoose;

const transactionStatus = new Schema({
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "user",
  },
  project: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "createProject",
  },
  package: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "package"
  },
  storagePakages: {
    type: mongoose.Schema.Types.ObjectId,
    ref: "storagePakages"
  },
  planType: {
    type: String
  },
  planName: {
    type: String
  },
  amount: {
    type: Number
  },
  selectedImage: {
    type: Array
  },
  editImage: [{
    image: { type: String },
    kbImage: { type: String },
    path: {
      type: String
    },
    imageId: { type: String },
  }],
  tokenId: {
    type: String
  },
  transactionStatus: {
    type: String
  },
  totalImage: {
    type: Number
  },
  assignStatus: {
    type: String,
    enum: ['PENDING', 'INPROCESS', 'COMPLETE'],
    default: 'PENDING'
  },
  transactionId: {
    type: String
  }
},
  { timestamps: true });
transactionStatus.plugin(mongoosePaginate);
const transactiondetail = mongoose.model("transactiondetail", transactionStatus);
module.exports = { transactiondetail };


