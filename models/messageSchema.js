const mongoose = require("mongoose");
const { Schema } = mongoose;
const messageSchema = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "user",
    },
    project: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "createProject",
    },
    message: {
      type: String,
      required: true,
    },
  },
  { timestamps: true }
);
const messageschema = mongoose.model("messageschema", messageSchema);
module.exports = messageschema;
