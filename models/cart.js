const mongoose = require("mongoose");
const { Schema } = mongoose;

const cartStatus = new Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "user",
    },
    projectId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "createProject",
    },
    packageId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "package"
    },
    link: {
        type: String
    },
    selectedImage: {
        type: Array
    },
    totalImage: {
        type: String
    },
    amount: {
        type: String
    },
    projectType: {
        type: String,
        enum: ['IMAGE', 'LINK'],
    },
    status: {
        type: String,
        enum: ["ACTIVE", "BLOCK", "DELETE"],
        default: "ACTIVE"
    }
});
const cartdetail = mongoose.model("cart", cartStatus);
module.exports = { cartdetail };


