const mongoose = require("mongoose");
const { Schema } = mongoose;
const packageSchema = new Schema(
  {
    packageName: {
      type: String,
    },
    amount: {
      type: Number,
    },
    validity: {
      type: Number,
    },
    imageQuantity: {
      type: Number,
    },
    status: {
      type: String,
      enum: ["ACTIVE", "BLOCK", "DELETE"],
      default: "ACTIVE"
    }
  },
  {
    timestamps: true,
  }
);
const package = mongoose.model("package", packageSchema);
module.exports = { package };
