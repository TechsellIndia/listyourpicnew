const router = require("express").Router();
const fetchuser = require("../../middleware/fetchuser");
const projectController = require("../../controllers/projectController1");
const { upload } = require("../../helper/filehelper");

// const { createProject } = require("../../models/createproject");
// const messageschema = require("../../models/messageSchema");
// // const { MultipleImage } = require("../models/ImageSchema");
// const { package } = require("../../models/PackageSchema");
// // var paypal = require("paypal-rest-sdk");
// const baseUrl = "http://localhost:8080/";
// const fileSizeFormatter = (bytes, decimal) => {
//   if (bytes === 0) {
//     return "0 Bytes";
//   }
//   const dm = decimal || 2;
//   const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "YB", "ZB"];
//   const index = Math.floor(Math.log(bytes) / Math.log(1000));
//   return (
//     parseFloat((bytes / Math.pow(1000, index)).toFixed(dm)) + " " + sizes[index]
//   );
// };
// router.post("/multipleimage", fetchuser, upload.array("files"), multipleFileUploader);


/**
* @swagger
* /api/v1/project1/uploadPhoto:
*   post:
*     tags:
*       - PROJECT X
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: image
*         description: image ?? base64
*         in: formData
*         required: true 
*       - name: imageName
*         description: imageName ?? base64
*         in: formData
*         required: true   
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.post('/uploadPhoto', upload.single('image'), projectController.uploadPhoto);
/**
* @swagger
* /api/v1/project1/uploadPhoto1:
*   post:
*     tags:
*       - PROJECT X
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:   
*       - name: files
*         description: files
*         in: formData
*         type: file
*         required: true
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/uploadPhoto1', upload.array("files"), projectController.uploadPhoto1);
/**
* @swagger
* /api/v1/project1/createproject:
*   post:
*     tags:
*       - PROJECT X
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: projectname
*         description: projectname
*         in: formData
*         required: false
*       - name: address1
*         in: formData
*         required: false
*       - name: address2
*         description: address2
*         in: formData
*         required: false
*       - name: city
*         description: city
*         in: formData
*         required: false
*       - name: pcode
*         description: pcode
*         in: formData
*         required: false
*       - name: state
*         description: state
*         in: formData
*         required: false  
*     responses:
*       200:
*         description: Requested data found.
*       404: 
*         description: Requested data not found.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*       405:
*         description: You are not allowed to access the YNOT-panel./You don't have permission to access this API.
*/
router.post("/createproject", fetchuser, projectController.createproject);
/**
* @swagger
* /api/v1/project1/uploadImageInProject/{_id}:
*   post:
*     tags:
*       - PROJECT X
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true   
*       - name: files
*         description: files
*         in: formData
*         type: file
*         required: true
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/uploadImageInProject/:_id', fetchuser, upload.array("files"), projectController.uploadImageInProject);
/**
* @swagger
* /api/v1/project1/uploadImageInProjectBase64:
*   post:
*     tags:
*       - PROJECT X
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - in: body
*         name: image
*         description: image Edit.
*         schema:
*           type: object
*           required:
*             - _id
*           properties:
*             _id:
*               type: string
*             image:
*               type: array
*               items:
*                type: string 
*             imageName:
*               type: array
*               items:
*                type: string 
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/uploadImageInProjectBase64', fetchuser, projectController.uploadImageInProjectBase64)
module.exports = router;

