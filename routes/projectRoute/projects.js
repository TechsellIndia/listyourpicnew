const router = require("express").Router();
const fetchuser = require("../../middleware/fetchuser");
const projectController = require("../../controllers/projectController");
const { upload } = require("../../helper/filehelper");

// const { createProject } = require("../../models/createproject");
// const messageschema = require("../../models/messageSchema");
// // const { MultipleImage } = require("../models/ImageSchema");
// const { package } = require("../../models/PackageSchema");
// // var paypal = require("paypal-rest-sdk");
// const baseUrl = "http://localhost:8080/";
// const fileSizeFormatter = (bytes, decimal) => {
//   if (bytes === 0) {
//     return "0 Bytes";
//   }
//   const dm = decimal || 2;
//   const sizes = ["Bytes", "KB", "MB", "GB", "TB", "PB", "EB", "YB", "ZB"];
//   const index = Math.floor(Math.log(bytes) / Math.log(1000));
//   return (
//     parseFloat((bytes / Math.pow(1000, index)).toFixed(dm)) + " " + sizes[index]
//   );
// };
// router.post("/multipleimage", fetchuser, upload.array("files"), multipleFileUploader);


/**
* @swagger
* /api/v1/project/uploadPhoto:
*   post:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: image
*         description: image ?? base64
*         in: formData
*         required: true 
*       - name: imageName
*         description: imageName ?? base64
*         in: formData
*         required: true   
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.post('/uploadPhoto', upload.single('image'), projectController.uploadPhoto);
/**
* @swagger
* /api/v1/project/uploadPhoto1:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:   
*       - name: files
*         description: files
*         in: formData
*         type: file
*         required: true
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/uploadPhoto1', upload.array("files"), projectController.uploadPhoto1);
/**
* @swagger
* /api/v1/project/createproject:
*   post:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: projectname
*         description: projectname
*         in: formData
*         required: false
*       - name: address1
*         in: formData
*         required: false
*       - name: address2
*         description: address2
*         in: formData
*         required: false
*       - name: city
*         description: city
*         in: formData
*         required: false
*       - name: pcode
*         description: pcode
*         in: formData
*         required: false
*       - name: state
*         description: state
*         in: formData
*         required: false  
*     responses:
*       200:
*         description: Requested data found.
*       404: 
*         description: Requested data not found.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*       405:
*         description: You are not allowed to access the YNOT-panel./You don't have permission to access this API.
*/
router.post("/createproject", fetchuser, projectController.createproject);
/**
* @swagger
* /api/v1/project/uploadImageInProject/{_id}:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true   
*       - name: files
*         description: files
*         in: formData
*         type: file
*         required: true
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/uploadImageInProject/:_id', fetchuser, upload.array("files"), projectController.uploadImageInProject);
/**
* @swagger
* /api/v1/project/uploadImageInProjectBase64:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - in: body
*         name: image
*         description: image Edit.
*         schema:
*           type: object
*           required:
*             - _id
*           properties:
*             _id:
*               type: string
*             image:
*               type: array
*               items:
*                type: string 
*             imageName:
*               type: array
*               items:
*                type: string 
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/uploadImageInProjectBase64', fetchuser, projectController.uploadImageInProjectBase64)
/**
* @swagger
* /api/v1/project/addDescriptionOnProject/{_id}:
*   post:
*     tags:
*       - PROJECT
*     description: addDescriptionOnProject
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true  
*       - name: description
*         description: description
*         in: formData
*         required: true        
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.post('/addDescriptionOnProject/:_id', fetchuser, projectController.addDescriptionOnProject)
/**
* @swagger
* /api/v1/project/addDescriptionOnImage/{_id}:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true  
*       - name: imageId
*         description: imageId
*         in: formData
*         required: true  
*       - name: description
*         description: description
*         in: formData
*         required: true        
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.post('/addDescriptionOnImage/:_id', fetchuser, projectController.addDescriptionOnImage)
/**
* @swagger
* /api/v1/project/commentOnProject/{_id}:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true   
*       - name: Comment
*         description: Comment
*         in: formData
*         required: true        
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.post('/commentOnProject/:_id', fetchuser, projectController.commentOnProject)
/**
* @swagger
* /api/v1/project/commentOnImage/{_id}:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true  
*       - name: imageId
*         description: imageId
*         in: formData
*         required: true  
*       - name: Comment
*         description: Comment
*         in: formData
*         required: true        
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.post('/commentOnImage/:_id', fetchuser, projectController.commentOnImage)
/**
* @swagger
* /api/v1/project/getProjectsList:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true   
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getProjectsList", fetchuser, projectController.getProjectsList);
/**
* @swagger
* /api/v1/project/getProjectsListwithPagination:
*   get:
*     tags:
*       - USER_PLAN_MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: page
*         description: page
*         in: query
*         required: false
*       - name: limit
*         description: limit
*         in: query
*         required: false
*     responses:
*       200:
*         description: Requested data found
*       404:
*         description: Requested data not found
*       500:
*         description: Internal Server Error
*/
router.get("/getProjectsListwithPagination", fetchuser, projectController.getProjectsListwithPagination);
/**
* @swagger
* /api/v1/project/getCompleteProjectsListwithPagination:
*   get:
*     tags:
*       - USER_PLAN_MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: page
*         description: page
*         in: query
*         required: false
*       - name: limit
*         description: limit
*         in: query
*         required: false
*     responses:
*       200:
*         description: Requested data found
*       404:
*         description: Requested data not found
*       500:
*         description: Internal Server Error
*/
router.get("/getCompleteProjectsListwithPagination", fetchuser, projectController.getCompleteProjectsListwithPagination);
/**
* @swagger
* /api/v1/project/getIncompleteProjectsListwithPagination:
*   get:
*     tags:
*       - USER_PLAN_MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: page
*         description: page
*         in: query
*         required: false
*       - name: limit
*         description: limit
*         in: query
*         required: false
*     responses:
*       200:
*         description: Requested data found
*       404:
*         description: Requested data not found
*       500:
*         description: Internal Server Error
*/
router.get("/getIncompleteProjectsListwithPagination", fetchuser, projectController.getIncompleteProjectsListwithPagination);
/**
* @swagger
* /api/v1/project/getNewProjectsListwithPagination:
*   get:
*     tags:
*       - USER_PLAN_MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: page
*         description: page
*         in: query
*         required: false
*       - name: limit
*         description: limit
*         in: query
*         required: false
*     responses:
*       200:
*         description: Requested data found
*       404:
*         description: Requested data not found
*       500:
*         description: Internal Server Error
*/
router.get("/getNewProjectsListwithPagination", fetchuser, projectController.getNewProjectsListwithPagination);
/**
* @swagger
* /api/v1/project/getproject/{id}:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: id
*         description: id
*         in: path
*         required: true     
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getproject/:id", projectController.getproject);
/**
* @swagger
* /api/v1/project/updateproject/{id}:
*   put:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: id
*         description: id
*         in: path
*         required: true
*       - name: projectname
*         description: projectname
*         in: formData
*         required: false
*       - name: address1
*         description: address1
*         in: formData
*         required: false
*       - name: address2
*         description: address2
*         in: formData
*         required: false
*       - name: city
*         description: city
*         in: formData
*         required: false
*       - name: pcode
*         description: pcode
*         in: formData
*         required: false
*       - name: state
*         description: state
*         in: formData
*         required: false
*     responses:
*       200:
*         description: Requested data found.
*       404: 
*         description: Requested data not found.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*       405:
*         description: You are not allowed to access the YNOT-panel./You don't have permission to access this API.
*/
router.put("/updateproject/:id", fetchuser, projectController.editProject);
/**
* @swagger
* /api/v1/project/deleteproject/{id}:
*   delete:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: id
*         description: id
*         in: path
*         required: true     
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.delete("/deleteproject/:id", fetchuser, projectController.deleteProject);
/**
* @swagger
* /api/v1/project/getImages:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true   
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getImages", fetchuser, projectController.getImages);
/**
* @swagger
* /api/v1/project/sendmessage/{id}:
*   post:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: id
*         description: id
*         in: path
*         required: true  
*       - name: message
*         description: message
*         in: formData
*         required: true      
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.post("/sendmessage/:id", fetchuser, projectController.sendmessage);
/**
* @swagger
* /api/v1/project/getmessagefromuser/{id}:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: id
*         description: id
*         in: path
*         required: true     
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getmessagefromuser/:id", fetchuser, projectController.getmessagefromuser);
/** 
* @swagger
* /api/v1/project/packagelist:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true   
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/packagelist", fetchuser, projectController.packagelist);
/**
* @swagger
* /api/v1/project/uploadeditedImage/{id}:
*   put:
*     tags:
*       - ASSIGN PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: id
*         description: id
*         in: path
*         required: true  
*       - name: files
*         description: files
*         in: formData
*         type: file
*         required: true
*     responses:
*       200:
*         description: Requested data found.
*       404: 
*         description: Requested data not found.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*       405:
*         description: You are not allowed to access the YNOT-panel./You don't have permission to access this API.
*/
router.put("/uploadeditedImage/:id", fetchuser, upload.array("files"), projectController.uploadeditedImage);
/** 
* @swagger
* /api/v1/project/getEditedImagesOnUserside/{id}:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: id
*         description: projectId
*         in: path
*         required: true     
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getEditedImagesOnUserside/:id", fetchuser, projectController.getEditedImagesOnUserside);
/**
* @swagger
* /api/v1/project/getCompleteProjectsList:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true    
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getCompleteProjectsList", fetchuser, projectController.getCompleteProjectsList);
/**
* @swagger
* /api/v1/project/getIncompleteProjectsList:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true    
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getIncompleteProjectsList", fetchuser, projectController.getIncompleteProjectsList);
/**
* @swagger
* /api/v1/project/getNewProjectsList:
*   get:
*     tags:
*       - PROJECT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true    
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getNewProjectsList", fetchuser, projectController.getNewProjectsList);
/** 
* @swagger
* /api/v1/project/storagePakageslist:
*   get:
*     tags:
*       - STORAGE PACKAGE
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/storagePakageslist", projectController.storagePakageslist);
/**
* @swagger
* /api/v1/project/viewStoragePakage/{id}:
*   get:
*     tags:
*       - STORAGE PACKAGE
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: id
*         description: id
*         in: path
*         required: true     
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/viewStoragePakage/:id", projectController.viewStoragePakage);
/**
* @swagger
* /api/v1/project/userStoragePakages:
*   get:
*     tags:
*       - STORAGE PACKAGE
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true    
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/userStoragePakages", fetchuser, projectController.userStoragePakages);
/**
* @swagger
* /api/v1/project/uploadImage:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:  
*       - name: image
*         description: image
*         in: formData
*         type: Array
*         required: true        
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.post('/uploadImage', projectController.uploadImage)
/**
* @swagger
* /api/v1/project/sendSelectedImage:
*   post:
*     tags:
*       - STORAGE PACKAGE
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true 
*       - in: body
*         name: sendSelectedImage
*         description: sendSelectedImage Edit.
*         schema:
*           type: object
*           required:
*             - id
*             - selectType
*           properties:
*             id:
*               type: string
*             selectType:
*               type: string
*             selectedImage:
*               type: array
*               items:
*                type: string       
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.post("/sendSelectedImage", fetchuser, projectController.sendSelectedImage);
/**
* @swagger
* /api/v1/project/deleteCommentOnImage/{_id}:
*   delete:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true  
*       - name: imageId
*         description: imageId
*         in: formData
*         required: true  
*       - name: CommentId
*         description: CommentId
*         in: formData
*         required: true        
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.delete('/deleteCommentOnImage/:_id', fetchuser, projectController.deleteCommentOnImage)
/**
* @swagger
* /api/v1/project/deleteParticularImage/{_id}:
*   delete:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true 
*       - name: imageid
*         description: imageid
*         in: query
*         required: true 
*       - name: imageId
*         description: imageId
*         in: query
*         required: true        
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.delete('/deleteParticularImage/:_id', fetchuser, projectController.deleteParticularImage)
/**
* @swagger
* /api/v1/project/addoriginalDriveLink/{_id}:
*   put:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true
*       - name: linkTitle
*         description: linkTitle
*         in: formData
*         required: true  
*       - name: link
*         description: link
*         in: formData
*         required: true  
*       - name: totalImage
*         description: totalImage
*         in: formData
*         required: true     
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.put('/addoriginalDriveLink/:_id', fetchuser, projectController.addoriginalDriveLink);
/**
* @swagger
* /api/v1/project/deleteOriginallink/{_id}:
*   delete:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true 
*       - name: linkId
*         description: linkId
*         in: query
*         required: true         
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.delete('/deleteOriginallink/:_id', fetchuser, projectController.deleteOriginallink)
/**
* @swagger
* /api/v1/project/deleteOriginallink1/{_id}:
*   delete:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: _id
*         description: _id
*         in: path
*         required: true        
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.delete('/deleteOriginallink1/:_id', projectController.deleteOriginallink1)
/**
 * @swagger
 * /api/v1/project/totalImage:
 *   get:
 *     tags:
 *       - PROJECT
 *     description: uploadFile
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true  
 *     responses:
 *       200:
 *         description: Upload successful.   
 *       401:
 *         description: Invalid file format
 */
router.get('/totalImage', fetchuser, projectController.totalImage);
/**
* @swagger
* /api/v1/project/addProjectType/{_id}:
*   put:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true   
*       - name: projectType
*         description: projectType ---> IMAGE || LINK
*         in: formData
*         required: true  
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.put('/addProjectType/:_id', fetchuser, projectController.addProjectType);

/**
* @swagger
* /api/v1/project/downloadAllImage/{_id}:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: _id
*         description: _id
*         in: path
*         required: true   
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/downloadAllImage/:_id', projectController.downloadAllImage);
/**
* @swagger
* /api/v1/project/downloadOrderImage/{_id}:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: _id
*         description: _id
*         in: path
*         required: true   
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/downloadOrderImage/:_id', projectController.downloadOrderImage);
/**
* @swagger
* /api/v1/project/downloadEditImage/{_id}:
*   post:
*     tags:
*       - PROJECT
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: _id
*         description: _id
*         in: path
*         required: true   
*     responses:
*       200:
*         description: Upload successful.
*       401:
*         description: Invalid file format
*/
router.post('/downloadEditImage/:_id', projectController.downloadEditImage);

module.exports = router;

