const router = require('express').Router();
const paypalController = require('../../controllers/paypalController');
const fetchuser = require("../../middleware/fetchuser");

/**
 * @swagger
 * /api/v1/paypal/addTocart:
 *  post:
 *    tags:
 *       - PAYPAL
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - in: body
 *         name: selectedImage
 *         description: image Edit.
 *         schema:
 *           type: object
 *           required:
 *             - projectId
 *             - packageId
 *             - selectedImage
 *           properties:
 *             projectId:
 *               type: string
 *             packageId:
 *               type: string
 *             selectedImage:
 *               type: array
 *               items:
 *                type: string 
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.post('/addTocart', fetchuser, paypalController.addTocart)
/**
 * @swagger
 * /api/v1/paypal/linkaddTocart:
 *  post:
 *    tags:
 *       - PAYPAL
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - in: body
 *         name: selectedImage
 *         description: image Edit.
 *         schema:
 *           type: object
 *           required:
 *             - projectId
 *             - packageId
 *             - linkId
 *           properties:
 *             projectId:
 *               type: string
 *             packageId:
 *               type: string
 *             linkId:
 *               type: string
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.post('/linkaddTocart', fetchuser, paypalController.linkaddTocart)
/**
* @swagger
* /api/v1/paypal/getCart:
*  get:
*    tags:
*       - PAYPAL
*    produces:
*      - application/json
*    parameters:
*       - name: token
*         description: token
*         in: header
*         required: true 
*    responses:
*       200:
*         description: Thanks, You have successfully signed up.
*       404:
*         description: This mobile number already exists.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!   
*/
router.get('/getCart', fetchuser, paypalController.getCart);

/**
* @swagger
* /api/v1/paypal/deleteCart/{_id}:
*   delete:
*     tags:
*       - PAYPAL
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true     
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.delete("/deleteCart/:_id", fetchuser, paypalController.deleteCart);
/**
 * @swagger
 * /api/v1/paypal/pay:
 *  post:
 *    tags:
 *       - PAYPAL
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.post('/pay', fetchuser, paypalController.pay)
router.get('/success', paypalController.success)
router.get('/cancel', paypalController.cancel)
/**
 * @swagger
 * /api/v1/paypal/transactionList:
 *  get:
 *    tags:
 *       - PAYPAL
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.get('/transactionList', fetchuser, paypalController.transactionList)

/**
 * @swagger
 * /api/v1/paypal/getTransaction/{id}:
 *  get:
 *    tags:
 *       - PAYPAL
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - name: id
 *         description: _id is required.
 *         in: path
 *         required: true
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.get('/getTransaction/:id', fetchuser, paypalController.getTransaction)
/**
 * @swagger
 * /api/v1/paypal/buyStoragePakage:
 *  post:
 *    tags:
 *       - PAYPAL
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - name: storageId
 *         description: storageId
 *         in: formData
 *         required: true
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.post('/buyStoragePakage', fetchuser, paypalController.buyStoragePakage)
router.get('/success1', paypalController.success1)
router.get('/cancel1', paypalController.cancel1)
module.exports = router;
