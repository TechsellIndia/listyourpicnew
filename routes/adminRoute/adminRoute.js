const router = require('express').Router();
const adminController = require('../../controllers/adminController');
const bannerController = require('../../controllers/bannerController');
const auth = require('../../middleware/auth');
const fetchuser = require("../../middleware/fetchuser");
// const { upload } = require("../../helper/filehelper");
var multer = require('multer');
var upload = multer({ dest: 'uploads/' });


/**
* @swagger
* /api/v1/admin/uploadPhoto:
*   post:
*     tags:
*       - ADMIN
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: image
*         description: image ?? base64
*         in: formData
*         type: file
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.post('/uploadPhoto', upload.single('image'), adminController.uploadPhoto);
/**
 * @swagger
 * /api/v1/admin/adminLogin:
 *   post:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email
 *         in: formData
 *         required: true
 *       - name: password
 *         description: password
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.post('/adminLogin', adminController.adminLogin)
/**
 * @swagger
 * /api/v1/admin/getAdmin_companyProfile:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get('/getAdmin_companyProfile', fetchuser, adminController.getAdmin_companyProfile)
/**
* @swagger
* /api/v1/admin/editProfile:
*   put:
*     tags:
*       - ADMIN
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: email
*         description: email
*         in: formData
*         required: false
*       - name: name
*         description: name
*         in: formData
*         required: false
*       - name: mobileNumber
*         description: mobileNumber
*         in: formData
*         required: false
*       - name: address
*         description: address
*         in: formData
*         required: false
*       - name: image
*         description: image ?? base64
*         in: formData
*         type: file
*         required: false
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.put('/editProfile', fetchuser, upload.single('image'), adminController.editProfile);
/**
* @swagger
* /api/v1/admin/editemployeeManagerProfile:
*   put:
*     tags:
*       - ADMIN
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - in: body
*         name: edit 
*         description: DOCS FOR ADMIN.
*         schema:
*           type: object
*           properties:
*             _id:
*              type: string
*             email:
*              type: string
*             password:
*              type: string
*             name:
*              type: string
*             image:
*              type: string
*             mobileNumber:
*              type: string
*             address:
*              type: string
*             userType:
*              type: string
*             permissions:
*              type: object
*              properties:
*                addEmployee:
*                 type: boolean
*                 default: false
*                viewEmployee:
*                 type: boolean
*                 default: false
*                EditEmployee:
*                 type: boolean
*                 default: false
*                deleteEmployee:
*                 type: boolean
*                 default: false
*                viewManager:
*                 type: boolean
*                 default: false
*                viewProject:
*                 type: boolean
*                 default: false
*                EditProject:
*                 type: boolean
*                 default: false
*                viewUser:
*                 type: boolean
*                 default: false
*                EditUser:
*                 type: boolean
*                 default: false
*                deleteUser:
*                 type: boolean
*                 default: false
*                bannerManagement:
*                 type: boolean
*                 default: false
*                blogManagement:
*                 type: boolean
*                 default: false
*                projectManagement:
*                 type: boolean
*                 default: false
*                staticContentManagement:
*                 type: boolean
*                 default: false
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.put('/editemployeeManagerProfile', fetchuser, adminController.editemployeeManagerProfile);
/**
 * @swagger
 * /api/v1/admin/verifyOTP:
 *   post:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email
 *         in: formData
 *         required: true
 *       - name: otp
 *         description: otp
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Your OTP verify is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.post('/verifyOTP', adminController.verifyOTP)
/**
 * @swagger
 * /api/v1/admin/resetPassword:
 *   post:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email
 *         in: formData
 *         required: true
 *       - name: newPassword
 *         description: newPassword
 *         in: formData
 *         required: true
 *       - name: confirmPassword
 *         description: confirmPassword
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Your password has been successfully changed.
 *       404:
 *         description: This user does not exist.
 *       422:
 *         description: Password not matched.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */
router.post('/resetPassword', adminController.resetPassword)
/**
  * @swagger
  * /api/v1/admin/forgotPassword:
  *   post:
  *     tags:
  *       - ADMIN
  *     description: Check for Social existence and give the access Token
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: email
  *         description: email
  *         in: formData
  *         required: true
  *     responses:
  *       200:
  *         description: OTP has been sent to your registered mobile number.
  *       404:
  *         description: Provided mobile number is not registered.
  *       500:
  *         description: Internal Server Error
  *       501:
  *         description: Something went wrong!
  */
router.post('/forgotPassword', adminController.forgotPassword)
/**
 * @swagger
 * /api/v1/admin/changePassword:
 *   put:
 *     tags:
 *       - ADMIN
 *     description: changePassword
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: oldPassword
 *         description: oldPassword
 *         in: formData
 *         required: true
 *       - name: newPassword
 *         description: newPassword
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Returns success message
 */
router.put('/changePassword', fetchuser, adminController.changePassword)
// /**
//  * @swagger
//  * /api/v1/admin/create_employee:
//  *   post:
//  *     tags:
//  *       - ADMIN
//  *     description: Add Employee by ADMIN/Manager
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: token
//  *         description: ADMIN / Manager token
//  *         in: header
//  *         required: true
//  *       - in: body
//  *         name: Add Manager
//  *         description: DOCS FOR ADMIN.
//  *         schema:
//  *           type: object
//  *           properties:
//  *             email:
//  *              type: string
//  *             password:
//  *              type: string
//  *             name:
//  *              type: string
//  *             permissions:
//  *              type: object
//  *              properties:
//  *                viewEmployee:
//  *                 type: boolean
//  *                 default: false
//  *                viewManager:
//  *                 type: boolean
//  *                 default: false
//  *                viewProject:
//  *                 type: boolean
//  *                 default: false
//  *                EditProject:
//  *                 type: boolean
//  *                 default: false
//  *                viewUser:
//  *                 type: boolean
//  *                 default: false
//  *                EditUser:
//  *                 type: boolean
//  *                 default: false
//  *                deleteUser:
//  *                 type: boolean
//  *                 default: false
//  *     responses:
//  *       200:
//  *         description: Sub Admin successfully added
//  *       404:
//  *         description: Invalid credentials
//  *       500:
//  *         description: Internal Server Error
//  */
router.post('/create_employee', fetchuser, adminController.create_employee)
/**
 * @swagger
 * /api/v1/admin/create_package:
 *   post:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: packageName
 *         description: packageName
 *         in: formData
 *         required: true
 *       - name: amount
 *         description: amount
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: validity
 *         description: validity
 *         in: formData
 *         required: true
 *         type: integer
 *       - name: imageQuantity
 *         description: imageQuantity
 *         in: formData
 *         type: integer
 *         required: true
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.post('/create_package', fetchuser, adminController.create_package);

/**
 * @swagger
 * /api/v1/admin/delete_package/{id}:
 *   delete:
 *     tags:
 *       - ADMIN 
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.delete("/delete_package/:id", fetchuser, adminController.delete_package);
/**
 * @swagger
 * /api/v1/admin/packagelist:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.get('/packagelist', fetchuser, adminController.packagelist)
/**
 * @swagger
 * /api/v1/admin/blogs:
 *   post:
 *     tags:
 *       - BLOG
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: title
 *         description: title
 *         in: formData
 *         required: true
 *       - name: image
 *         description: image ?? base64
 *         in: formData
 *         type: file
 *       - name: blogs
 *         description: blogs
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.post('/blogs', fetchuser, upload.single('image'), adminController.blogs);  //done
/**
 * @swagger
 * /api/v1/admin/editBlog/{slug}:
 *   put:
 *     tags:
 *       - BLOG
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: slug 
 *         description: slug 
 *         in: path
 *         required: false
 *       - name: title
 *         description: title
 *         in: formData
 *         required: false
 *       - name: image
 *         description: image ?? base64
 *         in: formData
 *         type: file
 *         required: false
 *       - name: blogs
 *         description: blogs
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.put('/editBlog/:slug', fetchuser, upload.single('image'), adminController.editBlog);
/**
 * @swagger
 * /api/v1/admin/deleteBlog/{id}:
 *   delete:
 *     tags:
 *       - BLOG 
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.delete("/deleteBlog/:id", fetchuser, adminController.deleteBlog);
/**
 * @swagger
 * /api/v1/admin/createManager:
 *   post:
 *     tags:
 *       - ADMIN
 *     description: Add Manager by ADMIN
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: ADMIN token
 *         in: header
 *         required: true
 *       - in: body
 *         name: Add Manager
 *         description: DOCS FOR ADMIN.
 *         schema:
 *           type: object
 *           properties:
 *             email:
 *              type: string
 *             password:
 *              type: string
 *             name:
 *              type: string
 *             image:
 *              type: string
 *             userType:
 *              type: string
 *             mobileNumber:
 *              type: string
 *             address:
 *              type: string
 *             permissions:
 *              type: object
 *              properties:
 *                addEmployee:
 *                 type: boolean
 *                 default: false
 *                viewEmployee:
 *                 type: boolean
 *                 default: false
 *                EditEmployee:
 *                 type: boolean
 *                 default: false
 *                deleteEmployee:
 *                 type: boolean
 *                 default: false
 *                viewManager:
 *                 type: boolean
 *                 default: false
 *                viewProject:
 *                 type: boolean
 *                 default: false
 *                EditProject:
 *                 type: boolean
 *                 default: false
 *                viewUser:
 *                 type: boolean
 *                 default: false
 *                EditUser:
 *                 type: boolean
 *                 default: false
 *                deleteUser:
 *                 type: boolean
 *                 default: false
 *                bannerManagement:
 *                 type: boolean
 *                 default: false
 *                blogManagement:
 *                 type: boolean
 *                 default: false
 *                projectManagement:
 *                 type: boolean
 *                 default: false
 *                staticContentManagement:
 *                 type: boolean
 *                 default: false
 *     responses:
 *       200:
 *         description: Sub Admin successfully added
 *       404:
 *         description: Invalid credentials
 *       500:
 *         description: Internal Server Error
 */
router.post('/createManager', fetchuser, adminController.createManager)
/**
 * @swagger
 * /api/v1/admin/getallemployee:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get('/getallemployee', fetchuser, adminController.getallemployee)
/**
 * @swagger
 * /api/v1/admin/getallManager:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get('/getallManager', fetchuser, adminController.getallManager)
/**
 * @swagger
 * /api/v1/admin/getparticularEmployee/{id}:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getparticularEmployee/:id", fetchuser, adminController.getparticularEmployee);
/**
 * @swagger
 * /api/v1/admin/getparticularManage/{id}:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getparticularManage/:id", fetchuser, adminController.getparticularManage);
/**
 * @swagger
 * /api/v1/admin/deleteEmp_manager/{id}:
 *   delete:
 *     tags:
 *       - ADMIN 
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.delete("/deleteEmp_manager/:id", fetchuser, adminController.deleteEmp_manager);
/**
 * @swagger
 * /api/v1/admin/getalluser:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get('/getalluser', fetchuser, adminController.getalluser)
/**
 * @swagger
 * /api/v1/admin/getparticularUser/{id}:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getparticularUser/:id", fetchuser, adminController.getparticularUser);
/**
 * @swagger
 * /api/v1/admin/deleteUser/{id}:
 *   delete:
 *     tags:
 *       - ADMIN 
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.delete("/deleteUser/:id", fetchuser, adminController.deleteUser);
/**
 * @swagger
 * /api/v1/admin/getProjectsList:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getProjectsList", fetchuser, adminController.getProjectsList);
/**
 * @swagger
 * /api/v1/admin/getproject/{id}:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getproject/:id", fetchuser, adminController.getproject);

/**
 * @swagger
 * /api/v1/admin/assignproject:
 *   post:
 *     tags:
 *       - ASSIGN PROJECT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - name: id
 *         description: id
 *         in: formData
 *         required: true   
 *       - name: employeeId
 *         description: employeeId
 *         in: formData
 *         required: true 
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.post("/assignproject", fetchuser, adminController.assignproject);
/**
 * @swagger
 * /api/v1/admin/removeAssignUser/{id}:
 *   delete:
 *     tags:
 *       - ASSIGN PROJECT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true    
 *       - name: transactionId
 *         description: _id --> transactionId
 *         in: formData
 *         required: true    
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.delete("/removeAssignUser/:id", fetchuser, adminController.removeAssignUser);
/**
 * @swagger
 * /api/v1/admin/getprojectlistofEmployee:
 *   get:
 *     tags:
 *       - ASSIGN PROJECT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true   
 *       - name: employeeId
 *         description: employeeId
 *         in: formData
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getprojectlistofEmployee", fetchuser, adminController.getprojectlistofEmployee);
// /**
// * @swagger
// * /api/v1/admin/getAssignprojectlistofEmployee:
// *   get:
// *     tags:
// *       - ADMIN
// *     description: Check for Social existence and give the access Token 
// *     produces:
// *       - application/json
// *     parameters:
// *       - name: token
// *         description: token
// *         in: header
// *         required: true     
// *     responses:
// *       200:
// *         description: Details have been fetched successfully.
// *       404: 
// *         description: This user does not exist.
// *       500:
// *         description: Internal Server Error.
// *       501:
// *         description: Something went wrong!
// *       401:
// *         description: Invalid JWT token.
// *       403:
// *         description: You are not authorized, please contact Admin.
// */
// router.get("/getAssignprojectlistofEmployee", fetchuser, adminController.getAssignprojectlistofEmployee);
/** 
 * @swagger
 * /api/v1/admin/updateStatus:
 *   put:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: projectId
 *         in: formData
 *         required: true
 *       - name: status
 *         description: status
 *         in: formData
 *         required: true        
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.put("/updateStatus", fetchuser, adminController.updateStatus);
/** 
 * @swagger
 * /api/v1/admin/transactionStatusUpdate:
 *   put:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: transactionId
 *         in: formData
 *         required: true
 *       - name: status
 *         description: status --> PENDING || INPROCESS || COMPLETE
 *         in: formData
 *         required: true        
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.put("/transactionStatusUpdate", fetchuser, adminController.transactionStatusUpdate);
/**
 * @swagger
 * /api/v1/admin/getCompleteProjectsList:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getCompleteProjectsList", fetchuser, adminController.getCompleteProjectsList);
/**
 * @swagger
 * /api/v1/admin/getIncompleteProjectsList:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true  
 *       - name: userId
 *         description: userId
 *         in: formData
 *         required: true    
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getIncompleteProjectsList", fetchuser, adminController.getIncompleteProjectsList);
/**
 * @swagger
 * /api/v1/admin/getNewProjectsList:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true  
 *       - name: userId
 *         description: userId
 *         in: formData
 *         required: true    
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getNewProjectsList", fetchuser, adminController.getNewProjectsList);
/**
 * @swagger
 * /api/v1/admin/blogList:
 *   get:
 *     tags:
 *       - BLOG
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/blogList", fetchuser, adminController.blogList);
/**
 * @swagger
 * /api/v1/admin/getBlog/{slug}:
 *   get:
 *     tags:
 *       - BLOG
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: slug
 *         description: slug
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getBlog/:slug", fetchuser, adminController.getBlog);

/**
 * @swagger
 * /api/v1/admin/addBannerCategory:
 *   post:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: categoryName
 *         description: categoryName
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.post('/addBannerCategory', fetchuser, bannerController.addBannerCategory);
/**
 * @swagger
 * /api/v1/admin/bannerCategoryList:
 *   get:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/bannerCategoryList", bannerController.bannerCategoryList);
/**
 * @swagger
 * /api/v1/admin/getbannerCategory/{id}:
 *   get:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getbannerCategory/:id", bannerController.getbannerCategory);
/**
 * @swagger
 * /api/v1/admin/editbannerCategory/{id}:
 *   put:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id 
 *         description: id 
 *         in: path
 *         required: true
 *       - name: categoryName
 *         description: categoryName
 *         in: formData
 *         required: false
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.put('/editbannerCategory/:id', fetchuser, bannerController.editbannerCategory);
/**
 * @swagger
 * /api/v1/admin/deletebannerCategoryr/{id}:
 *   delete:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.delete("/deletebannerCategoryr/:id", fetchuser, bannerController.deletebannerCategoryr);
/**
 * @swagger
 * /api/v1/admin/addBanner:
 *   post:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: bannerName
 *         description: bannerName
 *         in: formData
 *         required: true
 *       - name: bannerCategory
 *         description: bannerCategory 
 *         in: formData
 *         required: true
 *       - name: bannerImage
 *         description: bannerImage ?? base64
 *         in: formData
 *         type: file
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.post('/addBanner', fetchuser, upload.single('bannerImage'), bannerController.addBanner);
/**
 * @swagger
 * /api/v1/admin/getBanner/{id}:
 *   get:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getBanner/:id", bannerController.getBanner);
/**
 * @swagger
 * /api/v1/admin/bannerList/{bannerCategory}:
 *   get:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json 
 *     parameters:
 *       - name: bannerCategory
 *         description: bannerCategory
 *         in: path
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/bannerList/:bannerCategory", bannerController.bannerList);
/**
 * @swagger
 * /api/v1/admin/editBanner/{id}:
 *   put:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id 
 *         description: id 
 *         in: path
 *         required: true
 *       - name: bannerName
 *         description: bannerName
 *         in: formData
 *         required: false
 *       - name: bannerCategory
 *         description: bannerCategory
 *         in: formData
 *         required: false
 *       - name: bannerImage
 *         description: bannerImage ?? base64
 *         in: formData
 *         type: file
 *         required: false
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.put('/editBanner/:id', fetchuser, upload.single('bannerImage'), bannerController.editBanner);
/**
 * @swagger
 * /api/v1/admin/deleteBanner/{id}:
 *   delete:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true     
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.delete("/deleteBanner/:id", fetchuser, bannerController.deleteBanner);
/**
 * @swagger
 * /api/v1/admin/upper_bannerList:
 *   get:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json    
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
// // router.get("/upper_bannerList", bannerController.upper_bannerList);
/**
 * @swagger
 * /api/v1/admin/lowwer_bannerList:
 *   get:
 *     tags:
 *       - BANNER MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json    
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
// router.get("/lowwer_bannerList", bannerController.lowwer_bannerList);
/**
 * @swagger
 * /api/v1/admin/addTrusted_client:
 *   post:
 *     tags:
 *       - TRUSTED CLIENT MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: clientName
 *         description: clientName
 *         in: formData
 *         required: true
 *       - name: image
 *         description: image ?? base64
 *         in: formData
 *         type: file
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.post('/addTrusted_client', fetchuser, upload.single('image'), bannerController.addTrusted_client);

/**
 * @swagger
 * /api/v1/admin/trustedClientList:
 *   get:
 *     tags:
 *       - TRUSTED CLIENT MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json    
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/trustedClientList", bannerController.trustedClientList);

/**
* @swagger
* /api/v1/admin/notificationList:
*   get:
*     tags:
*       - NOTIFICATION MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/notificationList", fetchuser, adminController.notificationListforAdmin);
/**
* @swagger
* /api/v1/admin/notificationStatus:
*   get:
*     tags:
*       - NOTIFICATION MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/notificationStatus", fetchuser, adminController.notificationStatus);
// /**
//  * @swagger
//  * /api/v1/admin/notificationListforAdmin:
//  *   get:
//  *     tags:
//  *       - NOTIFICATION MANAGEMENT
//  *     description: Check for Social existence and give the access Token 
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: token
//  *         description: token
//  *         in: header
//  *         required: true
//  *     responses:
//  *       200:
//  *         description: Details have been fetched successfully.
//  *       404: 
//  *         description: This user does not exist.
//  *       500:
//  *         description: Internal Server Error.
//  *       501:
//  *         description: Something went wrong!
//  *       401:
//  *         description: Invalid JWT token.
//  *       403:
//  *         description: You are not authorized, please contact Admin.
//  */
// router.get("/notificationListforAdmin", fetchuser, adminController.notificationListforAdmin);
// /**
//  * @swagger
//  * /api/v1/admin/notificationListforAsignuser:
//  *   get:
//  *     tags:
//  *       - NOTIFICATION MANAGEMENT
//  *     description: Check for Social existence and give the access Token 
//  *     produces:
//  *       - application/json
//  *     parameters:
//  *       - name: token
//  *         description: token
//  *         in: header
//  *         required: true
//  *     responses:
//  *       200:
//  *         description: Details have been fetched successfully.
//  *       404: 
//  *         description: This user does not exist.
//  *       500:
//  *         description: Internal Server Error.
//  *       501:
//  *         description: Something went wrong!
//  *       401:
//  *         description: Invalid JWT token.
//  *       403:
//  *         description: You are not authorized, please contact Admin.
//  */
// router.get("/notificationListforAsignuser", fetchuser, adminController.notificationListforAsignuser);
/**
 * @swagger
 * /api/v1/admin/getProjectsListForemployee:
 *   get:
 *     tags:
 *       - ASSIGN PROJECT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getProjectsListForemployee", fetchuser, adminController.getProjectsListForemployee);

/**
 * @swagger
 * /api/v1/admin/replyOnComplaintbyAdmin/{id}:
 *   post:
 *     tags:
 *       - QUERY MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true    
 *       - name: comment
 *         description: comment
 *         in: formData
 *         required: true   
 *       - name: screenShot
 *         description: files
 *         in: formData
 *         type: file
 *         required: false
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.post("/replyOnComplaintbyAdmin/:id", fetchuser, upload.array("screenShot"), adminController.replyOnComplaintbyAdmin);

/**
* @swagger
* /api/v1/admin/ComplaintList:
*   get:
*     tags:
*       - QUERY MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/ComplaintList", fetchuser, adminController.ComplaintList);
/**
 * @swagger
 * /api/v1/admin/userComplaintList:
 *   get:
 *     tags:
 *       - QUERY MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: userId
 *         description: userId
 *         in: query
 *         required: true
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/userComplaintList", fetchuser, adminController.userComplaintList);
/**
 * @swagger
 * /api/v1/admin/closeComplaintfromAdminSide:
 *   post:
 *     tags:
 *       - QUERY MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: _id
 *         description: _id
 *         in: formData
 *         required: true    
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.post("/closeComplaintfromAdminSide", fetchuser, adminController.closeComplaintfromAdminSide);
/**
 * @swagger
 * /api/v1/admin/commentOnImageAdminSide/{_id}:
 *   post:
 *     tags:
 *       - ADMIN
 *     description: uploadFile
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: _id
 *         description: _id
 *         in: path
 *         required: true  
 *       - name: imageId
 *         description: imageId
 *         in: formData
 *         required: true  
 *       - name: Comment
 *         description: Comment
 *         in: formData
 *         required: true        
 *     responses:
 *       200:
 *         description: Upload successful.   
 *       401:
 *         description: Invalid file format
 */
router.post('/commentOnImageAdminSide/:_id', fetchuser, adminController.commentOnImageAdminSide)
/**
 * @swagger
 * /api/v1/admin/dashboard:
 *   post:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.post("/dashboard", fetchuser, adminController.dashboard);

/**
 * @swagger
 * /api/v1/admin/transactionList:
 *  get:
 *    tags:
 *       - ADMIN
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.get('/transactionList', fetchuser, adminController.transactionList)
/**
 * @swagger
 * /api/v1/admin/getTransaction/{id}:
 *  get:
 *    tags:
 *       - ADMIN
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - name: id
 *         description: _id is required.
 *         in: path
 *         required: true
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.get('/getTransaction/:id', fetchuser, adminController.getTransaction);

/**
 * @swagger
 * /api/v1/admin/getEmployeeProjectCount:
 *   get:
 *     tags:
 *       - ADMIN
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true 
 *       - name: employeeId
 *         description: employeeId
 *         in: query
 *         required: true 
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get("/getEmployeeProjectCount", fetchuser, adminController.getEmployeeProjectCount);/**
* @swagger
* /api/v1/admin/addEditDriveLink/{_id}:
*   put:
*     tags:
*       - ADMIN
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true 
*       - name: linkTitle
*         description: linkTitle
*         in: formData
*         required: true  
*       - name: link
*         description: link
*         in: formData
*         required: true    
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.put('/addEditDriveLink/:_id', fetchuser, adminController.addEditDriveLink);
/**
* @swagger
* /api/v1/admin/deleteEditDriveLink/{_id}:
*   delete:
*     tags:
*       - ADMIN
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: path
*         required: true 
*       - name: linkId
*         description: linkId
*         in: query
*         required: true         
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.delete('/deleteEditDriveLink/:_id', fetchuser, adminController.deleteEditDriveLink);
/**
* @swagger
* /api/v1/admin/broadcastNotification:
*   post:
*     tags:
*       - ADMIN
*     description: uploadFile
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: title
*         description: title
*         in: formData
*         required: true 
*       - name: content
*         description: content
*         in: formData
*         required: true            
*     responses:
*       200:
*         description: Upload successful.   
*       401:
*         description: Invalid file format
*/
router.post('/broadcastNotification', fetchuser, adminController.broadcastNotification);
/**
* @swagger
* /api/v1/admin/broadcastListforAdmin:
*   get:
*     tags:
*       - NOTIFICATION MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/broadcastListforAdmin", fetchuser, adminController.broadcastListforAdmin);

/**
 * @swagger
 * /api/v1/admin/createOrder/{_id}:
 *   post:
 *     tags:
 *       - QUERY MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: _id
 *         description: _id
 *         in: path
 *         required: true    
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.post("/createOrder/:_id", fetchuser, adminController.createOrder);

module.exports = router;

