const router = require('express').Router();
// const auth = require('../../middleWare/auth')
const fetchuser = require("../../middleware/fetchuser");

const staticController = require('../../controllers/staticController');
const afterbeforeController = require('../../controllers/afterbeforeController');
var multer = require('multer');
var upload = multer({ dest: 'uploads/' });
var cpUpload = upload.fields([{ name: 'afterImage', maxCount: 1 }, { name: 'beforeImage', maxCount: 1 }])


/**
* @swagger
* /api/v1/static/addStatic:
*   post:
*     tags:
*       - STATIC MANAGEMENT
*     description: Creating Docs for update static content
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: Admin token is required.
*         in: header
*         required: true
*       - name: title
*         description: Title is required.
*         in: formData
*         required: false
*       - name: description
*         description: description is required.
*         in: formData
*         required: false
*     responses:
*       200:
*         description: Data updated successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.post('/addStatic', fetchuser, staticController.addStatic);
/**
* @swagger
* /api/v1/static/getstaticList:
*   get:
*     tags:
*       - STATIC MANAGEMENT
*     description: Creating Docs for get static content
*     produces:
*       - application/json
*     responses:
*       200:
*         description: Data fetch successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.get('/getstaticList', staticController.getstaticList);
/**
* @swagger
* /api/v1/static/getStaticContent:
*   get:
*     tags:
*       - STATIC MANAGEMENT
*     description: Creating Docs for get static content
*     produces:
*       - application/json
*     parameters:
*       - name: Type
*         description: T&C/PrivacyAndPolicy
*         in: query
*         required: true
*     responses:
*       200:
*         description: Data fetch successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.get('/getStaticContent', staticController.getStaticContent);

/**
* @swagger
* /api/v1/static/getStaticContentbytype/{Type}:
*   get:
*     tags:
*       - STATIC MANAGEMENT
*     description: Creating Docs for get static content
*     produces:
*       - application/json
*     parameters:
*       - name: Type
*         description: T&C/PrivacyAndPolicy
*         in: path
*         required: true
*     responses:
*       200:
*         description: Data fetch successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.get('/getStaticContentbytype/:Type', staticController.getStaticContentbytype);
/**
* @swagger
* /api/v1/static/updateStaticContent/{_id}:
*   put:
*     tags:
*       - STATIC MANAGEMENT
*     description: Creating Docs for update static content
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: Admin token is required.
*         in: header
*         required: true
*       - name: _id
*         description: Type is required.
*         in: path
*         required: true
*       - name: title
*         description: Title is required.
*         in: formData
*         required: false
*       - name: description
*         description: description is required.
*         in: formData
*         required: false
*     responses:
*       200:
*         description: Data updated successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.put('/updateStaticContent/:_id', fetchuser, staticController.updateStaticContent);


/**
* @swagger
* /api/v1/static/addAfterbeforeImage:
*   post:
*     tags:
*       - AFTER BEFORE IMAGE
*     description: Creating Docs for user signUp 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: name
*         description: name is required.
*         in: formData
*         required: false
*       - name: afterImage
*         description: afterImage is required.
*         in: formData
*         type: file
*         required: true
*       - name: beforeImage
*         description: beforeImage is required.
*         in: formData
*         type: file
*         required: true
*     responses:
*       200:
*         description: You have successfully sign up.
*       409:
*         description: Already exist.
*       500:
*         description: Internal server error.
*/
router.post('/addAfterbeforeImage', fetchuser, cpUpload, afterbeforeController.addAfterbeforeImage);

/**
* @swagger
* /api/v1/static/afterbeforeImagerList:
*   get:
*     tags:
*       - AFTER BEFORE IMAGE
*     description: Creating Docs for get static content
*     produces:
*       - application/json
*     responses:
*       200:
*         description: Data fetch successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.get('/afterbeforeImagerList', afterbeforeController.afterbeforeImagerList);
/**
* @swagger
* /api/v1/static/editAfterbeforeImage:
*   post:
*     tags:
*       - AFTER BEFORE IMAGE
*     description: Creating Docs for user signUp 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id is required.
*         in: formData
*         required: true
*       - name: name
*         description: name is required.
*         in: formData
*         required: false
*       - name: afterImage
*         description: afterImage is required.
*         in: formData
*         type: file
*         required: false
*       - name: beforeImage
*         description: beforeImage is required.
*         in: formData
*         type: file
*         required: false
*     responses:
*       200:
*         description: You have successfully sign up.
*       409:
*         description: Already exist.
*       500:
*         description: Internal server error.
*/
router.post('/editAfterbeforeImage', fetchuser, cpUpload, afterbeforeController.editAfterbeforeImage);

/**
* @swagger
* /api/v1/static/viewAfterbeforeImage:
*   get:
*     tags:
*       - AFTER BEFORE IMAGE
*     description: Creating Docs for get static content
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: query
*         required: true
*     responses:
*       200:
*         description: Data fetch successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.get('/viewAfterbeforeImage', fetchuser, afterbeforeController.viewAfterbeforeImage);

/**
* @swagger
* /api/v1/static/deleteAfterbeforeImage:
*   delete:
*     tags:
*       - AFTER BEFORE IMAGE
*     description: Creating Docs for get static content
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: _id
*         description: _id
*         in: query
*         required: true
*     responses:
*       200:
*         description: Data fetch successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.delete('/deleteAfterbeforeImage', fetchuser, afterbeforeController.deleteAfterbeforeImage);
/**
* @swagger
* /api/v1/static/afterbeforeImagerListforuser:
*   get:
*     tags:
*       - AFTER BEFORE IMAGE
*     description: Creating Docs for get static content
*     produces:
*       - application/json
*     responses:
*       200:
*         description: Data fetch successfully.
*       404:
*         description: NOT FOUND.
*       500:
*         description: Internal server error.
*/
router.get('/afterbeforeImagerListforuser', afterbeforeController.afterbeforeImagerListforuser);
/**
* @swagger
* /api/v1/static/videoUpload:
*   post:
*     tags:
*       - AFTER BEFORE IMAGE
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: video
*         description: Video ?? base64
*         in: formData
*         type: file
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.post('/videoUpload', upload.single('video'), afterbeforeController.videoUpload);
module.exports = router;
