const router = require('express').Router();
const user = require('./userRoute/userRoute');
const project = require('./projectRoute/projects');
const admin = require('./adminRoute/adminRoute');
const static = require('./adminRoute/staticRoute');
const paypal = require('./paypalRoute/paypalRoute');
router.use('/user', user)
router.use('/project', project )
router.use('/admin', admin )
router.use('/static', static )
router.use('/paypal', paypal )
module.exports = router;    