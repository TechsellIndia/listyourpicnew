const router = require('express').Router();
const userController = require('../../controllers/userController');
const auth = require('../../middleware/auth');
const bannerController = require('../../controllers/bannerController');
const fetchuser = require("../../middleware/fetchuser");
const { upload } = require("../../helper/filehelper");

/**
 * @swagger
 * /api/v1/user/createCompany:
 *  post:
 *    tags:
 *       - CONSUMER
 *    produces:
 *      - application/json
 *    parameters:
 *       - name: company
 *         description: company
 *         in: formData
 *         required: true
 *       - name: email
 *         description: email
 *         in: formData
 *         required: true
 *       - name: password
 *         description: password
 *         in: formData
 *         required: true
 *       - name: fname
 *         description: fname
 *         in: formData
 *         required: true
 *       - name: lname
 *         description: lname
 *         in: formData
 *         required: true
 *       - name: phone
 *         description: phone
 *         in: formData
 *         required: true
 *       - name: extraoption
 *         description: extraoption
 *         in: formData
 *         required: false
 *    responses:
 *       200:
 *         description: Thanks, You have successfully signed up.
 *       404:
 *         description: This mobile number already exists.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!   
 */
router.post('/createCompany', userController.createCompany)
/**
 * @swagger
 * /api/v1/user/verifyOTP:
 *   post:
 *     tags:
 *       - CONSUMER
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email
 *         in: formData
 *         required: true
 *       - name: otp
 *         description: otp
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Your OTP verify is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
 router.post('/verifyOTP', userController.verifyOTP)
/**
 * @swagger
 * /api/v1/user/login:
 *   post:
 *     tags:
 *       - CONSUMER
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email
 *         in: formData
 *         required: true
 *       - name: password
 *         description: password
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Your login is successful
 *       402:
 *         description: Invalid credentials
 *       404:
 *         description: Entered email/mobile number is not registered.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 *       400:
 *         description: Fields are required.
 */
router.post('/login', userController.login)

/**
 * @swagger
 * /api/v1/user/getProfile:
 *   get:
 *     tags:
 *       - CONSUMER
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true   
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.get('/getProfile', fetchuser, userController.getProfile)

/**
* @swagger
* /api/v1/user/editProfile:
*   put:
*     tags:
*       - CONSUMER
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: company
*         description: company
*         in: formData
*         required: false
*       - name: email
*         description: email
*         in: formData
*         required: false
*       - name: fname
*         description: fname
*         in: formData
*         required: false
*       - name: lname
*         description: lname
*         in: formData
*         required: false
*       - name: phone
*         description: phone
*         in: formData
*         required: false
*       - name: extraoption
*         description: extraoption
*         in: formData
*         required: false
*       - name: image
*         description: image ?? base64
*         in: formData
*         type: file
*         required: false
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.put('/editProfile', fetchuser, upload.single('image'), userController.editProfile);
/**
 * @swagger
 * /api/v1/user/resendOTP:
 *   post:
 *     tags:
 *       - CONSUMER
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email
 *         in: formData
 *         required: true 
 *     responses:
 *       200:
 *         description: otp sent.
 *       404: 
 *         description: user not found.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.post('/resendOTP', userController.resendOTP)

/**
 * @swagger
 * /api/v1/user/resetPassword:
 *   post:
 *     tags:
 *       - CONSUMER
 *     description: Check for Social existence and give the access Token
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: email
 *         description: email
 *         in: formData
 *         required: true
 *       - name: newPassword
 *         description: newPassword
 *         in: formData
 *         required: true
 *       - name: confirmPassword
 *         description: confirmPassword
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Your password has been successfully changed.
 *       404:
 *         description: This user does not exist.
 *       422:
 *         description: Password not matched.
 *       500:
 *         description: Internal Server Error
 *       501:
 *         description: Something went wrong!
 */
 router.post('/resetPassword', userController.resetPassword)
 /**
  * @swagger
  * /api/v1/user/forgotPassword:
  *   post:
  *     tags:
  *       - CONSUMER
  *     description: Check for Social existence and give the access Token
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: email
  *         description: email
  *         in: formData
  *         required: true
  *     responses:
  *       200:
  *         description: OTP has been sent to your registered mobile number.
  *       404:
  *         description: Provided mobile number is not registered.
  *       500:
  *         description: Internal Server Error
  *       501:
  *         description: Something went wrong!
  */
 router.post('/forgotPassword', userController.forgotPassword)



/**
 * @swagger
 * /api/v1/user/changePassword:
 *   put:
 *     tags:
 *       - CONSUMER
 *     description: changePassword
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: oldPassword
 *         description: oldPassword
 *         in: formData
 *         required: true
 *       - name: newPassword
 *         description: newPassword
 *         in: formData
 *         required: true
 *     responses:
 *       200:
 *         description: Returns success message
 */
router.put('/changePassword',fetchuser, userController.changePassword)
/**
* @swagger
* /api/v1/user/blogList:
*  get:
*    tags:
*      - BLOG
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json    
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.get("/blogList", userController.blogList);
/**
* @swagger
  * /api/v1/user/getBlog/{slug}:
  *   get:
  *     tags:
  *       - BLOG
  *     description: Check for Social existence and give the access Token 
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: slug
  *         description: slug
  *         in: path
  *         required: true     
  *     responses:
  *       200:
  *         description: Details have been fetched successfully.
  *       404: 
  *         description: This user does not exist.
  *       500:
  *         description: Internal Server Error.
  *       501:
  *         description: Something went wrong!
  *       401:
  *         description: Invalid JWT token.
  *       403:
  *         description: You are not authorized, please contact Admin.
  */
router.get("/getBlog/:slug", userController.getBlog);
/**
* @swagger
* /api/v1/user/createReport/{_id}:
*  post:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true
*      - name: _id
*        description: _id
*        in: path
*        required: true
*      - name: title
*        description: title
*        in: formData
*        required: true
*      - name: photoLayout
*        description: photoLayout ---> TWO || THREE || FOUR
*        in: formData
*        required: true
*    responses:
*      200:
*        description: Your login is successful
*      402:
*        description: Inval_id credentials
*      404:
*        description: Entered email/mobile number is not registered.
*      500:
*        description: Internal Server Error
*      501:
*        description: Something went wrong!
*      400:
*        description: Fields are required.
*/
router.post('/createReport/:_id', fetchuser, bannerController.createReport);

/**
* @swagger
* /api/v1/user/listReport:
*  get:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.get("/listReport",fetchuser, bannerController.listReport);
/**
* @swagger
* /api/v1/user/reportList/{projectId}:
*  get:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: projectId
*        description: projectId
*        in: path
*        required: true  
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.get("/reportList/:projectId", bannerController.reportList);
/**
* @swagger
* /api/v1/user/viewReport/{_id}:
*  post:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: _id
*        description: _id
*        in: path
*        required: true  
*      - name: projectId
*        description: projectId
*        in: formData
*        required: true  
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.post("/viewReport/:_id", bannerController.viewReport);
/**
* @swagger
* /api/v1/user/deleteReport/{_id}:
*  delete:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true
*      - name: _id
*        description: _id
*        in: path
*        required: true  
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
 router.delete("/deleteReport/:_id", bannerController.deleteReport);
/**
* @swagger
* /api/v1/user/createSectionOnReport/{_id}:
*  post:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true
*      - name: _id
*        description: _id
*        in: path
*        required: true
*    responses:
*      200:
*        description: Your login is successful
*      402:
*        description: Inval_id credentials
*      404:
*        description: Entered email/mobile number is not registered.
*      500:
*        description: Internal Server Error
*      501:
*        description: Something went wrong!
*      400:
*        description: Fields are required.
*/
router.post('/createSectionOnReport/:_id', fetchuser, bannerController.createSectionOnReport);
/**
* @swagger
* /api/v1/user/editSectionOnReport/{_id}:
*  put:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true
*      - name: _id
*        description: _id
*        in: path
*        required: true
*      - name: title
*        description: title
*        in: formData
*        required: false  
*      - name: description
*        description: description
*        in: formData
*        required: false  
*    responses:
*      200:
*        description: Your login is successful
*      402:
*        description: Inval_id credentials
*      404:
*        description: Entered email/mobile number is not registered.
*      500:
*        description: Internal Server Error
*      501:
*        description: Something went wrong!
*      400:
*        description: Fields are required.
*/
router.put('/editSectionOnReport/:_id', fetchuser, bannerController.editSectionOnReport);
/**
* @swagger
* /api/v1/user/viewSectionOnReport/{_id}:
*  post:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: _id
*        description: _id
*        in: path
*        required: true  
*      - name: reportId
*        description: reportId
*        in: formData
*        required: true  
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
 router.post("/viewSectionOnReport/:_id", bannerController.viewSectionOnReport);
/**
* @swagger
* /api/v1/user/deleteSectionOnReport/{_id}:
*  delete:
*    tags:
*      - REPORT MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true
*      - name: _id
*        description: _id
*        in: path
*        required: true  
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.delete("/deleteSectionOnReport/:_id", bannerController.deleteSectionOnReport);
/**
* @swagger
  * /api/v1/user/addImageSecton/{_id}:
  *   post:
  *     tags:
  *       - REPORT MANAGEMENT
  *     description: addImageSecton
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: token
  *         description: token
  *         in: header
  *         required: true
  *       - name: _id
  *         description: _id
  *         in: path
  *         required: true   
  *       - name: files
  *         description: files
  *         in: formData
  *         type: file
  *         required: true
  *     responses:
  *       200:
  *         description: Upload successful.
  *       401:
  *         description: Invalid file format
  */
router.post('/addImageSecton/:_id', fetchuser, upload.array("files"), bannerController.addImageSecton)

/**
* @swagger
* /api/v1/user/addDescriptionOnImageSection/{_id}:
*  post:
*    tags:
*      - REPORT MANAGEMENT
*    description: addDescriptionOnImageSection
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true
*      - name: _id
*        description: _id
*        in: path
*        required: true  
*      - name: imageId
*        description: imageId
*        in: formData
*        required: true  
*      - name: description
*        description: description
*        in: formData
*        required: true        
*    responses:
*      200:
*        description: Upload successful.   
*      401:
*        description: Invalid file format
*/
router.post('/addDescriptionOnImageSection/:_id', fetchuser, bannerController.addDescriptionOnImageSection)
/**
* @swagger
* /api/v1/user/getNotification/{_id}:
*  post:
*    tags:
*      - NOTIFICATION MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: _id
*        description: _id
*        in: path
*        required: true  
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.post("/getNotification/:_id", userController.getNotification);

/**
* @swagger
* /api/v1/user/notificationList:
*   get:
*     tags:
*       - NOTIFICATION MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/notificationList", fetchuser, userController.notificationList);
/**
* @swagger
* /api/v1/user/notificationStatus:
*   get:
*     tags:
*       - NOTIFICATION MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/notificationStatus", fetchuser, userController.notificationStatus);
/**
* @swagger
* /api/v1/user/createComplaint:
*   post:
*     tags:
*       - QUERY MANAGEMENT
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: title
*         description: title
*         in: formData
*         required: true
*       - name: comment
*         description: comment
*         in: formData
*         required: true  
*       - name: screenShot
*         description: files
*         in: formData
*         type: file
*         required: true
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.post("/createComplaint", fetchuser, upload.array("screenShot"), userController.createComplaint);
/**
* @swagger
* /api/v1/user/ComplaintList:
*  get:
*    tags:
*      - QUERY MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.get("/ComplaintList", fetchuser, userController.ComplaintList);

/**
* @swagger
* /api/v1/user/getComplaint/{_id}:
*  get:
*    tags:
*      - QUERY MANAGEMENT
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: _id
*        description: _id
*        in: path
*        required: true  
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.get("/getComplaint/:_id", userController.getComplaint);
/**
 * @swagger
 * /api/v1/user/replyOnComplaintbyuser/{id}:
 *   post:
 *     tags:
 *       - QUERY MANAGEMENT
 *     description: Check for Social existence and give the access Token 
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: token
 *         description: token
 *         in: header
 *         required: true
 *       - name: id
 *         description: id
 *         in: path
 *         required: true    
 *       - name: comment
 *         description: comment
 *         in: formData
 *         required: true   
 *       - name: screenShot
 *         description: files
 *         in: formData
 *         type: file
 *         required: false
 *     responses:
 *       200:
 *         description: Details have been fetched successfully.
 *       404: 
 *         description: This user does not exist.
 *       500:
 *         description: Internal Server Error.
 *       501:
 *         description: Something went wrong!
 *       401:
 *         description: Invalid JWT token.
 *       403:
 *         description: You are not authorized, please contact Admin.
 */
router.post("/replyOnComplaintbyuser/:id", fetchuser, upload.array("screenShot"), userController.replyOnComplaintbyuser);
/**
* @swagger
  * /api/v1/user/closeComplaintfromUserSide:
  *   post:
  *     tags:
  *       - QUERY MANAGEMENT
  *     description: Check for Social existence and give the access Token 
  *     produces:
  *       - application/json
  *     parameters:
  *       - name: token
  *         description: token
  *         in: header
  *         required: true
  *       - name: _id
  *         description: _id
  *         in: formData
  *         required: true    
  *     responses:
  *       200:
  *         description: Details have been fetched successfully.
  *       404: 
  *         description: This user does not exist.
  *       500:
  *         description: Internal Server Error.
  *       501:
  *         description: Something went wrong!
  *       401:
  *         description: Invalid JWT token.
  *       403:
  *         description: You are not authorized, please contact Admin.
  */
router.post("/closeComplaintfromUserSide", fetchuser, userController.closeComplaintfromUserSide);

/**
* @swagger
* /api/v1/user/dashboard:
*  post:
*    tags:
*      - CONSUMER
*    description: Check for Social existence and give the access Token 
*    produces:
*      - application/json
*    parameters:
*      - name: token
*        description: token
*        in: header
*        required: true   
*    responses:
*      200:
*        description: Details have been fetched successfully.
*      404: 
*        description: This user does not exist.
*      500:
*        description: Internal Server Error.
*      501:
*        description: Something went wrong!
*      401:
*        description: Invalid JWT token.
*      403:
*        description: You are not authorized, please contact Admin.
*/
router.post("/dashboard", fetchuser, userController.dashboard);

/**
* @swagger
* /api/v1/user/compSetting:
*   put:
*     tags:
*       - CONSUMER
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: companyName
*         description: companyName
*         in: formData
*         required: false
*       - name: address
*         description: address
*         in: formData
*         required: false
*       - name: city
*         description: city
*         in: formData
*         required: false
*       - name: state
*         description: state
*         in: formData
*         required: false
*       - name: country
*         description: country
*         in: formData
*         required: false
*       - name: phone
*         description: phone
*         in: formData
*         required: false
*       - name: landlineNo
*         description: landlineNo
*         in: formData
*         required: false
*       - name: email
*         description: email
*         in: formData
*         required: false
*       - name: image
*         description: image ?? base64
*         in: formData
*         type: file
*         required: false
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.put('/compSetting', fetchuser, upload.single('image'), userController.compSetting);

/**
* @swagger
* /api/v1/user/getCompanySetting:
*   get:
*     tags:
*       - CONSUMER
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*     responses:
*       200:
*         description: Details have been fetched successfully.
*       404: 
*         description: This user does not exist.
*       500:
*         description: Internal Server Error.
*       501:
*         description: Something went wrong!
*       401:
*         description: Invalid JWT token.
*       403:
*         description: You are not authorized, please contact Admin.
*/
router.get("/getCompanySetting",fetchuser, userController.getCompanySetting);

/**
* @swagger
* /api/v1/user/contactUs:
*   post:
*     tags:
*       - CONSUMER
*     description: Check for Social existence and give the access Token 
*     produces:
*       - application/json
*     parameters:
*       - name: token
*         description: token
*         in: header
*         required: true
*       - name: email
*         description: email
*         in: formData
*         required: false
*       - name: userName
*         description: userName
*         in: formData
*         required: false
*       - name: mobileNumber
*         description: mobileNumber
*         in: formData
*         required: false
*       - name: comment
*         description: comment
*         in: formData
*         required: false
*     responses:
*       200:
*         description: Your login is successful
*       402:
*         description: Invalid credentials
*       404:
*         description: Entered email/mobile number is not registered.
*       500:
*         description: Internal Server Error
*       501:
*         description: Something went wrong!
*       400:
*         description: Fields are required.
*/
router.post('/contactUs', fetchuser, userController.contactUs);
module.exports = router;

